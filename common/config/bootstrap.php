<?php
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('mbr', dirname(dirname(__DIR__)) . '/mbr');
Yii::setAlias('jbr', dirname(dirname(__DIR__)) . '/jbr');
Yii::setAlias('files', dirname(dirname(__DIR__)) . '/files');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
