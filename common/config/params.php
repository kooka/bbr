<?php
return [
    'adminEmail' => 'claudiu.crisan@gmail.com',
    'supportEmail' => 'claudiu.crisan@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
];
