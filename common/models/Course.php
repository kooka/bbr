<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "course".
 *
 * @property integer $id
 * @property integer $event_id
 * @property string $title
 * @property string $description
 * @property string $track_link
 * @property string $track_embeded
 *
 * @property Category[] $categories
 * @property Event $event
 */
class Course extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'title'], 'required'],
            [['event_id'], 'integer'],
            [['description', 'track_embeded'], 'string'],
            [['title', 'track_link'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event',
            'title' => 'Title',
            'description' => 'Description',
            'track_link' => 'Track Link',
            'track_embeded' => 'Track Embeded',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }
	
	public function getTitleCategoryEvent()
	{
		$event = $this->event->slug;
		return "{$this->title} ({$event}) ";
	}
    
}
