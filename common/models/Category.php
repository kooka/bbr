<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Query;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $course_id
 * @property string $title
 * @property integer $start_year
 * @property integer $end_year
 * @property string $open
 * @property string $slug
 * @property string $gender
 * @property int $laps
 * @property string $notes
 *
 * @property Course $course
 * @property Subscriber[] $subscribers
 */
class Category extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'title', 'slug'], 'required'],
            [['course_id', 'start_year', 'end_year'], 'integer'],
            [['title', 'slug'], 'string', 'max' => 255],
            [['gender'], 'string', 'max' => 1],
            [['open'], 'string', 'max' => 1],
            [['laps'], 'integer'],
            [['notes'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Traseu',
            'title' => 'Denumire',
            'slug' => 'Identificator',
            'start_year' => 'De la anul',
            'end_year' => 'Pana la anul',
            'open' => 'Open?',
            'gender' => 'Tip M/F',
            'laps' => 'Nr de ture',
            'notes' => 'Observatii',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribers()
    {
        return $this->hasMany(Subscriber::className(), ['category_id' => 'id']);
    }
    
    public function getFullTitle()
    {
        return sprintf("%s (%s-%s)", $this->title, $this->start_year, $this->end_year);
    }
        
    public static function getCategoriesListItems($event=null)
    {
        if (isset(Yii::$app->params['currentEventYear'])) {
			$event = Event::getEventSlugFromParams(Yii::$app->params['currentEventYear']);
        }
				
        $query = new Query;
        $query->select(['category.id, concat(category.title," (", category.start_year, "-" , category.end_year, ")") as category, course.title as course'])
            ->from('category')
            ->join('INNER JOIN', 'course', 'course.id = category.course_id')
            ->join('INNER JOIN', 'event', 'event.id = course.event_id');
		if ($event)
            $query->where('event.slug=:event', [':event' => $event]);
        $query->orderBy('course.title, category.id');
        $command = $query->createCommand();
        $rows = $command->queryAll();     
		
        return ArrayHelper::map($rows, 'id', 'category', 'course');
    }
	
	public static function getCategoriesSlugTitle($event=null, $titleForRanking=true, $titleWithYears=false)
    {		
        if (!$event) {
			$event = Event::getEventSlugFromParams(Yii::$app->params['currentEventYear']);
        }
				
        $query = new Query;
        
        $query  ->select(['category.*, course.title, event.title,
			concat(category.title, ", ", course.title) as titleForRanking,
			concat(category.title," (", category.start_year, "-" , category.end_year, ")")  as titleWithYears
			',
			])
            ->from('category')
            ->join('INNER JOIN', 'course', 'course.id = category.course_id')
            ->join('INNER JOIN', 'event', 'event.id = course.event_id')    
            ->where('event.slug=:event', [':event' => $event])
            ->orderBy('category.start_year DESC');
        $command = $query->createCommand();
        $data = $command->queryAll();     
		
		$title = 'title';
		if ($titleWithYears) {
			$title = 'titleWithYears';
		}
		if ($titleForRanking) {
			$title = 'titleForRanking';
		}
        return ArrayHelper::map($data, 'slug', $title);
    }
	
	public function getTitleWithYears()
	{
		return "{$this->title} (din {$this->start_year} până în {$this->end_year})";
	}
	
	public function getGenderLabel()
	{
		switch ($this->gender) {
			case 'F':
				return 'Feminin';
				break;
			case 'M':
				return 'Masculin';
				break;			
			default:
				return '';
				break;
		}
	}
	
	public function getYearsRange()
    {
        return $this->start_year.'-'.$this->end_year;
    }
}
