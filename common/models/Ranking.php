<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ranking".
 *
 * @property integer $id
 * @property string $event_slug
 * @property string $course_slug
 * @property string $category_slug
 * @property string $event_title
 * @property string $course_title
 * @property string $category_title
 * @property integer $pnc
 * @property string $first_name
 * @property string $last_name
 * @property string $city
 * @property string $gender
 * @property string $final_time
 * @property integer $position_category
 * @property integer $position_course
 */
class Ranking extends \yii\db\ActiveRecord
{
    const TIME_DNS = 'DNS'; //Did not start
    const TIME_DSC = 'DSC'; //Desqualified
    const TIME_ABD = 'ABD'; //Abandoned
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ranking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_slug', 'course_slug', 'category_slug', 'event_title', 'course_title', 'category_title', 'pnc', 'first_name', 'last_name', 'position_category', 'sex', 'city', 'final_time'], 'required'],
            [['pnc', 'position_category', 'position_course'], 'integer'],
            [['event_slug', 'course_slug', 'category_slug', 'event_title', 'course_title', 'category_title', 'first_name', 'last_name', 'city'], 'string', 'max' => 255],
            [['pnc'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_slug' => 'Event Slug',
            'course_slug' => 'Course Slug',
            'category_slug' => 'Category Slug',
            'event_title' => 'Eveniment',
            'course_title' => 'Traseu',
            'category_title' => 'Categorie',
            'pnc' => 'CNP',
            'first_name' => 'Prenume',
            'last_name' => 'Nume',
            'position_category' => 'Pozitia',
            'position_course' => 'Position Course',
        ];
    }

    /**
     * @inheritdoc
     * @return RankingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RankingQuery(get_called_class());
    }
    
    public function getTime()
    {
        switch (strtoupper($this->final_time)) {
            case self::TIME_DNS:
                return 'Nu a luat startul';
                break;
            case self::TIME_DSC:
                return 'Descalificat';
                break;
            case self::TIME_ABD:
                return 'Abandon';
                break;
            default:                
                return $this->final_time;
                break;
        }
        
    }
    public function getFullname()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
