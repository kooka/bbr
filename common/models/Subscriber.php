<?php

namespace common\models;

use Yii;
use yii\base\Exception;
use kartik\mpdf\Pdf;

/**
 * This is the model class for table "subscriber".
 *
 * @property integer $id
 * @property string $hash
 * @property integer $category_id
 * @property integer $pnc
 * @property string $first_name
 * @property string $last_name
 * @property integer $year_of_birth
 * @property string $gender
 * @property string $email
 * @property string $phone
 * @property string $city
 * @property string $address
 * @property string $judet
 * @property string $ci
 * @property int $payed
 * @property string $payment_date
 * @property string $payment_description
 * @property string $tshirt_size
 * @property string $team
 * @property string $email_sent
 * @property integer $amount_to_pay
 * @property integer $amount_payed
* @property string $insurance
 *
 * @property Category $category
 */
class Subscriber extends \yii\db\ActiveRecord
{
    public $agreeWithRegulation;
    public $agreeWithGDPR;
    const PAYED = '1';
    const NOT_PAYED = '0';
    
    const TSHIRT_SIZE_S = 'S';
    const TSHIRT_SIZE_M = 'M';
    const TSHIRT_SIZE_L = 'L';
    const TSHIRT_SIZE_XL = 'XL';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscriber';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [           
            ['first_name', 'required', 'message'=>'Prenumele trebuie completat'],
            ['last_name', 'required', 'message'=>'Numele trebuie completat'],
            ['address', 'required', 'message'=>'Adresa trebuie completată', 'on'=>'Websubscribe'],
            ['city', 'required', 'message'=>'Localitatea trebuie completată'],
            ['judet', 'required', 'message'=>'Judetul trebuie completat', 'on'=>'Websubscribe'],
            ['pnc', 'required', 'message'=>'CNP trebuie completat'],
            ['category_id', 'required', 'message'=>'Trebuie să alegi o categorie de vârstă'],
            ['email', 'required', 'message' => 'Adresa de email trebuie completată'],
            ['phone', 'required', 'message' => 'Nr de telefon trebuie completat'],
            ['email', 'email', 'message'=>'Adresa de email nu este validă'],
            ['category_id', 'integer'],
            [['first_name', 'last_name', 'email'], 'string', 'max' => 255],
            ['phone', 'string', 'max' => 20],
            
            [['first_name', 'last_name', 'city'], 'filter', 'filter' => function ($value) {
				$value = str_replace('-', ' ', $value);
                return ucwords(strtolower($value));
            }],
            
            ['pnc', 'validateUniquePerEvent'],
            ['pnc', 'validateCNP', 'skipOnEmpty' => false, 'skipOnError' => false, 'on'=>['Websubscribe','WebsubscribeJbr']],
            
            ['category_id', 'validate_category', 'skipOnEmpty' => false, 'skipOnError' => false],
            
            [['year_of_birth', 'gender'], 'safe'],
            ['gender', 'string'],
            ['year_of_birth', 'integer'],
            ['agreeWithRegulation', 'safe'],
            ['agreeWithRegulation', 'compare', 'compareValue' => true, 'on'=>['Websubscribe','WebsubscribeJbr'], 'message' => 'Trebuie să fii de acord cu regulamentul concursului pentru a te putea înscrie.'],
            ['agreeWithGDPR', 'safe'],
            ['agreeWithGDPR', 'compare', 'compareValue' => true, 'on'=>['Websubscribe','WebsubscribeJbr'], 'message' => 'Trebuie să accepți acordul cu privire la datele personale.'],


            [['payed', 'payment_date', 'payment_description', 'email_sent', 'amount_to_pay', 'amount_payed'], 'safe'],
			[['amount_to_pay', 'amount_payed'], 'integer'],
					
            [['tshirt_size', 'team', 'number'], 'safe'],
            ['insurance', 'safe', 'on' => 'Websubscribe'],
                    
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Categorie',
            'pnc' => 'CNP',
            'first_name' => 'Prenume',
            'last_name' => 'Nume',
            'year_of_birth' => 'Anul nasterii',
            'gender' => 'Sexul',
            'email' => 'Email',
            'phone' => 'Telefon',
            'city' => 'Localitate',
            'address' => 'Adresa',
            'judet' => 'Judet',
            'number' => 'Nr concurs',
			'email_sent' => 'Email trimis?',
			'amount_to_pay' => 'De plătit',
			'amount_payed' => 'Suma plătită',			
			'payed' => 'A plătit?',			
			'payment_description' => 'Detalii plată',			
			'payment_date' => 'Data plății',			
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }    
    
    public function getGenderFull()
    {
        if ($this->gender === 'M')
            return 'Masculin';
        else if ($this->gender === 'F')
            return 'Feminin';
        else
            return 'Necunoscut';
    }
    
    public function validateCNP($attribute, $params)
    {
		$cnpData = Subscriber::parseCNP($this->pnc);
		if ($cnpData['status'] === false && isset($cnpData['error'])) {
			$this->addError($attribute, $cnpData['error']);
            return false;
		} 
		
		$year = date('Y', strtotime($cnpData['birthdate']));
		$this->year_of_birth = intval($year);
        $this->gender = $cnpData['gender'];  
		
		return true;
    }
	
	public static function parseCNP($cnp)
	{
		$cnpDigits = str_split(trim(strval($cnp)), 1);

		if (count($cnpDigits) != 13) {
			return array('status' => false, 'error' => 'Codul Numeric Personal trebuie să conțină 13 cifre.');
		}

		/**
		 * C este cifră de control (un cod autodetector) aflată în relație cu 
		 * toate celelate 12 cifre ale CNP-ului. Cifra de control este calculată 
		 * după cum urmează: fiecare cifră din CNP este înmulțită cu cifra de pe 
		 * aceeași poziție din numărul 279146358279; rezultatele sunt însumate, 
		 * iar rezultatul final este împărțit cu rest la 11. Dacă restul este 10, 
		 * atunci cifra de control este 1, altfel cifra de control este egală cu restul.
		 */
		$sum = $cnpDigits[0] * 2 
				+ $cnpDigits[1] * 7 
				+ $cnpDigits[2] * 9 
				+ $cnpDigits[3] * 1 
				+ $cnpDigits[4] * 4 
				+ $cnpDigits[5] * 6 
				+ $cnpDigits[6] * 3 
				+ $cnpDigits[7] * 5 
				+ $cnpDigits[8] * 8 
				+ $cnpDigits[9] * 2 
				+ $cnpDigits[10] * 7 
				+ $cnpDigits[11] * 9;

		$rest = $sum % 11;
		if (!( ($rest == 10 && 1 == $cnpDigits[12]) || ($rest == $cnpDigits[12]) )) {
			return array('status' => false, 'error' => 'Codul Numeric Personal este invalid.');
		}

		//de aici incolo CNP este valid

		/*
		  Persoanelor de sex masculin le sunt atribuite numerele impare
		  iar persoanelor de sex feminin numerele pare.
		 */
		$an = intval($cnpDigits[1]) * 10 + intval($cnpDigits[2]);

		switch (intval($cnpDigits[0])) {
			case 1:
			case 2:
				//1 / 2 - născuți între 1 ianuarie 1900 și 31 decembrie 1999
				$an += 1900;
				break;
			case 3:
			case 4:
				//3 / 4 - născuți între 1 ianuarie 1800 și 31 decembrie 1899
				$an += 1800;
				break;
			case 5:
			case 6:
				//5 / 6 - născuți între 1 ianuarie 2000 și 31 decembrie 2099
				$an += 2000;
				break;
			case 7:
			case 8:
			case 9:
				// 7 / 8 - pentru persoanele străine rezidente în România.
				// În plus 9 - pentru persoanele străine.
				$an += 2000;
				if ($an > intval(date('Y') - 14)) {
					$an -= 100;
				}
				break;
			default :
				return array('status' => false, 'error' => 'CNP gender invalid.');
				break;
		}

		$sex = intval($cnpDigits[0]) % 2 == 0 ? 'F' : 'M';
		if (intval($cnpDigits[0]) == 9) {
			$sex = 'S'; //strain
		}
		
		$an = intval($an);
		$luna = intval($cnpDigits[3].$cnpDigits[4]);
		$zi = intval($cnpDigits[5].$cnpDigits[6]);
		
		return array(
			'status' => true,
			'gender' => $sex,			
			'birthdate' => date('Y-m-d', strtotime("{$an}-{$luna}-{$zi}")),
		);
	}
	
	public function validateUniquePerEvent($attribute, $params)
	{
		if ($this->scenario !== 'Websubscribe' && $this->scenario !== 'WebsubscribeJbr') return true;
		
		$eventSlug = \common\models\Event::getEventSlugFromParams();
		
        $query = new \yii\db\Query;
        
        $query  ->select([
                        'subscriber.id'                        
                    ])
                ->from('subscriber')
                ->join('INNER JOIN', 'category', 'category.id = subscriber.category_id')
                ->join('INNER JOIN', 'course', 'course.id = category.course_id')
                ->join('INNER JOIN', 'event', 'event.id = course.event_id')
                ->andWhere('event.slug=:event', [':event' => $eventSlug])
                ->andWhere('subscriber.pnc=:pnc', [':pnc' => $this->pnc]);
		
		$count = $query->count();
		if ($count > 0) {
			$this->addError($attribute, 'Acest CNP este deja înregistrat.');
		}
	}
    
    public function validate_category($attribute, $params)
    {
        $categoryModel = Category::findOne($this->category_id);
        
        if ($categoryModel) {
			if (isset($categoryModel->gender) && !empty($categoryModel->gender) && $categoryModel->gender !== $this->gender) {
	            $this->addError($attribute, sprintf('Categoria aleasă "%s" este pentru sexul %s. Vă rugăm selectați categoria corectă.', $categoryModel->fullTitle, $categoryModel->genderLabel));				
				return false;
			}
            if ( (isset($categoryModel->start_year) && $categoryModel->start_year <= $this->year_of_birth)
                    && (isset($categoryModel->end_year) && $categoryModel->end_year >= $this->year_of_birth) ) {
                return true;
            }
            $this->addError($attribute, sprintf('Anul nașterii %s nu permite înscrierea la categoria %s.', $this->year_of_birth, $categoryModel->fullTitle));
            return false;
        }
        
        return true;
    }
    
    public static function registrationTimeAllowed()
    {
        $currentTime = time();
        if (isset(Yii::$app->params['stopRegistrationDatetime']) 
            && $currentTime > Yii::$app->params['stopRegistrationDatetime']) {
            return false;
        }
        return true;
    }
	
	public static function damTricou()
	{
		$currentTime = time();
        if (isset(Yii::$app->params['damTricou']) 
            && $currentTime > Yii::$app->params['damTricou']) {
            return false;
        }
        return true;
	}


	public static function isEventDay()
    {
        $currentTime = time();
        if (isset(Yii::$app->params['eventDayDatetime']) 
            && $currentTime > Yii::$app->params['eventDayDatetime']) {
            return true;
        }
        return false;
    }
    
    public static function isMaxRegistrations()
    {
        if (!isset(Yii::$app->params['maxRegistrationNumber']) || 
            Yii::$app->params['maxRegistrationNumber'] < 1) {
            return false;
        }
        
		$eventSlug = \common\models\Event::getEventSlugFromParams();
		
        $query = new \yii\db\Query;
        
        $query  ->select([
                        'subscriber.id'                        
                    ])
                ->from('subscriber')
                ->join('INNER JOIN', 'category', 'category.id = subscriber.category_id')
                ->join('INNER JOIN', 'course', 'course.id = category.course_id')
                ->join('INNER JOIN', 'event', 'event.id = course.event_id')
                ->andWhere('event.slug=:event', [':event' => $eventSlug]);
		
		$count = $query->count();
        return $count >= Yii::$app->params['maxRegistrationNumber'] ? true : false; 
    }
    
    public function beforeSave($insert) {
        if($this->isNewRecord) {
            $this->hash = Yii::$app->db->createCommand('SELECT LEFT(UUID(), 8)')->queryScalar();
        }
        if (parent::beforeSave($insert)) {
            if ($this->scenario === 'Websubscribe' || $this->scenario === 'WebsubscribeJbr') {

                if (self::isMaxRegistrations()) {
                    $this->addError('last_name', 'Numarul maxim de participanți a fost atins. Nu se mai pot face alte înscrieri.');
                    return false;
                }

                if (!self::registrationTimeAllowed()) {
                    $this->addError('last_name', 'La această dată nu se mai pot face înscrieri online.');
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }

    }
    
    public function getIsPayed()
    {
        return $this->payed === self::PAYED;
    }
    
    public static function getTshirtSizeOptions()
    {
        return array(
            self::TSHIRT_SIZE_S => 'S - Small',
            self::TSHIRT_SIZE_M => 'M - Medium',
            self::TSHIRT_SIZE_L => 'L - Large',
            self::TSHIRT_SIZE_XL => 'XL - Extra large',
        );
    }
    
    /**
     * Sends an email to registered
     *
     * @return boolean whether the email was send
     */
    public function sendRegistrationEmail()
    {
//		if (\Yii::$app->params['skipSendingRegistrationEmail']) {
//			return false;
//		}
		$sent = false;
		$category = $this->category;
		$course = $category->course;
		try {
            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
                'destination' => Pdf::DEST_STRING,
                'content' => Yii::$app->controller->renderPartial('declaratie',['subscriber' => $this, 'category' => $category, 'course' => $course]),
                'options' => [
                    // any mpdf options you wish to set
                ],
                'methods' => [
                    'SetSubject' => 'Declaratie prorpie raspundere MBR2019',
                    'SetAuthor' => 'Mamut Bike Race',
                    'SetCreator' => 'Mamut Bike Race',
                ]
            ]);
            $attachedPDf = $pdf->render();

			$sent = \Yii::$app->mailer->compose(['html' => 'registration-html', 'text' => 'registration-text'], ['subscriber' => $this])
				->setFrom([\Yii::$app->params['mbrContactEmail'] => 'Mamut Bike Race'])
				->setReplyTo([\Yii::$app->params['mbrContactEmail'] => 'Mamut Bike Race'])
				->setTo($this->email)
				->setSubject('Te-ai inregistrat la Mamut Bike Race, Alba Iulia')
                ->attachContent($attachedPDf,['fileName' => 'declaratie.pdf','contentType' => 'application/pdf'])
				->send();
			if ($sent) {
				$this->email_sent = '1';
				$this->save(false, ['email_sent']);
			}
		} catch (Exception $ex) {
			Yii::warning("Nu s-a trimis email pentru ID {$this->id} la emailul {$this->email}", "emailfail");
			return false;
		}

        return $sent;
    }

    /**
     * Print declaratie pdf
     *
     * @return boolean whether the email was send
     */
    public function printDeclaratiePdf()
    {
        $category = $this->category;
        $course = $category->course;
        try {
            $content = Yii::$app->controller->renderPartial('declaratie',['subscriber' => $this, 'category' => $category, 'course' => $course]);
            $pdf = new Pdf();
            $mpdf = $pdf->api; // fetches mpdf api
            $mpdf->SetCreator('Mamut Bike Race'); // call methods or set any properties
            $mpdf->SetSubject('Declaratie prorpie raspundere MBR2019'); // call methods or set any properties
            $mpdf->WriteHtml($content); // call mpdf write html
            echo $mpdf->Output('declaratie.pdf', 'I');
            //return $pdf->render();


        } catch (Exception $ex) {
            Yii::warning("Nu s-a trimis email pentru ID {$this->id} la emailul {$this->email}", "emailfail");
            return false;
        }
    }
	
	public function getFullname()
	{
		return $this->last_name . ' ' . $this->first_name;
	}
	
	public static function getTaxToPay()
	{
		$currentDatetime = strtotime(date('Y-m-d', time()));
		
		$taxes = \yii\helpers\ArrayHelper::getValue(Yii::$app->params, "taxe");
		foreach ($taxes as $id => $tax) {
			$value = $tax['value'];
			$period = $tax['period'];
			$periods = explode('-', $period);
			$periods = array_map('trim', $periods);
			
			$start = null;
			$end = null;
			if (isset($periods[0])) {
				$start = strtotime(date('Y-m-d', strtotime($periods[0])));
			}
			if (isset($periods[1])) {
				$end = strtotime(date('Y-m-d', strtotime($periods[1])));
			}
			
			if (isset($start) && isset($end) && ($currentDatetime>=$start && $currentDatetime<=$end)) {
				return $value; 
				break;
			} else if (isset($start) && !isset($end)) {
				return $value;
				break;
			}
		}
		
		return null;
	}
}
