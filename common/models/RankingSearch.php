<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Ranking;

/**
 * RankingSearch represents the model behind the search form about `common\models\Ranking`.
 */
class RankingSearch extends Ranking
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['event_slug', 'course_slug', 'category_slug', 'event_title', 'course_title', 'category_title', 'pnc', 'first_name', 'last_name', 'position_category', 'position_course'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ranking::find();
        //var_dump($params);die;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);
        $dataProvider->sort->defaultOrder = [
            'event_slug' => SORT_ASC,
            'course_slug' => SORT_ASC,
            'category_slug' => SORT_ASC,
            'id' => SORT_ASC,
        ];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pnc' => $this->pnc,
            'position_category' => $this->position_category,
            'position_course' => $this->position_course,
        ]);

        $query->andFilterWhere(['like', 'event_title', $this->event_title])
            ->andFilterWhere(['like', 'course_title', $this->course_title])
            ->andFilterWhere(['like', 'category_title', $this->category_title])
            ->andFilterWhere(['like', 'event_slug', $this->event_slug])
            ->andFilterWhere(['like', 'course_slug', $this->course_slug])
            ->andFilterWhere(['=', 'category_slug', $this->category_slug])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name]);

        return $dataProvider;
    }
}
