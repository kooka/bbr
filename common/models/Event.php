<?php

namespace common\models;

use Yii;
use \yii\helpers\ArrayHelper;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $location
 * @property string $start_date
 * @property string $end_date
 *
 * @property Course[] $courses
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug'], 'required'],
            [['description'], 'string'],
            [['slug'], 'unique'],            
            [['start_date', 'end_date'], 'safe'],
            [['title', 'location'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Denumire',
            'slug' => 'Prescurtare',
            'description' => 'Descriere',
            'location' => 'Localitate',
            'start_date' => 'Data de start',
            'end_date' => 'Data de sfarsit',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Course::className(), ['event_id' => 'id']);
    }
    
    public static function findBySlug($slug)
    {        
        if (($model = Event::findOne(['slug' => $slug])) !== null) {
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException('Evenimentul nu există.');
        }
    }
	
	public static function getEventSlugFromParams($year=null)
	{
		if (!$year) {
			$year = Yii::$app->params['currentEventYear'];
		}
		
		$eventSlug = ArrayHelper::getValue(Yii::$app->params, "events.{$year}.name");
		
		$eventModel = Event::findBySlug($eventSlug);
		
		return $eventModel->slug;
	}
}
