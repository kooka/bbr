<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Ranking]].
 *
 * @see Ranking
 */
class RankingQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Ranking[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Ranking|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}