<?php
/* @var $this yii\web\View */
/* @var $user common\models\Subscriber */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$t1 = ArrayHelper::getValue(Yii::$app->params, "taxe.t1");
$t2 = ArrayHelper::getValue(Yii::$app->params, "taxe.t2");
$t3 = ArrayHelper::getValue(Yii::$app->params, "taxe.t3");
//$t4 = ArrayHelper::getValue(Yii::$app->params, "taxe.t4");

?>
Salut <?= $subscriber->first_name ?>,
    
Îți mulțumim că te-ai înscris la competiția de ciclism Mamut Bike Race, Alba Iulia.

Pentru buna organizare a înscrierii, vă rugăm ca în ziua concursului să aveți tipărită  declarația anexată email-ului de confirmare a înscrierii.

Pentru a fi confirmat în lista de participanți plătitori la "Mamut Bike Race" te rugăm să achiți taxa de participare în contul bancar specificat mai jos:

Date financiare:
Asociația Bicheru Cycling 
Sediul: Șeușa, nr.113B, com.Ciugud, jud. Alba
C.I.F.: 33799070
IBAN: RO94BACX0000001063391001
UniCredit Bank – sucursala Alba Iulia

Detalii tranzacție: <?= Html::encode($subscriber->last_name) ?> <?= Html::encode($subscriber->first_name) ?> taxă participare MBR.
În cazul în care vei achita în nume personal, din contul propriu, poți specifica doar: taxă participare MBR.

Pentru a ne ajuta să organizăm totul cât mai bine te rugăm să efectuezi plata în maxim 7 zile de la înscriere.

Taxa de participare pentru traseele RACE si MARATON:
- <?= $t1['value'] ?> lei, în perioada <?= $t1['period'] ?> (include pachet de înscriere + tricou bumbac personalizat)
- <?= $t2['value'] ?> lei, în perioada <?= $t2['period'] ?> (include pachet de înscriere + tricou bumbac personalizat)
- <?= $t3['value'] ?> lei, în perioada <?= $t3['period'] ?> (include pachet de înscriere, NU INCLUDE tricou de bumbac personalizat MBR)

Modalități de plată
- transfer bancar în contul organizatorului
- depunere numerar la orice casierie UniCredit Bank                    
                
Pentru modalitățile de plată specificate, NU SE PERCEPE nici un comision bancar din partea organizatorului.

Confirmarea plății va dura cel puțin două zile lucrătoare după care vei fi adăugat în lista de participanți plătitori. 
În caz că nu vei apărea în listă în maxim 72 de ore de la achitarea taxei de participare, te rugăm să ne contactezi prin email, atașând dovada plății (scanată).

Te așteptăm la concurs!
Echipa Bicheru Cycling.