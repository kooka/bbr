<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $user common\models\Subscriber */

$t1 = ArrayHelper::getValue(Yii::$app->params, "taxe.t1");
$t2 = ArrayHelper::getValue(Yii::$app->params, "taxe.t2");
$t3 = ArrayHelper::getValue(Yii::$app->params, "taxe.t3");
//$t4 = ArrayHelper::getValue(Yii::$app->params, "taxe.t4");

?>
<div>
    <p>Salut <?= Html::encode($subscriber->first_name) ?>,</p>
    
    <p>Îți mulțumim că te-ai înscris la competiția de ciclism Mamut Bike Race, Alba Iulia.</p>

    <p><strong>Pentru buna organizare a înscrierii, vă rugăm ca în ziua concursului să aveți tipărită  declarația anexată acestui email.</strong></p>

    <p>Pentru a fi confirmat în <strong>lista de participanți plătitori</strong> la "Mamut Bike Race" te rugăm <strong>să achiți taxa de participare</strong> în contul bancar specificat mai jos:</p>

    <p><strong>Date financiare:</strong><br/>
    Asociația Bicheru Cycling <br/>
    Sediul: Șeușa, nr.113B, com.Ciugud, jud. Alba<br/>
    C.I.F.: 33799070<br/>
    IBAN: RO94BACX0000001063391001<br/>
    UniCredit Bank – sucursala Alba Iulia</p>

    <p><strong>Detalii tranzacție:</strong> <?= Html::encode($subscriber->last_name) ?> <?= Html::encode($subscriber->first_name) ?> taxă participare MBR.</p>
    <p>În cazul în care vei achita în nume personal, din contul propriu, poți specifica doar: <strong>taxă participare MBR.</strong></p>
    <p>Pentru a ne ajuta să organizăm totul cât mai bine te rugăm să efectuezi plata în maxim 7 zile de la înscriere.</p>
    
    <h4>Taxa de participare pentru traseele RACE si MARATON:</h4>
    <ul>
		<li><strong><?= $t1['value'] ?> lei</strong>, în perioada <i><?= $t1['period'] ?></i>
			<br/>include pachet de înscriere + tricou bumbac personalizat</li>

        <li><strong><?= $t2['value'] ?> lei</strong>, în perioada <i><?= $t2['period'] ?></i>
                <br/>include pachet de înscriere + tricou bumbac personalizat</li>

		<li><strong><?= $t3['value'] ?> lei</strong>, în perioada <i><?= $t3['period'] ?></i>	
			<br/>include pachet de înscriere, NU INCLUDE tricou de bumbac personalizat MBR</li>

	</ul>

    <h4>Modalități de plată</h4>
    <ul>
        <li>transfer bancar în contul organizatorului</li>
        <li>depunere numerar la orice casierie UniCredit Bank</li>
    </ul>                
    </p>

    <p>Pentru modalitățile de plată specificate, NU SE PERCEPE nici un comision bancar din partea organizatorului.</p>

    <p>Confirmarea plății va dura cel puțin două zile lucrătoare după care vei fi adăugat în lista de participanți plătitori. 
        În caz că nu vei apărea în listă în maxim 72 de ore de la achitarea taxei de participare, te rugăm să ne contactezi prin email, 
        atașând dovada plății (scanată).</p>
    
    <p>Te așteptăm la concurs!<br/>Echipa Bicheru Cycling.</p>
</div>
