<?php

use yii\db\Schema;
use yii\db\Migration;

class m150711_100614_alter_subscriber extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `subscriber` 
ADD COLUMN `tshirt_size` VARCHAR(45) NULL,
ADD COLUMN `team` VARCHAR(255) NULL");
    }

    public function down()
    {
        $this->exceute("ALTER TABLE `subscriber` 
DROP COLUMN `team`,
DROP COLUMN `tshirt_size`");
    }
}
