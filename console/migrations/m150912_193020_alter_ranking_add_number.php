<?php

use yii\db\Schema;
use yii\db\Migration;

class m150912_193020_alter_ranking_add_number extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE `ranking` 
ADD COLUMN `number` INT(11) NULL");
        
        $this->execute("ALTER TABLE `ranking` 
CHANGE COLUMN `pnc` `pnc` BIGINT(13) NULL ,
CHANGE COLUMN `gender` `gender` ENUM('F','M') NULL ,
CHANGE COLUMN `city` `city` VARCHAR(255) NULL");
        
    }

    public function safeDown()
    {
        echo "m150912_193020_alter_ranking_add_number cannot be reverted.\n";

        return false;
    }
   
}
