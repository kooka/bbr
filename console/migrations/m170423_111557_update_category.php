<?php

use yii\db\Schema;
use yii\db\Migration;

class m170423_111557_update_category extends Migration
{
    public function up()
    {
$this->execute("ALTER TABLE `category`
ADD COLUMN `laps` TINYINT(2) UNSIGNED NULL DEFAULT NULL,
ADD COLUMN `notes` VARCHAR(255) NULL DEFAULT NULL");
    }

    public function down()
    {
        echo "m170423_111557_update_category cannot be reverted.\n";

        return false;
    }
    
}
