<?php

use yii\db\Schema;
use yii\db\Migration;

class m160313_184820_alter_subscriber extends Migration
{
    public function up()
    {
		$this->execute("ALTER TABLE `subscriber` 
DROP INDEX `uq_pnc` ,
ADD UNIQUE INDEX `uq_pnc` (`pnc` ASC, `category_id` ASC)");
    }

    public function down()
    {
        echo "m160313_184820_alter_subscriber cannot be reverted.\n";

        return false;
    }      
}
