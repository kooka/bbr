<?php

use yii\db\Schema;
use yii\db\Migration;

class m150911_200715_alter_ranking extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ranking` 
DROP INDEX `pnc_UNIQUE`");
    }

    public function down()
    {
        echo "m150911_200715_alter_ranking cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
