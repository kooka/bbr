<?php

use yii\db\Schema;
use yii\db\Migration;

class m150911_201829_alter_ranking_add_team extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ranking` 
ADD COLUMN `team` VARCHAR(255) NULL AFTER `last_name`");
    }

    public function down()
    {
        echo "m150911_201829_alter_ranking_add_team cannot be reverted.\n";

        return false;
    }

}
