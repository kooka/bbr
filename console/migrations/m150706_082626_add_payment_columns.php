<?php

use yii\db\Schema;
use yii\db\Migration;

class m150706_082626_add_payment_columns extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `subscriber` 
ADD COLUMN `payed` TINYINT(1) NOT NULL DEFAULT '0' AFTER `created_at`,
ADD COLUMN `payment_date` DATE NULL AFTER `payed`,
ADD COLUMN `payment_description` VARCHAR(255) NULL AFTER `payment_date`");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `subscriber` 
DROP COLUMN `payment_description`,
DROP COLUMN `payment_date`,
DROP COLUMN `payed`");
    }
}
