<?php

use yii\db\Schema;
use yii\db\Migration;

class m150909_131001_alter_subscriber extends Migration
{
    public function up()
    {
		$this->execute("ALTER TABLE `subscriber` ADD COLUMN `number` INT NULL DEFAULT NULL");
    }

    public function down()
    {
        echo "m150909_131001_alter_subscriber cannot be reverted.\n";

        return false;
    }

}
