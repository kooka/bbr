<?php

use yii\db\Schema;
use yii\db\Migration;

class m150708_042036_add_category_slug extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `category` 
ADD COLUMN `slug` VARCHAR(255),
ADD UNIQUE INDEX `slug_UNIQUE` (`slug` ASC)");
        
        $this->execute("UPDATE `category` SET `slug`='JBR35' WHERE `id`='1'");
        $this->execute("UPDATE `category` SET `slug`='JBR68' WHERE `id`='2'");
        $this->execute("UPDATE `category` SET `slug`='JBR911' WHERE `id`='3'");
        $this->execute("UPDATE `category` SET `slug`='JBR1214' WHERE `id`='4'");
    }

    public function down()
    {
        echo "m150708_042036_add_category_slug cannot be reverted.\n";

        return false;
    }

}
