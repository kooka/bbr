<?php
namespace mbr\controllers;

use Yii;
use mbr\models\ContactForm;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use mbr\models\SubscriberSearchWeb;
use common\models\Subscriber;
use common\models\Ranking;
use common\models\RankingSearch;

use common\models\Course;
use common\models\Category;
use common\models\Event;
use yii\helpers\ArrayHelper;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
	
	public function actionTest()
	{
		$tax = Subscriber::getTaxToPay();
		echo $tax;
	}
	
	public function actionOffline()
	{
		$this->layout = 'maintenance';
		return $this->render('offline');
	}
	
	public function actionError()
	{
		$exception = Yii::$app->errorHandler->exception;
		if ($exception !== null) {
			return $this->render('error', ['exception' => $exception]);
		}
	}
	
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['mbrContactEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionTrack()
    {
        $year = Yii::$app->params['currentEventYear'];
    
        $eventSlug = Event::getEventSlugFromParams($year);
        
        $categories = Category::find()
            ->joinWith('course')
            ->join('INNER JOIN', 'event', 'event.id = course.event_id')
            ->where('event.slug=:event', [':event' => $eventSlug])
            ->orderBy('category.id ASC')
            ->all();
    
        return $this->render('track', ['categories' => $categories]);
    }
    
    public function actionRiders()
    {
        $this->view->title = "Participanți";
		
		$request = Yii::$app->request;
		$year = $request->get('year');
					
		if (!$year) {
			$year = Yii::$app->params['currentEventYear'];
		} else {
			$this->view->title = "Participanți, {$year}";
			$qp = Yii::$app->request->queryParams;
			$qp['year'] = $year;
			Yii::$app->request->setQueryParams($qp);
		}
		
		$eventSlug = Event::getEventSlugFromParams($year);
		
        $searchModel = new SubscriberSearchWeb();		
		
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $courses = ArrayHelper::map(
            Course::find()
                ->joinWith('event')
                ->where('event.slug=:event', [':event' => $eventSlug])
                ->all(), 
            'title', 'title');
        
        $categories = ArrayHelper::map(
            Category::find()
                ->joinWith('course')
                ->join('INNER JOIN', 'event', 'event.id = course.event_id')
                ->where('event.slug=:event', [':event' => $eventSlug])
                ->orderBy('category.id ASC')
                ->all(), 
            'title', 'title', 'course.title');

        return $this->render('riders', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'courses' => $courses,
            'categories' => $categories,
            'statistics' => $searchModel->getStatistics(),
        ]);
    }
    
    public function actionRegulation()
    {
        $year = Yii::$app->params['currentEventYear'];
    
        $eventSlug = Event::getEventSlugFromParams($year);
    
        $categories = Category::find()
            ->joinWith('course')
            ->join('INNER JOIN', 'event', 'event.id = course.event_id')
            ->where('event.slug=:event', [':event' => $eventSlug])
            ->orderBy('category.id ASC')
            ->all();
    
        return $this->render('regulation',
            [
                'categories' => $categories
            ]
        );
    }
    
	public function actionCheckcnp($cnp)
	{		
		$model = Subscriber::findBySql("select * from subscriber where pnc=:cnp", [':cnp' => $cnp])->one();
		
		$return = null;
		if ($model) {
			$return = array(
				'first_name' => $model->first_name,
				'last_name' => $model->last_name,
				'pnc' => $model->pnc,
				'email' => $model->email,
				'phone' => $model->phone,
				'city' => $model->city,
				'team' => $model->team,
				'tshirt_size' => $model->tshirt_size,
                'address' => $model->address,
                'judet' => $model->judet,
			);					
		}
		
		echo \yii\helpers\Json::encode($return);
	}
	
    public function actionRegister()
    {
        $model = new Subscriber();
        $model->setScenario('Websubscribe');

		$tax = Subscriber::getTaxToPay();
		if ($tax) {
			$model->amount_to_pay = $tax;
		}
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->refresh();
			$model->sendRegistrationEmail();
            Yii::$app->session->setFlash('registered', true);
            return $this->redirect(['thankyou']);
        } else {
            return $this->render('register', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionThankyou()
    {
        //Yii::$app->session->setFlash('registered', true);
        if (Yii::$app->session->hasFlash('registered')) {
            return $this->render('thankyou');
        } else {
            return $this->goHome();
        }
    }
    
    public function actionDeclaratie()
    {
        $filePath = Yii::getAlias('@files') . DIRECTORY_SEPARATOR . 'mbr-declaratie.pdf';
        if (file_exists($filePath)) {
            Yii::$app->response->sendFile($filePath, 'mbr-declaratie.pdf');
        } else {
            return $this->goHome();
        }                    
    }
    
    public function actionDeclaratie2()
    {
        $filePath = Yii::getAlias('@files') . DIRECTORY_SEPARATOR . 'mbr-declaratie-minor.pdf';
        if (file_exists($filePath)) {
            Yii::$app->response->sendFile($filePath, 'mbr-declaratie-minor.pdf');
        } else {
            return $this->goHome();
        }                    
    }
	
	public function actionAsigurare()
    {
        $filePath = Yii::getAlias('@files') . DIRECTORY_SEPARATOR . 'certificat_allianz_tiriac_mbr.pdf';
        if (file_exists($filePath)) {
            Yii::$app->response->sendFile($filePath, 'certificat_allianz_tiriac_mbr.pdf');
        } else {
            return $this->goHome();
        }                    
    }
    
    public function actionGpx($filename)
    {
        $filePath = Yii::getAlias('@files') . DIRECTORY_SEPARATOR . $filename;
        if (file_exists($filePath)) {
            Yii::$app->response->sendFile($filePath, $filename);
        } else {
            return $this->goHome();
        }                    
    }
    
    public function actionSchedule()
    {
        return $this->render('schedule');
    }
    
    public function actionMaps()
    {
        return $this->render('maps');
    }
    
    public function actionRankings()
    {
		$request = Yii::$app->request;
		$year = $request->get('year');
		
		if (!$year) {
			$year = Yii::$app->params['currentEventYear'];
		}
				
		if (!ArrayHelper::keyExists("{$year}", Yii::$app->params['events'])) {
			throw new NotFoundHttpException("Clasamentul nu exista.");
		}
		
		$eventSlug = Event::getEventSlugFromParams($year);
		//Yii::error($eventSlug);
		
		$eventCategories = array();
		if (ArrayHelper::keyExists("categories", Yii::$app->params['events'][$year])) {
			$eventCategories = ArrayHelper::getValue(Yii::$app->params, "events.{$year}.categories");			
		} else {
			$eventCategories = Category::getCategoriesSlugTitle($eventSlug, true, false);	
		}
		//Yii::error($eventCategories);
		
        $count = Ranking::find()->andWhere('event_slug=:event', [':event' => $eventSlug])->count();
		
        $searchModel = new RankingSearch();
        $dataproviders = array();

        foreach ($eventCategories as $category => $categoryDescription) {
             $dp = $this->getDataProviderFor($eventSlug, $category, $searchModel);
             if ($dp) {
                 $dataproviders [$category]= $dp;
             }                  
        }
		
        $this->view->title = "Clasamente Mamut Bike Race, {$year}";
		
        return $this->render('rankings', [
            'isRanking' => $count > 0,
            'dataproviders' => $dataproviders,
			'event' => $eventSlug,
			'categories' => $eventCategories,
        ]);
		
    }
    
    private function getDataProviderFor($event, $category, $searchModel)
    {
        $dataProvider = $searchModel->search([
            'RankingSearch' => [
                'event_slug' => $event,
                'category_slug' => $category,
            ]
        ]);
        
        if ($dataProvider->getTotalCount() > 0) {
            $dataProvider->sort = false;
            return $dataProvider;
        } 
        
        return false;
    }
    
}
