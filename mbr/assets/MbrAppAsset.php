<?php

namespace mbr\assets;

use yii\web\AssetBundle;

/**
 * Description of MbrAppAsset
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Apr 17, 2015
 * @encoding UTF-8 
 */
class MbrAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/normalize.min.css',
        'css/styles.css',
		
        'css/font-awesome.min.css',  
        'css/jquery.sidr.light.css',
        'js/media/mediaelementplayer.min.css',
        'js/owl-carousel/owl.carousel.css', 
        //'js/fancybox/jquery.fancybox.css?v=2.1.4',
        'css/mosaic.css',
        'css/responsive.css',
        'css/rs-plugin.css',
        'js/rs-plugin/css/settings.css',
        //'css/tooltipster.css',
        //'css/mega.css',        

        'css/skin3.css', 
        'css/full.css'       
    ];
    public $js = [
        'js/vendor/modernizr-2.6.2-respond-1.1.0.min.js',
//        'js/vendor/jquery-1.8.3.min.js',
        'js/jquery-migrate-3.0.0.min.js',
        'js/rs-plugin/js/jquery.themepunch.tools.min.js',
        'js/rs-plugin/js/jquery.themepunch.revolution.min.js',
        'js/jquery.sidr.js',
        //'js/fancybox/jquery.fancybox.js?v=2.1.4',
        'js/cleantabs.jquery.js',
        'js/fitvids.min.js',
        'js/jquery.scrollUp.min.js',
        'js/media/mediaelement-and-player.min.js',
        'js/owl-carousel/owl.carousel.js',
        //'js/selectivizr-min.js',
        'js/placeholder.js',
        'js/jquery.stellar.min.js',
        'js/mosaic.1.0.1.js',
        //'js/jquery.isotope.js',
        'js/toggle.js',
        //'js/jquery.tooltipster.js',
        'js/jquery.countdown.js',
        'js/jquery.sticky.js',
        //'js/html5media.js',
        'js/slider-3.js',

        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset',
    ];
}
