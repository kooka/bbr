<?php
return [
    //very important: this will be used in all project to filter data
	'currentEventYear' => '2019',
	'events' => [
        '2019' => [
            'name' => 'MBR2019',
        ],
        '2018' => [
            'name' => 'MBR2018',
        ],
        '2017' => [
            'name' => 'MBR2017',
        ],
		'2016' => [
			'name' => 'MBR2016',
		],
		'2015' => [
			'name' => 'MBR2015',
			'categories' => [
				'M1418' => 'Masculin 14-18', 
				'M1929' => 'Masculin 19-29', 
				'M3039' => 'Masculin 30-39', 
				'M40' => 'Masculin 40+', 
				'F1429' => 'Feminin 14-29', 
				'F30' => 'Feminin 30+', 
			],
		],
	],
	    
    'adminEmail' => 'admin@bicheru-cycling.ro',
    'bicheruEmail' => 'bicherucycling@gmail.com',
	'mbrContactEmail' => 'mamut@bicheru-cycling.ro',
    'mbrCurrentEventDate' => '14 Septembrie 2019',
    'mbrStopRegistrationDateTime' => '13 Septembrie 2019, ora 1:00',
	'mbrCurrentEventDateTime' => '14 Septembrie 2019, ora 09:00',
	'mbrCurrentLocation' => 'Alba Iulia, Dealul Mamut',
    
    //after this date registration is not allowed
    'stopRegistrationDatetime' => mktime(18, 0, 0, 9, 13, 2019), //5 sept 2019 ora 11:00:00
	
    'damTricou' => mktime(23, 59, 0, 8, 23, 2019), //23 aug 2019 ora 23:59:00
    
    //ora si data la care se ascunde pe site meniul Inscrieri, Participanti, Au mai ramas..
    //si se afiseaza link spre viitorul Clasament
    'eventDayDatetime' => mktime(8, 30, 0, 9, 14, 2019), //8 sep 2017 ora 8:30:00
    
    //over this number of subscribers, no more registration allowed; -1 no limits
    'maxRegistrationNumber' => 400,
	//lung,scurt 1- 45, 2-60, 3-80, 4-100
    //alba100 1 - 15, 2 - 50 la fata locului. locuri limitate 100
	'taxe' => array(
		't1' => array('value' => 50, 'period' => '01.07.2019 – 05.07.2019'),
		't2' => array('value' => 70, 'period' => '06.07.2019 – 23.08.2019'),
		't3' => array('value' => 80, 'period' => '24.08.2019 – 13.09.2019'),
//        't4' => array('value' => 100, 'period' => '11.09.2019 - 14.09.2019'),
	),
	
];
