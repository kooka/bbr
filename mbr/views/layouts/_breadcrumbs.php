<?php

/**
 * Description of _breadcrumbs
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Apr 20, 2015
 * @encoding UTF-8 
 */
use yii\widgets\Breadcrumbs;

$links = [];
if (isset($this->params['breadcrumbs'])) {
    $links = $this->params['breadcrumbs'];
}
//var_dump($links);
?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper breadcrumb-wrapper">		

    <?=
    Breadcrumbs::widget([
        'tag' => 'div',
        'options' => [
            'class' => 'wrapper'
        ],
        'itemTemplate' => "{link}\n",
        'activeItemTemplate' => "{link}\n",
        'homeLink' => [
            'label' => 'Acasa', 
            'url' => Yii::$app->homeUrl, 
            'template' => '<span class="fa">&#xf015;</span> {link} / '
        ],
        'links' => $links,
    ])
    ?>
    <!--div class="wrapper">
        <a href="index.html" class="fa">&#xf015;</a> <a href="index.html">Home</a>
    </div-->
</div>