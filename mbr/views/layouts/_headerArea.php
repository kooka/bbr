<?php
/**
 * Description of _headerArea
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Apr 20, 2015
 * @encoding UTF-8 
 */
?>
<!-- Header Area -->
<div class="outter-wrapper header-area header-style-3">
    <div class="wrapper clearfix logo-container">
        <header>
            <div class="clearfix">

                <!-- Start Logo -->
                <a class="logo centered" href="<?= Yii::$app->homeUrl ?>">
                    <img src="img/logo-mbr.png" alt="Mamut Bike Race" />
                </a>

            </div>
        </header>
    </div>
</div>