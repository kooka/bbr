<?php
/**
 * Description of _footerContent
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Apr 20, 2015
 * @encoding UTF-8 
 */
use yii\helpers\Url;
?>
<!-- Start Outter Wrapper -->
<div class="outter-wrapper footer-wrapper">		
    <div class="wrapper clearfix">
        <!-- Start Widget -->
        <div class="col-1-3 widget">
            <h3 class="widget-title">Organizator:</h3>
            <a class="logo" href="http://bicheru-cycling.ro" target="_blank">
                <img src="img/logo-bicheru.png" alt="Bicheru Cycling" />
            </a>	
        </div>

        <!-- Start Widget -->
        <div class="col-1-3 widget">
            <h3 class="widget-title">Partener:</h3>
            <a class="logo" href="http://apulum.ro" target="_blank">
                <img src="img/logo-primaria-albaiulia.png" alt="Primăria Alba Iulia" />
            </a>	
			<h3 class="widget-title">Arhiva MBR</h3>
            <ul class="open-hours">
                <li><a href="<?= Url::to(['site/rankings', 'year' => '2018']) ?>">2018 - Clasament</a>
                <li><a href="<?= Url::to(['site/rankings', 'year' => '2017']) ?>">2017 - Clasament</a>
                <li><a href="<?= Url::to(['site/rankings', 'year' => '2016']) ?>">2016 - Clasament</a>
				<li><a href="<?= Url::to(['site/rankings', 'year' => '2015']) ?>">2015 - Clasament</a>
            </ul>
        </div>


        <!-- Start Widget -->
        <div class="col-1-3 widget last">
            <h3 class="widget-title">Info</h3>
            <ul class="open-hours">
				<li>Data <span><?= Yii::$app->params['mbrCurrentEventDateTime'] ?></span></li>
                <li>Locația <span><?= Yii::$app->params['mbrCurrentLocation'] ?></span></li>
                <li>Email <span><?= Yii::$app->params['mbrContactEmail'] ?></span></li>
                <!--li>Telefon <span><a href="tel:0769 489 274">0769 489 274</a></span></li-->
                <li>Descarcă<br/>
                    <strong><a href="<?= Url::to(['site/declaratie']) ?>"><i class="fa fa-file-pdf-o">&nbsp;</i>Declarația pe proprie răspundere</a></strong><br/>
                    <strong><a href="<?= Url::to(['site/declaratie2']) ?>"><i class="fa fa-file-pdf-o">&nbsp;</i>Declarația pe proprie răspundere tutore</a></strong><br/>
                    <!--<strong><a href="https://www.bikemap.net/en/route/4151811-mamut-bike-race-2017-traseu-scurt/" target="_blank"><i class="fa fa-file">&nbsp;</i>Traseu Scurt - GPX</a></strong><br/>
                    <strong><a href="https://www.bikemap.net/en/route/4153164-mamut-bike-race-2017-traseu-lung/" target="_blank"><i class="fa fa-file">&nbsp;</i>Traseu Lung - GPX</a></strong><br/>-->
                </li>
            </ul>						
        </div>

    </div>
</div>