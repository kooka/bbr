<?php
/**
 * Description of _navArea
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Apr 20, 2015
 * @encoding UTF-8 
 */
use yii\helpers\Url;

?>
<!-- Post Header Area -->
<div class="outter-wrapper nav-container post-header-area header-style-3">

    <!-- Start Mobile Menu Icon -->
    <div id="mobile-header" class="">
        <a id="responsive-menu-button" href="#sidr-main">
            <em class="fa fa-bars"></em> Meniu
        </a>

        <div class="anuntImportantMobile">
            <h1>IMPORTANT!</h1>
            <p>
                Ultimele înscrieri pentru Mamut Bike Race vor avea loc vineri, 13 septembrie, ora 18:00.
                Din motive logistice, participanții nu se vor mai putea înscrie și nu vor mai putea achita taxa de participare la fața locului. Vă încurajăm așadar să completați din timp formularul de înscriere.
                Toți cei înscriși și care au achitat taxa în perioada 11-13 septembrie, sunt rugați să aducă la ridicarea pachetelor, dovada de plată a taxei de înscriere.
                Nemaiavând înscrieri la fața locului, taxa de participare rămâne 80 de lei până în ultima zi de înscriere.
            </p>
        </div>
    </div>

    <div id="navigation" class="clearfix">

        <div class="post-header-center centered-menu">
            
            <nav class="nav megamenu_container wrapper">
                <ul id="nav" class="centered-menu megamenu">
                    <?php if (common\models\Subscriber::isEventDay()) : ?>    
                    <li><a href="<?= Url::to(['site/rankings']) ?>">CLASAMENTE</a></li>
                    <?php else : ?>
                    <li><a href="<?= Url::to(['site/register']) ?>">ÎNSCRIERE</a></li>
                    <li><a href="<?= Url::to(['site/riders']) ?>">Participanți</a></li>
                    <?php endif; ?>
                    <li><a href="<?= Url::to(['site/about']) ?>">Despre MBR</a></li>
                    <li><a href="<?= Url::to(['site/track']) ?>">Traseu</a></li>
                    <li><a href="<?= Url::to(['site/maps']) ?>">Cum ajung?</a></li>
                    <li><a href="<?= Url::to(['site/schedule']) ?>">Program</a></li>
                    <li><a href="<?= Url::to(['site/regulation']) ?>">Regulament</a></li>
                </ul>
            </nav>            

        </div>

        <div class="anuntImportant">
            <h1>IMPORTANT!</h1>
            <p>
                Ultimele înscrieri pentru Mamut Bike Race vor avea loc vineri, 13 septembrie, ora 18:00.
                Din motive logistice, participanții nu se vor mai putea înscrie și nu vor mai putea achita taxa de participare la fața locului. Vă încurajăm așadar să completați din timp formularul de înscriere.
                Toți cei înscriși și care au achitat taxa în perioada 11-13 septembrie, sunt rugați să aducă la ridicarea pachetelor, dovada de plată a taxei de înscriere.
                Nemaiavând înscrieri la fața locului, taxa de participare rămâne 80 de lei până în ultima zi de înscriere.
            </p>
        </div>

    </div>
</div>