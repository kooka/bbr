<?php

/**
 * Description of _analytics
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  May 8, 2015
 * @encoding UTF-8 
 */
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55547197-3', 'auto');
  ga('send', 'pageview');

</script>