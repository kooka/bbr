<?php
/**
 * Description of _footerCopyright
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Apr 20, 2015
 * @encoding UTF-8 
 */
?>
<!-- Start Outter Wrapper -->
<div class="outter-wrapper base-wrapper" id="copy">
    <div class="wrapper clearfix">
        <div class="left">&copy; Asociația Bicheru Cycling.</div>

        <!-- Social Icons -->
        <ul class="social-links right">
            <li><a class="fa" title="Facebook" href="https://www.facebook.com/mamutbikerace">&#xf09a;</a></li>
        </ul>
    </div>
</div>