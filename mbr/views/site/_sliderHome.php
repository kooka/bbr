<?php
/**
 * Description of _sliderHome
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Apr 20, 2015
 * @encoding UTF-8 
 */
$slides = [
    [
//        'img' => 'img/sld-mbr2016-1.jpg',
        'img' => 'img/slides/MBR2019_slide1.jpg',
        'alt' => 'Mamut Bike Race',
        'layer1' => Yii::$app->params['mbrCurrentEventDate'] . " - Mamut Bike Race",
        'layer2' => "Mountain Bike - Race/Maraton, Alba Iulia, Dealul Mamut",
        'layer3' => $this->render('_registerNowBtn'),
    ],
    [
//        'img' => 'img/sld-mbr2016-4.jpg',
        'img' => 'img/slides/MBR2019_slide2.jpg',
        'alt' => 'Mamut Bike Race',
        'layer1' => "Pedalează în \"Cealaltă Capitală\"!<br/>
                    Alba Iulia",
        'layer2' => "Concurs de mountain-bike XC, destinat în principal sportivilor amatori, dar și sportivilor legitimați.<br/>",
        'layer3' => $this->render('_registerNowBtn'),
    ],
//    [
//        'img' => 'img/sld-mbr2016-2.jpg',
//        'alt' => 'Mamut Bike Race',
//        'layer1' => "Două trasee diferite<br/>
//                    Traseu scurt Race: 35Km | Traseu lung Maraton: 60Km",
//        'layer2' => "Concursul se desfășoară pe două trasee, diferite ca lungime și grad de dificultate, <br/>accesibile sportivilor începători sau avansați<br/>",
//        'layer3' => $this->render('_registerNowBtn'),
//    ],
];
?>

<!-- Revolution Slider -->
<div class="tp-banner-container">
    <div class="tp-banner" >
        <ul>
            <? foreach ($slides as $slide): ?>
                <!-- SLIDE  -->
                <li data-transition="fade" data-masterspeed="500" >
                    <!-- MAIN IMAGE -->
                    <img src="<?= $slide['img'] ?>" alt="<?= $slide['alt'] ?>" data-bgposition="left center" data-kenburns="on" data-duration="14000" data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="110" data-bgpositionend="right center"/>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption fadeout sfr sport-rs-boxed sport-rs-heading"
                         data-x="0" data-hoffset="0"
                         data-y="155" data-voffset="0"
                         data-captionhidden="off"
                         data-speed="800"
                         data-start="500"
                         data-easing="Power4.easeInOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.05"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power1.easeOut">
                        <?= $slide['layer1'] ?>
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption fadeout sfr sport-rs-boxed sport-rs-text"
                         data-x="0" data-hoffset="0"
                         data-y="280" data-voffset="0"
                         data-captionhidden="off"
                         data-speed="800"
                         data-start="750"
                         data-easing="Power4.easeInOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.05"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power1.easeOut">
                        <?= $slide['layer2'] ?>
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption fadeout sfr"
                         data-x="0" data-hoffset="0"
                         data-y="400" data-voffset="0"
                         data-speed="800"
                         data-start="1000"
                         data-easing="Power4.easeInOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.05"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power1.easeOut">
                        <?= $slide['layer3'] ?>
                    </div>
                </li>
            <? endforeach; ?>

        </ul>
    </div>
</div>
