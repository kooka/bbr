<?php
use yii\helpers\Url;
/**
 * Bikemap.net track embed code
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Jul 28, 2015
 * @encoding UTF-8 
 */
?>
<div style="background-color:#fff;"> <iframe src="http://www.bikemap.net/en/route/3222859-mamut-bike-race-2015/widget/?width=640&amp;height=480&amp;unit=metric" width="640" height="480" border="0" frameborder="0" marginheight="0" marginwidth="0" scrolling="no"> </iframe> <div style="margin: -4px 0 0 5px; height: 16px; color: #000; font-family: sans-serif; font-size: 12px;"> Route <a href="http://www.bikemap.net/en/route/3222859-mamut-bike-race-2015/" style="color:#2a88ac; text-decoration:underline;" target="_blank">3,222,859</a> - powered by <a href="http://www.bikemap.net" style="color:#2a88ac; text-decoration:underline;" target="_blank">www.bikemap.net</a> </div> </div>
<strong><a href="<?= Url::to(['site/gpx']) ?>"><i class="fa fa-file">&nbsp;</i>Descarcă track GPX</a></strong><br/>
