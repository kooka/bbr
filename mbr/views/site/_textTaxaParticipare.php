<?php 
use yii\helpers\ArrayHelper;

$t1 = ArrayHelper::getValue(Yii::$app->params, "taxe.t1");
$t2 = ArrayHelper::getValue(Yii::$app->params, "taxe.t2");
$t3 = ArrayHelper::getValue(Yii::$app->params, "taxe.t3");
//$t4 = ArrayHelper::getValue(Yii::$app->params, "taxe.t4");
?>
<p><strong>Taxa de participare pentru traseele RACE si MARATON:</strong></p>
<ul class="list-3">
	<li><strong><?= $t1['value'] ?> lei</strong>, în perioada <i><?= $t1['period'] ?></i>
		<br/>include pachet de înscriere + tricou bumbac personalizat.</li>

	<li><strong><?= $t2['value'] ?> lei</strong>, în perioada <i><?= $t2['period'] ?></i>
            <br/>include pachet de înscriere + tricou bumbac personalizat.</li>

	<li><strong><?= $t3['value'] ?> lei</strong>, în perioada <i><?= $t3['period'] ?></i>	
		<br/>include pachet de înscriere, NU INCLUDE tricou de bumbac personalizat MBR.</li>

</ul>