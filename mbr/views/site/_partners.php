<?php
/**
 * Description of _partners
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Apr 22, 2015
 * @encoding UTF-8 
 */
use yii\helpers\Url;

$partners = [
//    ['img' => 'logo-elis.png',          'url' => 'http://elis.ro',                                  'alt' => 'Elis Pavaje'],
    ['img' => 'logo-antel.png',         'url' => 'http://antel.com.ro',                             'alt' => 'Antel'],
    ['img' => 'logo-giro.png',          'url' => 'http://www.girotrans.ro',                         'alt' => 'Giro Trans'],
//    ['img' => 'logo-ciuc-new.png',   'url' => 'https://www.ciucpremium.ro/radler.201',           'alt' => 'Ciuc Radler'],
    ['img' => 'logo-dacia.png',         'url' => '#',                                               'alt' => 'Dacia Market'],
    ['img' => 'logo-asv.png',           'url' => 'https://www.facebook.com/Magazinuldebiciclete',   'alt' => 'Alba Sport Vision'],
    ['img' => 'logo-djst.png',          'url' => 'http://www.djstalba.ro',                          'alt' => 'DJST Alba'],
    ['img' => 'logo-bizza.png',         'url' => 'http://bizza.ro',                                 'alt' => 'Bizza'],
    ['img' => 'logo-ballan.png',        'url' => 'http://ballansportswear.com',                     'alt' => 'Ballan Sportswear'],
//    ['img' => 'logo-xcurier.png',      'url' => '#',                               'alt' => 'X Curier'],
    ['img' => 'logo-albacoexim.png',    'url' => 'http://albacoexim.ro',							'alt' => 'Albaco Exim'],
    ['img' => 'logo-sth.png',			'url' => 'http://sth.ro',									'alt' => 'STH'],
    ['img' => 'logo-htransilvania.png',	'url' => 'http://hoteltransilvania.eu',						'alt' => 'Hotel Transilvania, Alba Iulia'],
    ['img' => 'consama.png',	'url' => 'http://consama.ro',						'alt' => 'Consama'],
    //['img' => 'logo-squeezy.png',       'url' => 'http://squeezy-nutrition.ro',                     'alt' => 'Squeezy Sports Nutrition'],
    //['img' => 'logo-balkanstudio.png',	'url' => 'http://www.balkanstudio.ro',						'alt' => 'Balkan Studio'],
    ['img' => 'logo-pepsi.png', 'url' => 'http://www.pepsi.com/ro-ro',          'alt' => 'Pepsi'],
    ['img' => 'logo-alba24.png',        'url' => 'http://alba24.ro',                                'alt' => 'Alba 24'],
    ['img' => 'logo-ziarul-unirea.png', 'url' => 'http://ziarulunirea.ro',                          'alt' => 'Ziarul Unirea'],
    ['img' => 'logo-freerider.png',      'url' => 'http://www.freerider.ro',                          'alt' => 'freerider.ro'],
    //['img' => 'logo-dirtbike.png',      'url' => 'http://www.dirtbike.ro',                          'alt' => 'Dirt Bike'],
    ['img' => 'logo-urbeamea.png',		'url' => 'http://urbeamea.ro/albaiulia',					'alt' => 'Urbea Mea Alba Iulia'],
    //['img' => 'logo-alloro.png',		'url' => 'https://www.facebook.com/RestaurantAlloro',		'alt' => 'Alloro Restaurant'],
    ['img' => 'logo-boieru.png',        'url' => 'http://www.domeniileboieru.ro',                   'alt' => 'Domeniile Boieru'],
//    ['img' => 'logo-alpinlux.png',      'url' => 'http://www.alpin57lux.com',                       'alt' => 'Alpin 57 Lux'],
//    ['img' => 'logo-atlas.png',			'url' => 'http://www.atlastransporte.ro',					'alt' => 'Atlas Transport International'],
    //['img' => 'logo-evola.png',			'url' => 'http://www.fereastraevola.ro',					'alt' => 'Fereastra Evola'],
    ['img' => 'logo-lions.png',         'url' => 'https://www.facebook.com/lions.alba',             'alt' => 'Lions Club Alba Iulia'],
    //['img' => 'logo-tobimar.png',		'url' => 'http://www.tobimar.ro',							'alt' => 'Tobimar'],
    //['img' => 'logo-hotelcetate.png',   'url' => 'http://alba.imparatulromanilor.ro',               'alt' => 'Hotel Cetate'],
//    ['img' => 'logo-rustiq.png',		'url' => 'http://rustiq.ro',								'alt' => 'Rustiq Restaurant Hotel'],
    ['img' => 'logo-liniadesosire.png', 'url' => '#',                                               'alt' => 'Linia de sosire'],
    ['img' => 'logo-allianz.png', 'url' => Url::to(['site/asigurare']) , 'alt' => 'Allianz Tiriac', 'local' => true],
    //['img' => 'logo-brothers.png',		'url' => '#',								'alt' => 'Brothers'],
    ['img' => 'logo-torockoi.png',      'url' => 'http://lactatecoltesti.ro',                               'alt' => 'Lactate Coltesti'],
//    ['img' => 'logo-zuzu.png',      'url' => '#',                               'alt' => 'Zuzu'],
    ['img' => 'logo-cora-nou.png',           'url' => 'http://ectc.ro',                                  'alt' => 'Cora Trade Center'],
    //['img' => 'logo-transeuro.png',		'url' => 'http://transeurogrup.ro',								'alt' => 'TransEuro'],
    ['img' => 'logo-stil-market.png',           'url' => '#',                                  'alt' => 'Stil Market'],
    ['img' => 'logo-dena-asis.png',           'url' => 'http://www.dena-asis.ro',                                  'alt' => 'Dena Asis'],
//    ['img' => 'logo-led-trading-2.png',           'url' => 'http://ledtrading.ro',                                  'alt' => 'Led Trading'],
    //['img' => 'logo-bio-terapia.png',           'url' => '#',                                  'alt' => 'Bio Terapia'],
    //['img' => 'logo-perla.jpg',           'url' => 'http://www.perlaharghitei.ro/',                                  'alt' => 'Perla Harghitei'],
];

?>
<!-- Start Carousel -->
<h3>Parteneri / Sponsori</h3>
<ul class="partners-list">	
<?php foreach ($partners as $partner) :?>
	<li class="item">
		<?php if ($partner['url'] == '#'): ?>
			<img src="img/partners/<?= $partner['img'] ?>" alt="<?= $partner['alt'] ?>" title="<?= $partner['alt'] ?>" style="max-width: 150px !important;">
		<?php else : ?>
			<a href="<?= $partner['url'] ?>" target="<?= array_key_exists('local', $partner) &&  $partner['local'] ? '_self':'_blank' ?>" title="<?= $partner['alt'] ?>">
			<img src="img/partners/<?= $partner['img'] ?>" alt="<?= $partner['alt'] ?>" style="max-width: 150px !important;">
		</a>            
		<?php endif; ?>            
	</li>            
	<?php endforeach; ?> 
</ul>

<!--div class="owl-carousel-container ">
    
    <div id="carousel-partners" class="owl-carousel carousel-partners">
        <?php foreach ($partners as $partner) :?>
        <div class="item">
            <?php if ($partner['url'] == '#'): ?>
                <img src="img/partners/<?= $partner['img'] ?>" alt="<?= $partner['alt'] ?>">
            <?php else : ?>
            <a href="<?= $partner['url'] ?>" target="_blank">
                <img src="img/partners/<?= $partner['img'] ?>" alt="<?= $partner['alt'] ?>">
            </a>            
            <?php endif; ?>            
        </div>            
        <?php endforeach; ?>        
    </div>
</div-->