<?php

use common\models\Subscriber;

/* @var $this yii\web\View */
$this->title = 'Mamut Bike Race, Alba Iulia';
?>

<?= $this->render('_sliderHome'); ?> 


<?php if (!Subscriber::isEventDay()) : ?> 
<!-- Start Outter Wrapper -->
<div class="outter-wrapper centered paralax-block" style="background-image: url(img/parallax-bg-mbr.jpg);" data-stellar-background-ratio="0.75">
    <div class="wrapper clearfix">
        <h3>Au mai rămas până la concurs</h3>
        <div class="countdown styled big-count"></div>
    </div>
</div>
<!-- Start Outter Wrapper -->
<div class="outter-wrapper divider"></div>	
<?php endif ?>

<?= $this->render('_index'); ?> 

<!-- Start Outter Wrapper -->
<div class="outter-wrapper divider"></div>	

<!-- Start Outter Wrapper -->
<div class="outter-wrapper feat-block-1" style="background-color: #ffffff;">
    <div class="wrapper ad-pad clearfix">

        <?= $this->render('_partners'); ?> 

    </div>
</div>