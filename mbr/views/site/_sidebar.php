<?php
use yii\helpers\Html;
/**
 * Description of _sidebar
 *
 * @author  cc
 * @copyright (c) 
 * @date  Aug 18, 2015
 * @encoding UTF-8 
 */
?>
<aside class="col-1-4 last">
	<div class="widget">
		<h3>Ne găsiți si aici</h3>
		<ul class="widget-list list-1">
			<li><?= Html::a('facebook.com/mamutbikerace', 'https://www.facebook.com/mamutbikerace', ['target' => '_blank'])?></li>
			<li><?= Html::a('www.bicheru-cycling.ro', 'http://bicheru-cycling.ro', ['target' => '_blank'])?></li>
			<li><?= Html::mailto(Yii::$app->params['mbrContactEmail'], Yii::$app->params['mbrContactEmail'])?></li>
			<!--li><a href="tel:0769 489 274">0769 489 274</a></li-->
		</ul>
	</div>	
</aside>