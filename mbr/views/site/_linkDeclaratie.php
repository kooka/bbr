<?php
use yii\helpers\Url;
/**
 * Description of _linkDeclaratie
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  May 15, 2015
 * @encoding UTF-8 
 */
?>
Descarcă <strong><a href="<?= Url::to(['site/declaratie']) ?>"><i class="fa fa-file-pdf-o">&nbsp;</i>Declarația pe proprie răspundere</a></strong><br/>
Descarcă <strong><a href="<?= Url::to(['site/declaratie2']) ?>"><i class="fa fa-file-pdf-o">&nbsp;</i>Declarația pe proprie răspundere tutore</a></strong>