<div class="message success">
	<p><strong>Date financiare:</strong><br/>
		Asociația Bicheru Cycling <br/>
		Sediul: Șeușa, nr.113B, com.Ciugud, jud. Alba<br/>
		C.I.F.: 33799070<br/>
		IBAN: RO94BACX0000001063391001<br/>
		UniCredit Bank – sucursala Alba Iulia</p>
	<p><strong>Detalii tranzacție:</strong> [Nume Prenume] taxă participare MBR.</p>
	<p>În cazul în care vei achita în nume personal, din contul propriu, poți specifica doar: <strong>taxă participare MBR.</strong></p>
</div>