<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">

    <div class="wrapper blog-roll ad-pad clearfix">

        <!-- Start Main Column  -->
        <div class="col-1-1">

            <div class="clearfix post">

                <h1 class="title"><?= Html::encode($this->title) ?></h1>

                <?php if (!$isRanking): ?>
                    <div class="message error">Clasamentul nu este încă disponibil. Vom anunța momentul publicării lui. Vă rugăm reveniți.</div>
                <?php endif ?>

            </div>
        </div>

	<?php if ($isRanking): ?>
		
		<? if ($event==='MBR2015') :?>
		<div class="row">
			<div class="col-1-2 last">
				<h3>Queen of Mamut</h3>
				<h5>Beata Adrienne Piringer - Giant Team Romania</h5>
				<p>Timp: 1:44:16</p>
			</div>
			<div class="col-1-2">
				<h3>King of Mamut</h3>
				<h5>Ferencz Ilyes</h5>
				<p>Timp: 1:16:19</p>
			</div>
			<div class="col-1-1 centered">
				<img src="img/podium/<?= strtolower($event) ?>/kom-qom.jpg" alt="Mamut Bike Race - King and Queen of Mamut"/>
			</div>
		</div>
		<hr/>
		<? endif ?>

		<?php $row = 0; ?>
		<?php foreach ($categories as $categorySlug => $categoryDescription) : ?>
			<? if ($row % 2 == 0) $isRow = true; else $isRow = false; ?>
			<? if (($row+1) % 2 == 0) $isEndRow = true; else $isEndRow = false; ?>
		
			<? if ($isRow): ?>
			<div class="row">
			<? endif; ?>			
				
				<div class="col-2-3 <?= $isEndRow ? 'last': '' ?>">
                <h3><?= $categoryDescription?></h3>
                <img src="img/podium/<?= strtolower($event) ?>/<?= strtolower($categorySlug) ?>.jpg" alt=""/>
                <?= $this->render('_ranking', ['key' => $categorySlug, 'dataproviders' => $dataproviders]) ?>
				</div>	
			
			<? if ($isEndRow): ?>
			</div><!--end row-->
			<? endif; ?>				
			
			<? $row++; ?>
		<?php endforeach; ?>
		
	<?php endif ?>

    </div>

</div>