<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Mulțumim pentru înscriere';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="outter-wrapper body-wrapper">

    <div class="wrapper blog-roll ad-pad clearfix">

        <!-- Start Main Column  -->
        <div class="col-1-1">

            <div class="clearfix post">


                <h1 class="title"><?= Html::encode($this->title) ?></h1>
                
                <p>Felicitări. Ai fost înregistrat. Ce urmează acum?</p>

                <p><strong>Pentru buna organizare a înscrierii, vă rugăm ca în ziua concursului să aveți tipărită  declarația anexată email-ului de confirmare a înscrierii.</strong></p>
                
                <p>Pentru a fi confirmat în <strong>lista de participanți plătitori</strong> la "Mamut Bike Race" te rugăm <strong>să achiți taxa de participare</strong> în contul bancar specificat mai jos:</p>
                <?= $this->render('_textDateFinanciare') ?>
                
                <?= $this->render('_textTaxaParticipare') ?>
                
                <h4>Modalități de plată</h4>
                <ul class="list-3">
                    <li>transfer bancar din contul propriu în contul organizatorului</li>
                    <li>depunere numerar la orice casierie UniCredit Bank</li>                    
                </ul>                
                </p>
                
                <p>Pentru modalitățile de plată specificate, NU SE PERCEPE nici un comision bancar din partea organizatorului.</p>
                
                <p>Pentru a ne ajuta să organizăm totul cât mai bine te rugăm să efectuezi plata în maxim 7 zile de la înscriere.</p>
                
                <p>Confirmarea plății va dura cel puțin două zile lucrătoare după care vei fi adăugat în lista de participanți plătitori. 
                    În caz că nu vei apărea în listă în maxim 72 de ore de la achitarea taxei de participare, te rugăm să ne contactezi prin email, 
                    atașând dovada plății (scanată).</p>
                
                <h4>Ridicarea pachetului de concurs</h4>
                <p>Pentru a fluidiza procesul de înscriere îți recomandăm să aduci, la ridicarea pachetului, completate și semnate:
				<ul class="list-3">
					<li>declarația anexată email-ului de confirmare a înscrierii, pentru bicicliștii adulți (vârsta > 18ani, la data concursului)</li>
					<li><?= Html::a('declarația pe proprie răspundere, însoțită de declarația tutorelui / părintelui / împuternicitului legal', Url::to(['site/declaratie2']), ['target' => '_blank']) ?> pentru bicicliștii juniori (vârsta < 18ani, la data concursului)</li>				
				</ul>				
				</p>
                
                <p class="lead">Te așteptăm la concurs!<br/>Echipa Bicheru Cycling.</p>
            </div>
            
        </div>
    </div>
</div>