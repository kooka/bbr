<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Program de desfășurare';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">

    <div class="wrapper blog-roll ad-pad clearfix">

        <div class="col-2-3">
            <div class="clearfix post">

                <h1 class="title"><?= Html::encode($this->title) ?></h1>
				<h5>Desfășurarea evenimentului are loc cu plecare de la baza de agrement din zona Mamut.</h5>
				<h5>Joi, 12 septembrie 2019.</h5>
                <table class="table-style-1">
                    <tbody>
                        <tr>
                            <td>18:00 - 20:00</td> 
                            <td>Ridicare pachete de concurs</td>
                        </tr>                                               
                    </tbody>
                </table>
				
				<h5>Vineri, 13 septembrie 2019.</h5>
                <table class="table-style-1">
                    <tbody>
                        <tr>
                            <td>18:00 - 20:00</td> 
                            <td>Ridicare pachete de concurs</td>
                        </tr>                                               
                    </tbody>
                </table>
				
				<h5>Sâmbătă, 14 septembrie 2019 - ZIUA CONCURSULUI</h5>
                <table class="table-style-1">
                    <tbody>
                        <tr>
                            <td>7:30 - 9:30</td>
                            <td>Ridicare pachete de concurs</td>
                        </tr>
                        <tr>
                            <td>9:45 - 10:00</td>
                            <td>Ședință tehnică cu toți participanții.</td> 
                        </tr>                        
                        <tr class="red-color">
                            <td>10:15</td>
                            <td>Alinierea concurenților la start - traseu lung (MARATON)</td>
                        </tr>
						<tr class="red-color">
                            <td><strong>10:30</strong></td>
                            <td><strong>START traseu lung (MARATON)</strong> </td>
                        </tr>
						<tr class="green-color">
                            <td>10:45</td>
                            <td>Alinierea concurenților la start - traseu scurt (RACE)</td>
                        </tr>
						<tr class="green-color">
                            <td><strong>11:00</strong></td>
                            <td><strong>START traseu scurt (RACE)</strong> </td>
                        </tr>
                        <tr>
                            <td>12:30</td>
                            <td>Așteptarea primilor concurenți</td>
                        </tr>
                        <tr>
                            <td><strong>17:00</strong></td>
                            <td><strong>Premierea</strong></td>
                        </tr>
                        <tr>
                            <td>19:30</td>
                            <td>Încheierea evenimentului</td>
                        </tr>
                        <tr>
                            <td>20:00</td>
                            <td>Ridicarea restricțiilor auto din zona de concurs.</td>
                        </tr>
                    </tbody>
                </table>
				
				<p>În funcție de numărul participanților și de condițiile meteorologice la data și locul concursului, programul prezentat poate fi modificat.</p>
                <p>Timp limită parcurgere traseu scurt: <strong>5 ore 30 min</strong></p>
                <p>Timp limită parcurgere traseu lung: <strong>6 ore</strong></p>

                <p>După acest timp, concurentul care nu trece linia de sosire va fi considerat descalificat (<span title="Did Not Finish" style="cursor:pointer;">DNF</span>).</p>
				
				<p>Timp limită intermediar – (PA2 – despărțire trasee, Inuri): pentru traseu scurt – 3ore,
                    după start, iar pentru traseu lung 2ore 30min, după start. După acest timp, concurentul
                    care nu ajunge la punctele de alimentare specificate va fi considerat descalificat (DNF).</p>
            </div>
        </div>

        <!-- Start Main Sidebar  -->
        <?= $this->render('_sidebar') ?> 
        <!-- Finish Sidebar -->

    </div>
</div>
