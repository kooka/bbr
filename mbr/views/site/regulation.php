<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
$this->title = 'Regulament concurs';
$this->params['breadcrumbs'][] = $this->title;


$t1 = ArrayHelper::getValue(Yii::$app->params, "taxe.t1");
$t2 = ArrayHelper::getValue(Yii::$app->params, "taxe.t2");
$t3 = ArrayHelper::getValue(Yii::$app->params, "taxe.t3");
//$t4 = ArrayHelper::getValue(Yii::$app->params, "taxe.t4");
?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">
    <div class="wrapper ad-pad clearfix">

        <!-- Start Main Column  -->
        <div class="main-content col-4-5 right last">

                <h1 class="title"><?= Html::encode($this->title) ?></h1>
                
                <section id="section1">
                <h4>1. Cadru general</h4>
                <p>1.1. Acest regulament se aplică participanţilor la concursul pentru juniori și adulți "Mamut Bike Race".</p>
                <p>1.2. Organizatorii evenimentului menționat sunt Primăria Municipiului Alba Iulia şi Asociaţia Bicheru Cycling, denumiţi în continuare "organizatori".</p>
                <p>1.3. "Mamut Bike Race" este o competiție de tip mountain-bike XC. Concursul este deschis publicului larg și se adresează în principal bicicliștilor amatori (începători, intermediari și avansați), care au împlinit vârsta de 14ani. Competiția este deschisă sportivilor legitimați la cluburi sportive, precum și celor care dețin o licență FRC, amatori sau profesioniști.Pe traseul scurt vor putea participa minori care au împlinit 9ani la data concursului, într-o categorie separată – Family Open – însoțiți de către un singur părinte; dacă minorul a împlinit 14ani la data concursului, va putea participa doar individual, neînsoțit.</p>
                <p>1.4. Sunt admise biciclete de tip “mountain bike”, în stare bună de funcționare, cu roți de 26”, 27.5” sau 29”.</p>
                <p>1.5. Înscrierea participanţilor se face online sau la faţa locului, în intervalul orar anunțat.</p>
                <p>1.6. Prin bifarea căsuței corespunzătoare în timpul înregistrării online sau prin semnarea formularului de înscriere la fața locului, fiecare participant:
                <br/>-  declară că acceptă şi înţelege termenii şi condiţiile de participare la "Mamut Bike Race", precum şi regulamentul competiţiei;
                <br/>-  îşi dă acordul ca imaginea sa sau alte date publice (spre exemplu rezultatele obţinute, clasamentele etc.) să fie folosite în materialele de promovare a evenimentului, sub formă de fotografii, afişe, filme, interviuri etc., în mod gratuit şi necenzurat.</p>
                <p>1.7. Organizatorul îşi rezervă dreptul de a refuza înregistrarea şi participarea la cursă a concurenților, din motive obiective.</p>
                </section>
                
                <section id="section2">
                <h4>2. Obligațiile participanților</h4>
                <p>2.1. Participarea la "Mamut Bike Race" se face pe proprie răspundere, în deplină cunoştinţă a aptitudinii de a participa la eveniment, a stării de sănătate fizică şi mentală, precum şi a stării tehnice a echipamentului fiecărui participant.</p>
                <p>2.2. Fiecare participant este responsabil pentru propria securitate şi siguranţă. Organizatorii, partenerii, sponsorii, voluntarii şi personalul care participă la realizarea evenimentului nu sunt responsabili pentru orice fel de rănire, deces sau pagube de orice natură care pot surveni în timpul sau ca urmare a desfăşurării evenimentului.</p>
                <p>2.3. Fiecare participant, indiferent de modul de înscriere, trebuie să semneze la preluarea numărului de concurs o <strong>declaraţie de responsabilitate</strong> prin care îşi asumă riscurile ce pot rezulta din participarea la “Mamut Bike Race”. Este recomandat ca participanţii să dispună de asigurare în caz de accidente valabilă pentru competiţii sportive.</p>
                <p>2.4. Purtarea unei <strong>căşti de protecţie</strong> este obligatorie pe toată durata desfăşurării concursului. Refuzul purtării unei căşti de protecţie pe durata competiţiei atrage descalificarea.</p>
                <p>2.5. Participanţii au obligaţia de a asculta şi de a respecta indicaţiile/instrucţiunile organizatorului şi ale personalului de concurs.</p>
                <p>2.6. Preluarea numărului de concurs şi a pachetului de înscriere se face la faţa locului sau în alte locații, anunțate în prealabil, înaintea desfăşurării concursului, în intervalul orar prevăzut de către organizator. Pachetul de înscriere nu poate fi expediat prin poștă în avans.</p>
                <p>2.7. Organizatorul poate interzice luarea startului sau continuarea cursei pentru orice participant, dacă acesta se prezintă în stare de ebrietate, este sub efectul substanțelor halucinogene/dopante sau este în stare de slăbiciune evidentă, boală, epuizare fizică sau psihică.</p>
                <p>2.8. Sportivii juniori vor putea participa la “Mamut Bike Race” dacă sunt insoţiţi de tutorele legal. Sportivii juniori legitimați la un club de ciclism vor putea participa și dacă sunt însoțiți de un reprezentant din partea clubului la care sunt legitimați.</p>
                </section>
                
                <section id="section3">
                <h4>3. Taxa de participare</h4>
                <p>3.1. Taxa de participare se poate achita <b>DOAR</b> prin transfer bancar.</p>
                <p>3.2. Plata prin transfer bancar se poate efectua până în data de <strong><?= date("d.m.Y \(\o\\r\a\ H:i\)", Yii::$app->params['stopRegistrationDatetime']) ?>.</strong></p>
                <p>3.3. Înregistrările online se deschid la data şi ora prevăzută de organizator pe site-ul evenimentului şi se încheie în data de <strong><?= date("d.m.Y \(\o\\r\a\ H:i\)", Yii::$app->params['stopRegistrationDatetime']) ?>.</strong></p>
                <p>3.4. Taxe de participare pentru traseele RACE si MARATON</p>
                <ul class="list-3">
                    <li>Taxa de participare este stabilită la <strong><?= $t1['value'] ?> lei</strong> în cazul înscrierii online şi plăţii prin transfer bancar, efectuată  în perioada: <?= $t1['period'] ?>.</li>
                    <li>Taxa de participare este stabilită la <strong><?= $t2['value'] ?> lei</strong> în cazul înscrierii online şi plăţii prin transfer bancar, efectuată  în perioada: <?= $t2['period'] ?>.</li>
                    <li>Taxa de participare este stabilită la <strong><?= $t3['value'] ?> lei</strong> în cazul înscrierii online şi plăţii prin transfer bancar, efectuată în perioada: <?= $t3['period'] ?>.</li>
               </ul>
                <p>3.5. <b>Toți cei înscriși și care au achitat taxa în perioada 11-13 septembrie, sunt rugați să aducă la ridicarea pachetelor, dovada de plată a taxei de înscriere.</b></p>
                <p>3.6. <b>Nu se vor mai putea face înscrieri la fața locului.</b></p>
                <p>3.7. În caz de neprezentare la start, abandon sau retragere din orice motiv, taxa de participare nu se returnează, nu este transmisibilă şi nu este valabilă la un concurs ulterior.</p>
                <p>3.8. În cazul anulării competiţiei în caz de forţă majoră sau din motive care nu depind de organizator, taxa de participare nu se returnează. Organizatorul nu va suporta în niciun fel eventualele costuri adiacente ale participanților (transport, cazare, masă, etc.).</p>
                </section>
                
                <section id="section4">
                <h4>4. Pachetul de înscriere</h4>
                <p>4.1. Pachetul de înscriere include: </p>
                <ul class="list-3">
                    <li>alimente şi băuturi în zona de start/finish;</li>
                    <li>număr de concurs (<strong> chip cronometrare electronică</strong>) și bride de fixare;</li>
                    <li>produse oferite de către sponsori și parteneri.</li>
                </ul>
                </section>
                
                <section id="section5">
                <h4>5. Trasee</h4>
                <p>5.1. Concursul se desfășoară pe 2 trasee, diferite ca lungime și grad de dificultate. Cele 2 trasee ar putea satisface cerințele sportivilor amatori și profesioniști,fiecare dintre participanți putând alege în funcție de condiția fizică și nivelul de pregătire. Traseele de concurs vor fi definitivate şi anunțate cu cel puțin 3 săptămâni înainte de data concursului.</p>
                <p>5.2. Trasee de concurs:<br/>
                    - <strong>MBR Race - traseu SCURT</strong> (accesibil sportivilor de nivel intermediar și celor începători cu o condiție fizică bună), cu o lungime de aproximativ 35km și o diferență de nivel pozitivă de cca.1300m, considerat de dificultate medie.<br/>
					- <strong>MBR Maraton - traseu LUNG</strong> (accesibil sportivilor avansați), cu o lungime de aproximativ 60km și o diferență de nivel pozitivă de cca.2000m, considerat de dificultate sporită. </p>
                <p>5.3. Organizatorii nu își asumă răspunderea pentru obiectele pierdute sau uitate pe traseu în timpul întrecerii, însă sunteți rugați să aduceți orice obiect sau bun găsit pe traseu organizatorilor pentru a le restitui posesorilor.</p>
                </section>
                
                <section id="section6">
                <h4>6. Categorii și clasament</h4>
                <p>6.1. Participanţii se pot înregistra în următoarele categorii:</p>

                <table class="table-style-1">
                    <thead>
                    <tr>
                        <th>Categorie</th>
                        <th>Traseu</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach($categories as $category): ?>
                            <tr>
                                <td>
                                    <i class="fa <?= $category->gender == 'F' ? 'fa-female':'fa-male' ?>"></i>
                                    <?= $category->title ?> (<?= $category->getYearsRange()?>)
                                </td>
                                <td><?= $category->course->title ?></td>
                            </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
                
                <p>6.2. Încadrarea în categoria de vârstă se face în funcție doar de anul nașterii, indiferent de lună și zi. Vârsta pentru stabilirea categoriei se consideră calculând diferența între anul curent și anul de naștere al concurentului, indiferent de luna sau ziua nașterii.</p>
                <p>6.3. Se fac clasamente pentru categoriile specificate mai sus, precum și un clasament general.</p>
                <p>6.4 Premiere</p>
                <p>6.4.1 La traseele RACE si MARATOR se premiază primii 3 (trei) clasați la fiecare categorie. Premiile vor consta în bani, produse oferite de către sponsori și parteneri, diplome și medalii.</p>
                <p>6.5. Premiile în produse, diplome și medalii se decernează la festivitatea organizată după concurs și nu se expediază prin poștă. Premiile în bani se încasează numerar, <strong> exclusiv de către câștigător</strong>, după festivitatea de premiere, în baza actului de identitate (copie CI).</p>
                <p>6.6. Numărul minim de participanţi la o categorie este de 10 (zece). În cazul în care nu se întruneşte acest număr, categoria va fi comasată cu cea mai apropiată categorie validă.</p>
                <p>6.7. Concurenții pot schimba traseul de concurs pe întreaga perioadă în care înscrierile online sunt deschise. Solicitarea se realizează în scris, prin email trimis organizatorului. După data de 11.09.2019 (ora 11:00) nu mai este posibil.</p>
                </section>
                
                <section id="section7">
                <h4>7. Proceduri de concurs</h4>
                <p>7.1. Şedinţa tehnică<br/>
                O şedinţă tehnică va fi ţinută pentru toţi participanţii, în ziua concursului. În cursul şedinţei tehnice participanţii vor fi informaţi asupra traseului, porţiunilor periculoase, eventualelor modificări, precum şi a regulilor de comportament pe care participanţii sunt obligaţi să le respecte. Şedinţa tehnică va fi ţinută de directorul de concurs, la linia de start, recomandând prezența tuturor participanților.</p>
                <p>7.2. Cronometrarea<br/>
                    La traseele RACE si MARATOR cronometrarea se face la secundă, cu ajutorul unui sistem electronic bazat pe chip-uri. Fiecare participant va purta un chip personalizat, acesta nefiind transmisibil. Este responsabilitatea participanţilor de a monta corect numărul de concurs (care conţine şi chipul de cronometrare).</br>
                </p>
                <p>7.3. Timp limită parcurgere traseu scurt: 5:30ore, după start. Timp limită parcurgere traseu lung: 6ore, după start. După acest timp, concurentul care nu trece linia de sosire va fi considerat descalificat. </p>
				<p>7.4 Timp limită intermediar: (PA2 – despărțire trasee, Inuri): pentru traseu scurt – 3ore, după start, iar pentru traseu lung 2:30ore, după start. După acest timp, concurentul care nu ajunge la punctele de alimentare specificate va fi considerat descalificat. Poate parcurge traseul de concurs, pe proprie răspundere, fără chip de cronometrare, însă organizatorii recomandă întreruperea cursei și întoarcerea în zona Start/Sosire.</p>
                    <p>7.5. Numărul de concurs<br/>
                Preluarea numărului de concurs şi a pachetului de înscriere se face pe baza copiei după actul de identitate, în locațiile și orele stabilite de organizator, conform informațiilor publicate pe site.
                <br/>Numărul de concurs trebuie montat pe ghidon şi trebuie să fie vizibil din faţă pe tot timpul concursului.
                <br/>Logo-urile plasate pe număr trebuie să fie vizibile. 
                <br/>Numărul nu trebuie să fie tăiat sau modificat sub nicio formă în timpul concursului.
                <br/>În caz de abandon numărul trebuie predat reprezentanților organizatorului de pe traseu sau în zona de Start/Sosire.<p>
                <p>7.6. Startul<br/>
                        Startul se va da la data şi ora stabilită de organizator şi anunţată pe site. Localizarea zonei de Start/Sosire va fi publicată în prealabil. Startul va fi deschis timp de 10 minute după ora oficială de start, cei care au pierdut startul şi vor pleca în cursă în aceste 10 minute vor fi cronometraţi în acord cu ora oficială de start. Participanţii care vor lua startul după acest timp nu vor fi clasaţi.</p>
                <p>7.7. Retragerea din concurs/Abandonul<br/>
                Retragerea din concurs din alt motiv decât cel de accident se poate face doar la punctele de control sau în zona Start/Sosire. Concurentul care nu mai poate sau nu mai doreşte să continue concursul are obligaţia să se deplaseze până la cel mai apropiat punct de control sau în zona de Start/Sosire, unde se consemnează retragerea sa, păstrându-şi numărul de concurs, dar fără chip de cronometrare.
                <br/>Concurenţii care se deplasează mai încet au obligaţia de a fi atenţi pentru a face loc celor care vin din spate şi doresc şi pot să îi depăşească. Toți concurenții sunt rugați să nu pedaleze agresiv și să-i respecte pe aceia care sunt mai lenți.
                <br/>În caz de pană, reparaţii, căzătură, accidentare etc. traseul de concurs trebuie eliberat cât mai repede pentru a face posibilă trecerea celorlalţi concurenţi sau a echipelor de salvare.
                <br/>Neanunțarea retragerii din concurs poate cauza o operațiune de căutare și salvare (echipe de salvare, salvamont etc.) pe cheltuiala participantului implicat. Retragerea va fi confirmată prin semnătură pe foaia de arbitraj. Orice participant care se retrage va fi ajutat cu privire la cea mai bună variantă de revenire la Start/Sosire, dar este responsabil pentru transportul propriu, ruta pe care o va folosi și acțiunile ulterioare momentului și locului retragerii.</p>
                    <p>7.8. Oprirea din concurs<br/>
                        Organizatorul îşi rezervă dreptul de a opri un participant în următoarele cazuri:
                    <ul class="list-3">
                        <li>s-a accidentat după start, iar starea sa nu-i mai permite continuarea cursei;</li>
                        <li>nu s-a încadrat în timpii limită anunțați în prealabil pentru anumite puncte de pe traseu;</li>
                        <li>în cazuri excepţionale.</li></ul></p>

                    <p>7.9. Puncte de control<br/>
                Pe traseu sunt amplasate puncte de control obligatorii. Participanții care nu trec printr-un punct de control, indiferent de motiv, vor fi descalificați.</p>
                </section>
                
                <section id="section8">
                <h4>8. Protecția mediului înconjurător</h4>
                <p>8.1. Este strict interzisă aruncarea deşeurilor de orice fel, inclusiv  bidoane, flacoane, tuburi cu gel, ambalaje, în orice loc de pe traseu în afara celor amenajate sau la punctele de control. Cei care nu respectă această regulă vor fi descalificaţi.</p>
                </section>
                
                <section id="section9">
                    <h4>9. Echipament obligatoriu</h4>
                    <p>9.1. Traseele <strong>Race și Maraton</strong>: Echipamentul obligatoriu este compus din <strong>cască de protecţie</strong> şi <strong>număr de concurs</strong> care conține chipul de cronometrare pus la dispoziţie de către organizator.</p>
                    <p>9.2. Bicicleta va fi verificată de către concurent înaintea concursului și va fi în stare bună de funcţionare.</p>
                    <p>9.3. Se recomandă utilizarea recipientelor pentru păstrarea lichidelor pe traseul de concurs între punctele de alimentare.</p>
                    <p>9.4. Alte recomandări: ochelari, pompă, cameră de rezervă, petice pentru pană și leviere, trusă de scule, telefon mobil protejat de umezeală, echipament de ciclism adecvat vremii.</p>
                </section>
                
                <section id="section10">
                    <h4>10. Penalizări/Descalificări</h4>
                    <p>10.1. Vor duce la descalificare / penalizare următoarele acțiuni:</p>
                    <ul class="list-3">
                        <li>nerespectarea regulilor de protecţie a mediului, aruncarea deşeurilor de orice fel în afara zonelor special amenajate;</li>
                        <li>neacordarea priorităţii participanţilor mai rapizi, comportamentul agresiv;</li>
                        <li>lipsa sau neutilizarea echipamentului obligatoriu;</li>
                        <li><strong>abaterea de pe traseul stabilit în vederea scurtării/uşurării acestuia;</strong></li>
                        <li>lipsa de respect față de ceilalţi concurenţi şi spectatori;</li>
                        <li>utilizarea unui alt mijloc de transport în timpul cursei, în afara unei biciclete propulsate de forța umană (exclusiv biciclete tip electric sau hibrid);</li>
                        <li>schimbarea/modificarea numărului de concurs sau a bicicletei;</li>
                        <li>limbajul  vulgar şi comportamentul  nesportiv;</li>
                        <li>doppingul, utilizarea substanțelor halucinogene, alcoolului sau a țigărilor;</li>
                        <li>folosirea sistemelor audio cu căști (ex: iPod, walk-man, mp3 player, etc.);</li>
                        <li>nerespectarea indicațiilor personalului organizator.</li>                    
                    </ul>
                    <p>10.2. Descalificările se pot face și după concurs în urma vizualizării pozelor, filmelor sau a sesizării fondate a altor concurenți.</p>
                </section>
        
                <section id="section11">
                    <h4>11. Reclamaţii/Contestaţii</h4>
                    <p>11.1. Orice participant poate face o reclamație/contestație împotriva altui participant care nu a respectat regulile competiției sau împotriva deciziilor oficialilor.</p>
                    <p>11.2. Reclamaţiile şi contestaţiile se depun în scris comitetului de concurs în decurs de 15 minute de la finish-ul oficial.</p>
                    <p>11.3. Contestația se va depune în scris, la 15 minute de la afișarea rezultatelor complete provizorii, împreună cu o taxă de 50 lei. Rezolvarea contestației se va face in următoarele 30 minute. Taxa de 50 lei va fi înapoiată petentului dacă va fi acceptată contestația.</p>
                </section>
                
                <section id="section12">
                    <h4>12. Caracteristici ale competiției</h4>
                    <p>12.1. Evenimentul este o competiție outdoor. Traseul este neamenajat și neprotejat, în cea mai mare parte desfășurându-se prin pădure, folosește drumuri publice, poteci marcate și nemarcate,  etc. Anumite porțiuni din traseu pot fi considerate dificile; acestea vor fi semnalizate de către organizatori și parcurse cu grijă de către concurenți. </p>
                    <p>12.2. Parcurgerea traseului necesită experiență și unele cunoștințe, cum ar fi:</p>
                    <ul class="list-3">
                        <li>fiecare participant să aibă cel puțin o experiență generală despre concursurile de biciclete, fiind pregătit din punct de vedere al condiției fizice;</li>
                        <li>fiecare participant să aibă cunoștințe tehnice despre abordarea unui traseu neamenajat prin pădure;</li>
                        <li>fiecare participant să fie capabil să urce şi să coboare în siguranţă o pantă înclinată în condiţii dificile;</li>
                        <li>fiecare participant ar trebui să dea dovadă de fair-play și să acționeze în consecință atunci când situația o cere;</li>
                        <li>fiecare participant ar trebui să știe și să aibă o atitudine care să țină cont de faptul că oricât de multe măsuri de siguranță ar lua organizatorii, nu sunt și nu vor putea fi acoperite toate riscurile rezultate din natura competițiilor, mediul și condițiile în care se desfășoara acestea;</li>
                        <li>fiecare participant trebuie să știe că niciun premiu nu este mai presus de sănătatea și viața lui și/sau a altor participanți și să acționeze în consecință pentru a se proteja atunci când simte sau observă un pericol.</li>
                    </ul>
                </section>
                
                <section id="section13">
                    <h4>13. Modificarea traseului, alte decizii privind desfăşurarea concursului</h4>
                    <p>13.1. Organizatorul poate modifica oricând traseul sau poate anula/opri competiţia dacă aceasta nu se poate desfăşura în condiţii de siguranţă.</p>
                    <p>13.2.Suporterii pot susține orice competitor cu condiția să respecte regulile de conduită, indicațiile autorităților și/sau ale oficialilor și să nu impiedice desfășurarea competiției.</p>
                    <p>13.3. Pe parcursul traseelor de concurs nu este permis niciun ajutor venit din exterior (suporter, spectator etc.), excepție făcând cazurile deosebite și ajutorul acordat în punctele de realimentare amenajate, unde competitorii pe lângă băuturile, fructele și alimentele energizante oferite de organizatori pot primii și alte alimente sau lichide.</p>
                    <p>13.4. Toate decizile vor fi luate de comitetul de concurs sau de directorul de concurs.</p>
                    <p>13.5. În caz de forță majoră, organizatorii pot lua decizia unor modificări de traseu sau program. La limită se poate decide inclusiv amânarea sau anularea cursei, în situații excepționale.</p>
                    <p>13.6. Organizatorul îşi rezervă dreptul de a aduce modificări asupra informaţiilor afişate pe site, a regulamentului sau a programului etc. Aceste modificări vor fi aduse la cunoştinţă participanţilor cu minim 5 zile înaintea concursului sau cel târziu la preluarea numerelor de concurs. </p>
                    <p>13.7. Asociația “Bicheru Cycling” organizează în data de <?= Yii::$app->params['mbrCurrentEventDate'] ?> evenimentul sportiv de ciclism Mamut Bike Race. Organizatorul Asociația “Bicheru Cycling” este o asociație non-profit, fără scop comercial / lucrativ și fără angajați. Concursul Mamut Bike Race nu este un produs sau serviciu oferit contracost, ci este o manifestare sportivă pentru amatori a cărei organizare și desfășurare este posibilă datorită exclusiv contribuțiilor de participare ale concurenților, contribuțiilor sponsorilor / partenerilor, precum și datorită pasiunii noastre pentru mișcarea în natură, a muncii în afara joburilor permanente și a dedicației voluntarilor noștri.</p>
                </section>
                
                <hr/>
                <p><?= $this->render('_linkDeclaratie')?></p>
            </div>

        
        <!-- Start Main Sidebar  -->
        <aside class="col-1-5">
            <div class="widget">
                <h3 class="widget-title">Cuprins</h3>
                <ul class="widget-list list-1">
                    <li><a href="#section1">Cadru general</a></li>
                    <li><a href="#section2">Obligațiile participanților</a></li>
                    <li><a href="#section3">Taxa de participare</a></li>
                    <li><a href="#section4">Pachetul de înscriere</a></li>
                    <li><a href="#section5">Traseu</a></li>
                    <li><a href="#section6">Categorii și clasament</a></li>
                    <li><a href="#section7">Proceduri de concurs</a></li>
                    <li><a href="#section8">Protecția mediului înconjurător</a></li>
                    <li><a href="#section9">Echipament obligatoriu</a></li>
                    <li><a href="#section10">Penalizări/Descalificări</a></li>
                    <li><a href="#section11">Reclamaţii/Contestaţii</a></li>
                    <li><a href="#section12">Caracteristici ale competiției</a></li>
                    <li><a href="#section13">Modificarea traseului, alte decizii privind desfăşurarea concursului</a></li>
                </ul>	
            </div>	
        </aside> 
        <!-- Finish Sidebar -->        

    </div>
</div>