<?php
use yii\helpers\Html;
use \yii\helpers\Url;
use common\models\Subscriber;

/* @var $this yii\web\View */
$this->title = 'Înscriere';
$this->params['breadcrumbs'][] = $this->title;

$errorMsg = '';
$isRegistrationExpired = false;
if (Subscriber::isMaxRegistrations()) {
    $errorMsg = 'Numărul maxim de participanți a fost atins. Nu se mai pot face alte înscrieri online.';
    $isRegistrationExpired = true;
}

if (!Subscriber::registrationTimeAllowed()) {
    $errorMsg = 'Perioada de înscrieri ONLINE a expirat.';
//    $errorMsg .= '<br/>Vă puteți înscrie la fața locului, în intervalul orar anunțat:';
//    $errorMsg .= '<br/>JOI, 6 septembrie, între orele 18:00-20:00.';
//    $errorMsg .= '<br/>VINERI, 7 septembrie, între orele 18:00-20:00.';
//    $errorMsg .= '<br/>SÂMBĂTĂ, 8 septembrie, între orele 7:30-9:30.';
//	$errorMsg .= '<br/>Taxa de participare va fi de 100 Lei.';
	$errorMsg .= '<br/><br/>Vă așteptăm la concurs!';
    $isRegistrationExpired = true;
}
?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">

    <div class="wrapper blog-roll ad-pad clearfix">

        <!-- Start Main Column  -->
        <div class="half" style="margin: 0 auto; float: none;">

            <h1 class="title"><?= Html::encode($this->title) ?></h1>
            
            <?php if ($isRegistrationExpired) : ?>
            
            <div class="message error">
                <h4><?= $errorMsg ?></h4>
            </div>
            
            <?php else : ?>
            
            <p>Pentru a te înscrie la competiția de ciclism Mamut Bike Race te rugăm să completezi formularul de mai jos, 
    nu înainte de a fi citit <?= Html::a('Regulamentul', Url::to(['site/regulation']), ['target' => '_blank']) ?>. 
    <br/>După înscriere vei primi un email de confirmare în care vei afla cum poți achita taxa de participare.</p>
            
            <?= $this->render('_registerForm', ['model' => $model]) ?>

			<p>Pentru a fluidiza procesul de înscriere îți recomandăm să aduci, la ridicarea pachetului, completate și semnate:
				<ul class="list-3">
					<li><?= Html::a('declarația pe proprie răspundere', Url::to(['site/declaratie']), ['target' => '_blank']) ?></li>
					<li><?= Html::a('declarația pe proprie răspundere, însoțită de declarația tutorelui / părintelui / împuternicitului legal', Url::to(['site/declaratie2']), ['target' => '_blank']) ?> pentru bicicliștii juniori (vârsta < 18ani, la data concursului)</li>
				</ul>				
			</p>
				
			<?= $this->render('_textTaxaParticipare') ?>

            <p><strong>Înscrieri ONLINE se pot face până vineri, 13 septembrie 2019, ora 18:00!</strong></p>

			<h4>Cum plătesc?</h4>
			<ul class="list-3">
				<li>transfer bancar din contul propriu în contul organizatorului.</li>
				<li>depunere numerar la orice casierie UniCredit Bank.</li>
			</ul>                
            <?= $this->render('_textDateFinanciare') ?>
			
            <p class="small-text grey-color">
                Asociația Bicheru Cycling prelucrează datele cu caracter personal în conformitate cu dispozițiile Legii nr.677/2001 exclusiv în scopul organizării competiției sportive Mamut Bike Race și al identificării participanților, datele fiind destinate pentru acest scop.
                Număr notificare: 35082. Data:21.05.2015.
            </p>

            <h4>Cauți cazare in Alba Iulia?</h4>
                <p>
                    Pentru a putea profita din plin de sejurul la Alba Iulia, partenerii noștri de la <a href="http://www.hoteltransilvania.eu/" target="_blank">Hotel Transilvania</a> și <a href="https://astoriahotels.ro/" target="_blank">Hotel Astoria</a> oferă prețuri preferențiale pentru participanții la Mamut Bike Race. De asemenea, vă este pusă la dispoziție parcare supravegheată video și spațiu închis pentru depozitarea bicicletelor. Vă rugăm să completați formularul de <a href="https://l.facebook.com/l.php?u=https%3A%2F%2Fforms.gle%2FkeDmmoLv6V2HLUqX9%3Ffbclid%3DIwAR0aj2dIL-R-ssTEqzroI_5EiCRcP4b_s9iO7bRgbnUrxxbMCKIpSSMBe1M&h=AT2reJyyFFw8Ngoc72fjEAvvm-y2GS_y8XT4k7ElGCX3HpBv6N-AA79O5c2V_prNSD5xqvfH9FlmJdcf-IfW7lMBgexKYTvLhDK-GlKcCdDPKDvyE-nAfRwlnrDc3fuCOd_PMg" target="_blank"><strong>aici</strong></a> dacă doriți să rezervați o cameră. Mai multe informații despre opțiunile de cazare puteți obține la telefon 0786 560 540.
                </p>
            <?php endif; ?>
            

        </div>
        
    </div>
</div>
