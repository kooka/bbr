<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Subscriber;

/* @var $this yii\web\View */
$this->title = 'Participanți';
$this->params['breadcrumbs'][] = $this->title;

?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">

    <div class="wrapper blog-roll ad-pad clearfix">

        <!-- Start Main Column  -->
        <div class="col-1-1">

            <div class="clearfix post">


                <h1 class="title"><?= Html::encode($this->title) ?></h1>
                
                <?php if (Yii::$app->session->hasFlash('registered')): ?>
                <div class="message success">Felicitări. Ai fost înregistrat. Te așteptăm la concurs. :)</div>
                <?php endif; ?>
                
                <?php \yii\widgets\Pjax::begin(); ?>
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'summary' => "Total: {totalCount} înscriși",
                    'emptyText' => "Nu am găsit rezultate",
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered'
                    ],
                    'columns' => [
                        //['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'last_name',
                            'value' => 'last_name',
                            'label' => 'Nume'
                        ],
                        [
                            'attribute' => 'first_name',
                            'value' => 'first_name',
                            'label' => 'Prenume'
                        ],
                        [
                            'attribute' => 'city',
                            'value' => 'city',
                            'label' => 'Localitate'
                        ],
                        [
                            'attribute' => 'judet',
                            'value' => 'judet',
                            'label' => 'Judet'
                        ],
                        [
                            'attribute' => 'team',
                            'value' => 'team',
                            'value' => function($data) {
                                return strlen(trim($data['team'])) > 0 ? $data['team'] : '-'; 
                            },
                            'label' => 'Echipa/Club'
                        ],
						[
                            'attribute' => 'course',
                            'value' => 'course',
                            'filter' => $courses,
                            'label' => 'Traseu'
                        ],
                        [
                            'attribute' => 'category',
                            'value' => 'category',
                            'filter' => $categories,
                            'label' => 'Categorie'
                        ],
//                        [
//                            'attribute' => 'gender',
//                            'format' => 'html',
//                            'contentOptions' => ['class' => 'centered'],
//                            'value' => function($data) {
//                                return $data['gender'] === 'M' ? '<i class="fa fa-male"></i>' : '<i class="fa fa-female"></i>'; 
//                            },
//                            'filter' => array("M" => "Masculin", "F" => "Feminin"),
//                            'label' => 'Sex'
//                        ],
                        [
                            'attribute' => 'payed',
                            'format' => 'html',
                            'contentOptions' => ['class' => 'centered'],
                            'value' => function($data){
                                return $data['payed'] === Subscriber::PAYED ? '<i class="fa fa-check-circle green-color"></i>' : '<i class="fa fa-circle-o red-color"></i>'; 
                            },
                            'filter' => array(Subscriber::PAYED => "Da", Subscriber::NOT_PAYED => "Nu"),
                            'label' => 'Achitat'
                        ],
                    ],
                ]);
                ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
            
            <hr/>
            
            <?php if (isset($statistics) && !empty($statistics)) : ?>
            <div class="clearfix post">
                <h2>Sumar înscrieri:</h2>
                
                <table class="table-style-1" style="width:60%">
                    <thead>
                        <tr>
                            <th>Traseu</th>
                            <th>Categorie</th>
                            <th>Nr. înscriși</th>                            
                        </tr>
                    </thead>
                    <tbody>
                <?php foreach ($statistics as $row) : ?>
                        <tr>
                            <td><?= $row['course']?></td>
                            <td><?= $row['category'] ?></td>
                            <td><?= $row['total']?></td>
                        </tr>
                <?php endforeach; ?>
                        <tr>
                            <th colspan="2" style="text-align: right">TOTAL ÎNSCRIȘI:</th>
                            <th><?= $dataProvider->getTotalCount(); ?></th>
                        </tr>
                    </tbody>
                </table>
            </div>
            <?php endif; ?>

        </div>

    </div>
</div>
