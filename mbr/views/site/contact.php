<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model common\models\ContactForm */

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">		
    <div class="wrapper ad-pad clearfix">


        <div class="col-3-4">
            <h1><?= Html::encode($this->title) ?></h1>

            <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

                <div class="alert alert-success">
                    Mesajul tău a fost transmis. Vom răspunde cât mai curând posibil.
                </div>
                
                <!--p>
                    Note that if you turn on the Yii debugger, you should be able
                    to view the mail message on the mail panel of the debugger.
                    <?php if (Yii::$app->mailer->useFileTransport): ?>
                        Because the application is in development mode, the email is not sent but saved as
                        a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
                        Please configure the <code>useFileTransport</code> property of the <code>mail</code>
                        application component to be false to enable email sending.
                    <?php endif; ?>
                </p-->

            <?php else: ?>

                <p class="lead">Ne puteți contacta dacă aveți întrebări legate de concursul Mamut Bike Race.</p> 
                
                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                    <?= $form->field($model, 'name')->input('text', ['placeholder' => 'Numele tău']) ?>
                    <?= $form->field($model, 'email')->input('text', ['placeholder' => 'Adresa ta de email']) ?>
                    <?= $form->field($model, 'subject')->input('text', ['placeholder' => 'Subiectul mesajului']) ?>
                    <?= $form->field($model, 'body')->textArea(['rows' => 6, 'placeholder' => 'Scrie mesajul tău']) ?>
                    <?=
                    $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '{image} {input}',
                    ])
                    ?>
                    <?= Html::submitButton('Trimite', ['class' => 'btn2', 'name' => 'contact-button', 'id' => 'send']) ?>
                <?php ActiveForm::end(); ?>

<?php endif; ?>            
            <!-- Finish Main Content -->
        </div>

        <!-- Start Main Sidebar  -->
        <?= $this->render('_sidebar') ?>  
        <!-- Finish Sidebar -->

    </div>
</div>             

<!-- Start Outter Wrapper -->
<div class="outter-wrapper divider"></div>	
