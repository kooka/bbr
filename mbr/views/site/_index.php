<?php
use yii\helpers\Url;
/**
 * Description of _index
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Jun 8, 2015
 * @encoding UTF-8 
 */
?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">		
    <div class="wrapper ad-pad clearfix">
        <!-- Column -->
        <!--
        <div class="col-1-1">
            <p class="lead">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/IL33PGIH1MA?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </p>
        </div> -->
        <div class="col-1-1">
            <h3>De ce să particip?</h3>

            <p class="lead">
                Ajuns la cea de-a cincea ediție, MBR2019 propune 2 trasee de concurs provocatoare, pe care fiecare dintre participanți își poate testa condiția fizică și nivelul de pregătire.
                Mamut Bike Race este un nou mod de a împrieteni mișcarea cu explorarea unei piste numită de pasionați Transalpina bicicliștilor, de a îmbina distracția cu protejarea mediului înconjurător și de a contopi aventura cu autodepășirea. Toate într-o competiție din categoria cross-country, destinată în principal sportivilor amatori, dar și sportivilor legitimați la cluburi sportive.</p>

            <p class="lead">    Mamut Bike Race este un nou mod de a împrieteni mișcarea cu explorarea unei piste numită de pasionați Transalpina bicicliștilor, de a îmbina distracția cu protejarea mediului înconjurător și de a contopi aventura cu autodepășirea. Toate într-o competiție din categoria cross-country, destinată în principal sportivilor amatori, dar și sportivilor legitimați la cluburi sportive.</p>
            
            <p class="lead">Vă așteptăm așadar în data de <strong><?= Yii::$app->params['mbrCurrentEventDate'] ?></strong>, pe toți iubitorii mersului pe bicicletă și a unui stil de viață sănătos la Alba Iulia.</p>
            <p class="lead">Nu uitați să vă înscrieți din timp, pentru a ne ajuta să organizăm totul cât mai bine!</p>
            <?= $this->render('_registerNowBtn'); ?>

        </div>
        <!-- Column -->
        <!--div class="col-1-2 last">
            <h3>Traseu scurt 30Km</h3>
            <div class="mosaic-block circle">
                <a href="<?= Url::to(['site/track']) ?>" class="mosaic-overlay fancybox link" title="Traseu Scurt"></a><div class="mosaic-backdrop">
                    <div class="corner">Traseu Scurt 30Km</div><img src="img/track-mbr-2015.png" alt="Traseu Scurt" /></div>
            </div>
        </div-->


    </div>
</div>