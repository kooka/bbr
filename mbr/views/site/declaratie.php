<?php
use yii\helpers\Html;
use Da\QrCode\QrCode;

$qrCode = (new QrCode($subscriber->hash))
    ->setSize(100)
    ->setMargin(5)
    ->useForegroundColor(0, 0, 0);
?>

<div>
    <div>
        <img src="img/logo-mbr.png" alt="Mamut Bike Race" width="250" style="text-align: left; float: left"/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span style="margin-left: 50px;float:left">
            Numar concurs: <img src="img/numar_declaratie.png" alt="Mamut Bike Race" width="100"/>
        </span>
        <?php
        // or even as data:uri url
        echo '<img src="' . $qrCode->writeDataUri() . '" style="float:right; text-align:right">';
        ?>
    </div>
    <div>

        <h4>Cursa: <?= Html::encode($course->title) ?></h4>
        <h4>Categoria: <?= Html::encode($category->title) ?></h4>
    </div>
    <h3 style="text-align: center">DECLARAŢIE PE PROPRIE RĂSPUNDERE</h3>  <br />
    <p>Subsemnatul(a) <b><?= Html::encode($subscriber->first_name) ?> <?= Html::encode($subscriber->last_name) ?></b>, născut(ă) in anul <b><?= Html::encode($subscriber->year_of_birth) ?></b>,
        domiciliat(ă) în localitatea <b><?= Html::encode($subscriber->city) ?></b>, <b><?= Html::encode($subscriber->address) ?></b>, judeţul <b><?= Html::encode($subscriber->judet) ?></b>,
        identificat cu C.N.P. <b><?= Html::encode($subscriber->pnc) ?></b>, telefon de contact <b><?= Html::encode($subscriber->phone) ?></b>,
        în calitate de participant la competiţia de ciclism;
    </p>
    <p style="font-size: 12px">declar pe proprie răspundere că am citit toate informaţiile publicate pe site-ul competiţiei Mamut Bike Race (abreviată MBR2019),
        organizată în data de 14.09.2019 (sâmbătă), pe traseul stabilit în apropierea municipiului Alba Iulia, jud. Alba.
        Menţionez că sunt informat(ă) în legătură cu riscurile pe care le implică practicarea ciclismului în condiţiile deosebite
        pe care le presupune un asemenea eveniment sportiv, sunt de acord cu aceste condiţii şi, în cazul implicării mele într-un accident
        survenit în timpul concursului, îmi voi asuma întreaga răspundere pentru incidentul respectiv şi nu voi pretinde organizatorului
        nici un fel de daune pentru acesta. Menționez că sunt asigurat medical, în eventualitatea unui accident.
    </p>
   <p style="font-size: 12px">
       Precizez că nu îi voi trage pe organizatorii concursului la răspundere în cazul în care voi fi descalificat din compeţitie pentru nerespectarea regulamentului concursului
   </p>
    <p style="font-size: 12px">
        Pe propria mea răspundere, mai declar următoarele:
    </p>
    <p style="font-size: 12px">
        1. Datele de mai sus sunt corecte și exacte; <br/>
        2. Sunt clinic sănătos/sănătoasă şi nu sufăr de nicio afecţiune care să îmi pericliteze starea de sănătate în timpul desfăşurării evenimentului şi îmi asum întrega responsabilitate în ceea ce priveşte participarea mea la concursul de ciclism MBR2019; <br/>
        3. Sunt de acord să particip la concursul de mountain-bike MBR2019. Am citit, am luat la cunoştinţă şi am înţeles pe deplin Regulamentul de Participare publicat pe site-ul http://mamut.bicheru-cycling.ro/regulament, sunt de acord cu acesta și îl voi respecta în totalitate; <br/>
        4. Am echipamentul adecvat pentru participarea la această competiţie (cască de protecţie, echipament de protecţie, bicicletă în stare foarte bună de funcţionare etc.); <br/>
        5. M-am informat asupra tuturor detaliilor şi riscurilor concursului, pe care mi le asum în totalitate. Cunosc faptul că traseul nu este închis circulaţiei publice, putând apărea animale, vehicule sau alte persoane în afara concursului; <br/>
        6. Organizatorul şi reprezentanţii săi nu pot fi traşi la răspundere pentru niciun fel de rănire sau pierdere, oricare ar fi motivul acesteia. Înţeleg implicaţiile juridice ale participării la acest concurs şi îmi asum întreaga răspundere. <br/>
        7. Am înţeles că informaţiile cu caracter personal vor fi stocate de către organizator exclusiv în scopul organizării competiţiei sportive. Am înţeles că anumite date vor apărea pe liste publice (nume, prenume, localitate, categoria de vârstă, rezultat etc.); <br/>
        8. Mă consider capabil(ă) să termin această cursă *** <br/>
        *** Dacă suferiţi de vreo afecţiune medicală vă rugăm să aduceţi acest lucru la cunoştinţă organizatorului. Sugerăm ca, pentru a  participa la competiţii sportive, să consultaţi în prealabil medicul de familie şi să aveţi acordul acestuia. <br/>

    </p>
    <p>
        <span style="float: left; font-weight: bold;font-size: 12px">Data: ...................</span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span style="float: right; font-weight: bold;font-size: 12px">Semnatura: ......................</span>
    </p>
</div>
