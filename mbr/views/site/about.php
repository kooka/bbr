<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Despre Mamut Bike Race';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">

    <div class="wrapper blog-roll ad-pad clearfix">

        <!-- Start Main Column  -->
        <div class="col-1-1">

            <div class="clearfix post">


                <h1 class="title"><?= Html::encode($this->title) ?></h1>
                <p class="lead">Sâmbătă, 14 septembrie 2019, în Alba Iulia se va desfășura concursul de ciclism Mamut Bike Race. Competiția este de tipul mountain-bike XC, destinată în principal sportivilor amatori (începători, intermediari și avansați), precum și sportivilor legitimați la cluburi sportive.</p>

                <div class="col-2-5 right last pad-left">
                    <img class="" src="img/logo-mbr.png" alt="Mamut Bike Race" />
                </div>
				<p>Evenimentul este o competiție outdoor desfășurată în mare parte prin pădurea din apropierea municipiului Alba Iulia. Traseele propuse în acest an vor include zone de asfalt (pista de biciclete și șosea), pământ, pietriș, bolovăniș și iarbă (drumuri secundare, drumuri forestiere și poteci turistice). Cele 2 trasee sunt adaptate cerințelor sportivilor amatori și profesioniști, fiecare dintre participanți putând alege în funcție de condiția fizică și nivelul propriu de pregătire.</p>
                <p>Parcurgând traseele scurt și lung vei ajunge în zone de belvedere asupra orașului, cât și pe vârful Mamut (altitudine 765m), cu o excelentă panoramă înspre Pietrele Ampoiței și Piatra Craivii. Cele două trasee ajung pe Dealul Stânii (altitudine 920m), de pe vârful căruia, în zilele senine de toamnă se deschid privirii perspective de neuitat înspre Valea Mureșului și Carpații Meridionali.</p>
                <p>Desfășurarea evenimentului are loc cu plecare de la baza de agrement din zona Mamut,  construită pentru a oferi
                    albaiulienilor şi turiştilor un spaţiu amenajat pentru picnic şi recreere. Baza de agrement cuprinde locuri de 
                    parcare, locuri destinate desfăşurării unor activităţi de picnic, loc de joacă pentru copii și un pavilion central 
                    destinat desfăşurării de activităţi de alimentaţie publică.</p>
                <p>
                    Primele menţiuni ale <a href="http://adevarul.ro/locale/alba-iulia/legenda-dealului-mamut-colina-strajuieste-alba-iulia-1_50adaef07c42d5a663992048/index.html" target="_blank">dealului Mamut</a> apar la începutul secolului XIX în hărţile şi însemnările călătorilor străini, 
                    odată cu descoperirea unor fosile de animale preistorice. Numele colinei a stârnit şi mai mult interes, după ce, în 
                    2009, a fost descoperită accidental o măsea de mamut, în timpul lucrărilor de sistematizare a șanțurilor Cetății 
                    Alba Carolina. Specialiştii estimează că fosila, descoperită la mare adâncime într-un strat de pietriș depus în albia 
                    râului Mureş, datează din Pleistocen, perioadă cuprinsă între 2558 milioane şi 10.000 de ani î.Ch.
                </p>

                <p><a href="http://alba24.ro/foto-pista-de-biciclete-de-pe-mamut-deschisa-oficial-ce-urmeaza-sa-se-faca-in-cea-mai-noua-in-zona-de-agrement-din-alba-iulia-338251.html" target="_blank">Pista de biciclete</a>, deschisă oficial în luna august 2014, măsoară cu puţin peste 5 kilometri şi leagă, prin pădure, 
                    două dintre cartierele oraşului Alba Iulia: Miceşti şi Pâclişa. Pista are o lăţime de 3 metri şi este însoţită de o alee 
                    pietonală lată de 2 metri, pe care cei dornici de promenade pot să se plimbe în voie, să admire priveliştea și să 
                    încurajeze concurenții. Pe lângă zona pietonală şi pista de biciclete, mai există şi o bandă asfaltată, destinată 
                    circulaţiei cu autoturismul, însă aceasta doar pentru o parte din traseu (aproximativ un kilometru, până la zona de 
                    agrement).</p>
				
				<!--h2>A doua zi: tura de socializare :)</h2>
				<p>În ziua următoare concursului Bicheru Cycling organizează o tură de socializare pe un traseu montan din apropiere. 
					<ul class="list-1">
						<li>Traseul propus: <strong>Ighiu - Ighiel - Lacul Iezer - Platoul carstic Ciumerna - Cetatea dacică de la Piatra Craivii - Craiva - Ighiu</strong></li>
						<li>Distanța: aproximativ <strong>43km</strong>, cu diferență de nivel aprox 1500m</li>
						<li><strong>Întâlnire la ora 8:30</strong> în parcarea din fața Stadionului "Cetate" din Alba Iulia</li>
						<li>Deplasare cu maşinile în comuna Ighiu, la 11 km de Alba Iulia</li>
				</p-->

                <h2>Ce pot face însoțitorii?</h2>
                <p>Dacă doriți să petreceți mai mult timp în zonă, vă invităm să <a href="http://turism.apulum.ro" target="_blank">VIZITAȚI ALBA IULIA</a>.</p>
                <div class="centered">
                    <img class="" src="img/alba-iulia-cealalta-capitala.jpg" alt="Alba Iulia, Cealaltă Capitală" />
                </div> 
                
                <p>În timp ce bicicliștii cuceresc Dealul Mamut, însoțitorii acestora pot vizita, în imediata apropiere a zonei de campare:</p>
                
                <div class="wrapper ad-pad2 clearfix">
                    <div class="col-2-5">
                    <p><a href="http://parc.apulum.ro" target="_blank">Parcul Dendrologic "Dr. Ion Vlad"</a>, o minune peisagistică a Transilvaniei, primul parc de acest fel 
                        construit direct în pădure, pe un deal.</p>
                    <p>Parcul, în suprafață de 21 de hectare, adăpostește peste 1100 de 
                        specii din toate colțurile lumii și un mare număr de păsări, în special insectivore, care și-au găsit adăpost 
                        în acest colț de rai.</p>
                    </div>
                    <div class="col-3-5 last">
                        <img class="" src="img/parc-dendrologic.jpg" alt="Parcul Dendrologic Dr. Ion Vlad Alba Iulia" />
                    </div> 
                </div>
                
                <div class="wrapper ad-pad2 clearfix">
                    <div class="col-2-5">
                        <p><a href="http://alba24.ro/foto-parcul-de-aventura-de-pe-dealul-mamut-din-alba-iulia-deschis-pentru-amatorii-de-sport-si-adrenalina-dar-si-pentru-copii-program-403516.html" target="_blank">Parcul de Aventură Dynamis</a>, având o suprafață de aproximativ 20.000mp, cu trasee alcătuite dintr-o 
                            serie de elemente suspendate pe copaci sau piloni care se pot parcurge folosind echipamentul 
                            corespunzător de alpinism, prin traversare sau căţărare.</p>
                        <p>Elementele traseelor sunt construite din cabluri, 
                            corzi şi grinzi, care corespund standardelor europene.</p>
                    </div>
                    <div class="col-3-5 last">
                        <img class="" src="img/parc-aventura-dynamis.jpg" alt="Parcul de ventura Dynamis, Alba Iulia" />
                    </div> 
                </div>

                <?= $this->render('_registerNowBtn'); ?>

            </div>



        </div>

    </div>
</div>