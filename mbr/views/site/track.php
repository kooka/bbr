<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Category;

/**
 * @var \common\models\Category $category
 *
 */
/* @var $this yii\web\View */
$this->title = 'Traseu';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">

    <div class="wrapper blog-roll ad-pad clearfix">

        <!-- Start Main Column  -->
        <div class="col-1-1">

            <div class="clearfix post">

                <h1 class="title"><?= Html::encode($this->title) ?></h1>
                <p class="lead">Concursul se desfășoară pe 2 trasee, diferite ca lungime, diferență de nivel și grad de dificultate.
                    Cele 2 trasee ar putea satisface cerințele sportivilor  amatori și profesioniști, fiecare dintre participanți putând alege în funcție de condiția fizică și nivelul de pregătire. Traseele de concurs vor fi definitivate şi anunțate cu
					cel puțin 3 săptămâni înainte de data concursului.</p>
				<p>Vă propunem pentru această a V-a ediție 2 trasee de concurs:
					<ul class="list-3">
                    <li><strong>MBR Race - traseu SCURT</strong> (accesibil sportivilor de nivel intermediar și celor începători cu o condiție fizică bună), cu o lungime de aproximativ <strong>35km</strong>
                        și o diferență de nivel pozitivă de cca.1300m, considerat de dificultate medie. Acest traseu este similar traseului din anul anterior.</li>
						<li><strong>MBR Maraton - traseu LUNG</strong> (accesibil sportivilor avansați), cu o lungime de aproximativ <strong>60km</strong> și o diferență de nivel pozitivă de cca. 2000m, considerat de dificultate sporită. Acest traseu va ramâne, în cea mai mare parte, traseul din anul anterior, putând suferi mici modificări.</li>
					</ul> 					
				</p>
				
                <p class="lead">Startul se va da din apropierea bazei de agrement.</p>
				<p>Organizatorii nu își asumă răspunderea pentru obiectele pierdute sau uitate pe traseu în timpul întrecerii, 
					însă sunteți rugați să aduceți orice obiect sau bun găsit pe traseu organizatorilor pentru a le restitui posesorilor.</p>
				
                <p>Pe parcursul competiției circulația nu va fi restricționată, pe traseu se pot întâlni alte vehicule, turiști, animale. 
					Pe traseul de concurs pot exista porțiuni erodate din cauza ploilor și a utilajelor forestiere, precum și porțiuni 
					înclinate, unde se recomandă mersul pe lânga bicicletă, atât pe urcare cât și pe coborâre. 
					În cazul în care se produc incidente tehnice sau de alta natură, vă rugam să anunțați telefonic organizatorii și/sau, 
					în măsura în care se poate, să vă deplasați până la primul post de control, de unde veți putea fi recuperați de echipele tehnice.</p>


                <p>Track-urile traseelor afișate mai jos sunt <strong>orientative</strong>. Sfătuim concurenții să se orienteze după indicatoare, panglici și marcajele cu vopsea prezente pe trasee. </p>

                <h3>MBR Race - Traseu scurt 35Km</h3>
                <div class="clearfix">
                    <div class="col-1-2">
                        <ul class="list-4">
                            <li>Lungime: 33km</li>
                            <li>Diferență de nivel: cca.1300m</li>

                            <li><strong><a href="https://www.gpsies.com/map.do?fileId=wkbjxlsmidofxjoq" target="_blank"><i class="fa fa-file">&nbsp;</i>Descarcă track GPX</a></strong></li>

                        </ul>
                    </div>
                    <div class="col-1-2">
                        <table class="table-style-1">
                            <thead>
                                <tr>
                                    <th>Categorii - Race</th>
                                </tr>
                            </thead>
                            <tbody>
                            <? foreach($categories as $category): ?>
                                <? if (stripos($category->course->title,  'Race') !== false): ?>
                                    <tr>
                                        <td>
                                            <i class="fa <?= $category->gender == 'F' ? 'fa-female':'fa-male' ?>"></i>
                                            <?= $category->title ?>(<?= $category->getYearsRange()?>)
                                        </td>
                                    </tr>
                                <? endif; ?>
                            <? endforeach; ?>
                            </tbody>
                        </table>                              
                    </div>
                    
                    <div class="centered">
                        <img class="" src="img/MBRScurt.png" alt="Traseu scurt MBR" />
                    </div>
                    
                </div>
				
				<h3>MBR Maraton - Traseu lung 60Km</h3>
                <div class="clearfix">
                    <div class="col-1-2">
                        <ul class="list-4">
                            <li>Lungime: 55km</li>
                            <li>Diferență de nivel: cca.2100m</li>

                            <li><strong><a href="https://www.gpsies.com/map.do?fileId=tyvrdthrqxrhjuxi" target="_blank"><i class="fa fa-file">&nbsp;</i>Descarcă track GPX</a></strong></li>

                        </ul>
                    </div>
                    <div class="col-1-2">
                        <table class="table-style-1">
                            <thead>
                            <tr>
                                <th>Categorii - Maraton</th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach($categories as $category): ?>
                                <? if (stripos($category->course->title,  'Maraton') !== false): ?>
                                    <tr>
                                        <td>
                                            <i class="fa <?= $category->gender == 'F' ? 'fa-female':'fa-male' ?>"></i>
                                            <?= $category->title ?> (<?= $category->getYearsRange()?>)
                                        </td>
                                    </tr>
                                <? endif; ?>
                            <? endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    
					<div class="centered">
                        <img class="" src="img/MBRLung.png" alt="Traseu lung MBR" />
                    </div>
  
                </div>
				
                <p>Încadrarea în categoria de vârstă se face în funcție doar de <strong>anul nașterii</strong>, indiferent de lună și zi. Vârsta pentru stabilirea categoriei se consideră calculând diferența între anul curent și anul de naștere al concurentului, indiferent de luna sau ziua nașterii.</p>
				
				
                <h3>Premii</h3>
                <p>
                    Concurenții care își validează înscrierea prin achitarea taxei de participare până la data de <?= date("d.m.Y \(\o\\r\a\ H:i\)", Yii::$app->params['damTricou']) ?>
                    primesc cadou un <strong>tricou de bumbac personalizat Mamut Bike Race</strong>.</p>
                <p>Se fac clasamente pentru categoriile specificate mai sus, precum și un clasament general.</p>
                <p>Se premiază primii 3 (trei) clasați la fiecare categorie.
					Se acordă PREMII ÎN BANI, diplome și medalii, precum și eventuale produse oferite de către sponsori și parteneri. </p>

                <p>Premiile în bani vor fi după cum urmează:
                <ul class="list-3">
                    <li>Traseu MBR Race: locul I - 300 Lei, locul II - 200 Lei, locul III - 100 Lei</li>
                    <li>Traseu MBR Maraton: locul I - 600 Lei, locul II - 400 Lei, locul III - 200 Lei</li>
                </ul>
                </p>
                
                <h3>Beneficii</h3>
                <p>Concurenții vor putea beneficia de:</p>
                <ul class="list-3">
                    <li>tricou bumbac personalizat (doar cei înscriși online, vezi Înscriere)</li>
                    <li>pachet de înscriere conținând produse oferite de sponsori și parteneri (doar cei înscriși online, vezi Înscriere)</li>
                    <li>număr de concurs și 4bride de fixare</li>
                    <li>puncte de alimentare / hidratare pe traseu (apă, fructe, glucoză etc.)</li>
                    <li>punct de revitalizare la sosire (alimente și băuturi)</li>
                    <li>cronometrare individuală (exclusiv cursa populară)</li>
                    <li>clasament afișat online</li>
                    <li>medalie de finisher pentru toți cei care termină cursa în timpul indicatt</li>
                    <li>fotografii de pe traseul de concurs</li>
                    <li>asistență medicală</li>
                    <li>zonă parcare, toalete ecologice</li>
                </ul>

            </div>

        </div>
        
        <div class="clearfix">
            <div class="col-1-1">
                <div class="centered">
                    <?= $this->render('_registerNowBtn'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
