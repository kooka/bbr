<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Cum ajung la concurs?';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">

    <div class="wrapper blog-roll ad-pad clearfix">

        <div class="col-1-1 last">
            <div class="clearfix post">

                <h1 class="title"><?= Html::encode($this->title) ?></h1>

                <h3>Dinspre Cluj-Napoca</h3>   
				<p class="small-text">Click pe hartă pentru detalii</p>
				<p>
					<a href="https://www.google.ro/maps/dir/46.110184,23.6107163/46.0799947,23.535128/@46.0990451,23.5630951,13.28z/am=t/data=!4m6!4m5!3e0!6m3!1i0!2i3!3i0?hl=en" target="_blank">
						<img src="img/map-mbr-cluj-alba.jpg" alt="Dinspre Cluj-Napoca"/>
					</a>
                </p>
				
                <h3>Dinspre Sebeș</h3>
				<p class="small-text">Click pe hartă pentru detalii</p>
                <p>
					<a href="https://www.google.ro/maps/dir/46.0342957,23.5567945/46.0799947,23.535128/@46.0497017,23.5275822,13.04z/data=!4m9!4m8!1m5!3m4!1m2!1d23.5589246!2d46.0852832!3s0x474ea862d0327095:0xb07758b3df15c8e2!1m0!3e0?hl=en" target="_blank">
						<img src="img/map-mbr-sebes-alba.jpg" alt="Dinspre Sebeș"/>
					</a>
                </p>
				
                <h3>Dinspre Zlatna</h3>
				<p class="small-text">Click pe hartă pentru detalii</p>
                <p>
					<a href="https://www.google.ro/maps/dir/46.1100727,23.5404032/46.0799947,23.535128/@46.0947904,23.5350365,14z/data=!4m9!4m8!1m5!3m4!1m2!1d23.5587669!2d46.0854741!3s0x474ea862c145ea3d:0xd4931dd7af37eb7b!1m0!3e0?hl=en" target="_blank">
						<img src="img/map-mbr-zlatna-alba.jpg" alt="Dinspre Zlatna"/>
					</a>
                </p>
                           
            </div>
        </div>

    </div>
</div>
