<?php
/**
 * Description of _sliderHome
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Apr 20, 2015
 * @encoding UTF-8 
 */
$imgs = array(
    'slide01.jpg',
    'slide02.jpg',
    'slide03.jpg',
    'slide04.jpg',
    'slide05.jpg',
);
?>
<!-- Revolution Slider -->
<div class="tp-banner-container">
    <div class="tp-banner" >
        <ul>
            <?php foreach($imgs as $img): ?>
            <li data-transition="fade" data-masterspeed="500" >
                <img src="img/slides2018/<?= $img ?>" alt="Mamut Bike Race" data-bgposition="left center" data-kenburns="on" data-duration="14000" data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="110" data-bgpositionend="right center"/>
			</li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
