<?php

/**
 * Description of _registerForm
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Apr 28, 2015
 * @encoding UTF-8 
 */


use yii\helpers\Html;
use \yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\Subscriber;

$items = \common\models\Category::getCategoriesListItems();
$tshirtSizes = \common\models\Subscriber::getTshirtSizeOptions();
?>

<?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>
<?= $form->field($model, 'pnc')->textInput(['placeholder' => 'Codul Numeric Personal'])->label(false)  ?>
<?= $form->field($model, 'last_name')->textInput(['maxlength' => 255, 'placeholder' => 'Numele de familie'])->label(false)  ?>
<?= $form->field($model, 'first_name')->textInput(['maxlength' => 255, 'placeholder' => 'Prenumele'])->label(false) ?>
<?= $form->field($model, 'team')->textInput(['maxlength' => 255, 'placeholder' => 'Echipa/Clubul sportiv (optional)'])->label(false) ?>
<?= $form->field($model, 'address')->textInput(['maxlength' => 255, 'placeholder' => 'Adresa'])->label(false) ?>
<?= $form->field($model, 'city')->textInput(['maxlength' => 255, 'placeholder' => 'Localitatea'])->label(false) ?>
<?= $form->field($model, 'judet')->textInput(['maxlength' => 255, 'placeholder' => 'Judet'])->label(false) ?>
<?= $form->field($model, 'category_id')->dropDownList($items, ['prompt'=>'Alege traseul/categoria de vârstă...'])->label(false) ?>
<?= $form->field($model, 'email')->textInput(['maxlength' => 255, 'placeholder' => 'Adresa de email'])->label(false) ?>
<?= $form->field($model, 'phone')->textInput(['maxlength' => 20, 'placeholder' => 'Număr de telefon'])->label(false) ?>
<?php if(Subscriber::damTricou()) : ?>
<?= $form->field($model, 'tshirt_size')->dropDownList($tshirtSizes, ['prompt'=>'Alege mărimea tricoului...'])->label(false) ?>
<p class="small-text grey-color">Acest tricou îl vei primi în pachetul de înscriere.</p>
<?php endif; ?>

<?= $form->field($model, 'agreeWithRegulation')->checkbox(['label' => 'Sunt de acord cu ' . Html::a('Regulamentul concursului', Url::to(['site/regulation']), ['target' => '_blank'])]) ?>
<?= $form->field($model, 'agreeWithGDPR')->checkbox(['label' => 'Bifand aceasta casuta declar ca sunt de acord ca datele mele personale, completate in formularul de mai sus sa fie folosite in scopuri de informare cu privire la concursul Mamut Bike Race 2019 si toate aspectele legate de acesta']) ?>
<?= $form->field($model, 'insurance')->checkbox(['label' => 'Vreau asigurare de la Allianz Țiriac pentru concurs. Prețul este de 10 lei și se va achita în ziua evenimentului la standul Allianz Țiriac']) ?>
<?= Html::submitButton('Înscrie-mă', ['class' => 'btn-2', 'name' => 'register-button', 'id' => 'register-button']) ?>

<?php ActiveForm::end(); ?>
