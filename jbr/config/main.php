<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-jbr',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'jbr\controllers',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info', 'error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                'despre' => 'site/about',
                'traseu' => 'site/track',
                'inscriere' => 'site/register',
                'participanti' => 'site/riders',
                'regulament' => 'site/regulation',
                'contact' => 'site/contact',
                'declaratie' => 'site/declaratie',
                'program' => 'site/schedule',
                'cum-ajung' => 'site/maps',
                'clasament' => 'site/rankings',
                'checkcnp' => 'site/checkcnp',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
    ],
    'aliases' => [
        '@files' => dirname(__DIR__) . DIRECTORY_SEPARATOR . 'files'
    ],
    'params' => $params,
	
	'on beforeRequest' => function ($event) {
        if (isset(Yii::$app->params['siteMode']) && Yii::$app->params['siteMode'] == 'offline') {
            $letMeIn = Yii::$app->session['lasama'] || isset($_GET['lasama']);
            if (!$letMeIn) {
                Yii::$app->catchAll = ['site/offline'];
            } else {
                Yii::$app->session['lasama'] = 1;
            }
        }
    },
];
