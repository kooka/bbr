<?php
return [	
    //very important: this will be used in all project to filter data
	'currentEventYear' => '2022',
	'events' => [
        '2022' => [
            'name' => 'JBR2022',
        ],
        '2019' => [
            'name' => 'JBR2019',
        ],
        '2018' => [
            'name' => 'JBR2018',
        ],
		'2017' => [
			'name' => 'JBR2017',
		],
		'2016' => [
		    'name' => 'JBR2016',
        ],
		'2015' => [
			'name' => 'JBR2015',
			'categories' => [
				'35B' => '3-5 ani, băieți, traseu Iepurași', 
				'35F' => '3-5 ani, fete, traseu Iepurași', 
				'68B' => '6-8 ani, băieți, traseu Iepurași', 
				'68F' => '6-8 ani, fete, traseu Iepurași', 
				'911B' => '9-11 ani, băieți, traseu Lupișori', 
				'911F' => '9-11 ani, fete, traseu Lupișori', 
				'1214B' => '12-14 ani, băieți, traseu Lupișori', 
				'1214F' => '12-14 ani, fete, traseu Lupișori',
			],
		],
	],
	    
    'adminEmail' => 'admin@bicheru-cycling.ro',
    'jbrContactEmail' => 'junior@bicheru-cycling.ro',
	'jbrCurrentEventDateTime' => '21 Mai 2022, ora 10:00',
	'jbrCurrentEventDate' => '21 Mai 2022',
	'jbrStopRegistrationDateTime' => '20 Mai 2022, ora 11:00',
	'jbrCurrentLocation' => 'com. Ciugud, Alba',
    
    //after this date registration is not allowed
    'stopRegistrationDatetime' => mktime(11, 0, 0, 5, 20, 2022), //30 mai 2019 ora 11:00:00
    
    //ora si data la care se ascunde pe site meniul Inscrieri, Participanti, Au mai ramas..
    //si se afiseaza link spre viitorul Clasament
    'eventDayDatetime' => mktime(8, 30, 0, 5, 21, 2022), //1 iunie 2019 ora 8:30:00
    
    //over this number of subscribers, no more registration allowed; -1 no limits
    'maxRegistrationNumber' => 350,
];
