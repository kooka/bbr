<?php

/**
 * Description of _registerForm
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Apr 28, 2015
 * @encoding UTF-8 
 */


use yii\helpers\Html;
use \yii\helpers\Url;
use yii\widgets\ActiveForm;

$items = \common\models\Category::getCategoriesListItems();
?>

<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'pnc')->textInput(['placeholder' => 'Codul Numeric Personal'])->label(false)  ?>
<?= $form->field($model, 'last_name')->textInput(['maxlength' => 255, 'placeholder' => 'Numele de familie'])->label(false)  ?>
<?= $form->field($model, 'first_name')->textInput(['maxlength' => 255, 'placeholder' => 'Prenumele'])->label(false) ?>
<?= $form->field($model, 'city')->textInput(['maxlength' => 255, 'placeholder' => 'Localitatea'])->label(false) ?>
<?= $form->field($model, 'category_id')->dropDownList($items, ['prompt'=>'Alege categoria de vârstă...'])->label(false) ?>
<?= $form->field($model, 'email')->textInput(['maxlength' => 255, 'placeholder' => 'Adresa de email'])->label(false) ?>
<?= $form->field($model, 'phone')->textInput(['maxlength' => 20, 'placeholder' => 'Număr de telefon'])->label(false) ?>
<?= $form->field($model, 'agreeWithRegulation')->checkbox(['label' => 'Sunt de acord cu ' . Html::a('Regulamentul', Url::to(['site/regulation']), ['target' => '_blank']) . ' concursului.']) ?>
<?= $form->field($model, 'agreeWithGDPR')->checkbox(['label' => 'Bifand aceasta casuta declar ca sunt de acord ca datele mele personale, completate in formularul de mai sus sa fie folosite in scopuri de informare cu privire la concursul Junior Bike Race 2022 si toate aspectele legate de acesta']) ?>
<?= Html::submitButton('Înscrie-mă', ['class' => 'btn-2', 'name' => 'register-button', 'id' => 'register-button']) ?>

<?php ActiveForm::end(); ?>