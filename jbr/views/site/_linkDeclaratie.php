<?php

/**
 * Description of _linkDeclaratie
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  May 15, 2015
 * @encoding UTF-8 
 */
?>
Descarcă <strong><a href="<?= yii\helpers\Url::to(['site/declaratie']) ?>"><i class="fa fa-file-pdf-o">&nbsp;</i>Declarația pe proprie răspundere</a></strong>