<?php
use yii\grid\GridView;
?>
<style type="text/css">
    .gold {color: #D9A441;}
    .silver {color: #A8A8A8;}
    .bronze {color: #965A38;}    
</style>
<?php if(array_key_exists($key, $dataproviders)): ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataproviders[$key],
        'filterModel' => false,
        'filterPosition' => '',
        'summary' => '',
        'columns' => [
            [
                'attribute' => 'position_category',
                'format' => 'raw',
                'value' => function($data){
                    if ($data->position_category === 1) {
                        return $data->position_category . '&nbsp;<i class="fa fa-certificate gold"></i>';
                    } elseif($data->position_category === 2) {
                        return $data->position_category . '&nbsp;<i class="fa fa-certificate silver"></i>';
                    } elseif($data->position_category === 3) {
                        return $data->position_category . '&nbsp;<i class="fa fa-certificate bronze"></i>';                        
                    } else {
                        return $data->position_category;
                    }
                },
                'label' => 'Loc'
            ],
            [
                'attribute' => 'last_name',
                'value' => 'fullname',
                'label' => 'Nume'
            ],
            [
                'attribute' => 'final_time',
                'value' => 'time',
                'label' => 'Timp'
            ]
        ],
    ]);
    ?>
<?php else: ?>
            <div class="message error">Clasament indisponibil momentan.</div>
<?php endif; ?>