<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Junior Bike Race';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">

    <div class="wrapper blog-roll ad-pad clearfix">

        <!-- Start Main Column  -->
        <div class="col-1-1">

            <div class="clearfix post">


                <h1 class="title"><?= Html::encode($this->title) ?></h1>

                <p class="lead">
                    La prima ediție ne-am antrenat, la următoarele ne-am perfecționat,
                    iar acum suntem pregătiți să facem din Junior Bike Race una dintre cele mai frumoase
                    competiții cicliste pentru copii! Este un eveniment dedicat celor mici, dar și pentru părinți și prieteni,
                    pe care ne bazăm să-i încurajeze pe copii să se antreneze și să participe. Credem că este o ocazie
                    excelentă pentru mișcare în aer liber, joacă alături de alți copii și o zi care va rămâne între cele mai dragi amintiri
                    ale familiei.
                    Am pregătit trasee atât pentru începători, cât și pentru avansați, precum și surprize pentru toți participanții!
                    Desigur, merituoșii care vor urca pe podium vor avea premii pregătite de către partenerii și sponsorii noștri!
                    <br/><br/>Vă invităm să luați parte în 2022 la Junior Bike Race, ediția a VI-a!
                </p>

                <div class="col-2-5 right last pad-left">
                    <img class="" src="img/logo-jbr.png" alt="Junior Bike Race" />
                </div>
                
                <p>Cei mici vor avea la dispoziție 2 trasee, în funcție de vârstă. 
                    Așadar, cei sub 9 ani vor fi înscriși în „Traseul Iepurașilor” și vor avea de pedalat 550m,
                    cu o diferență de nivel de 5 metri. Pe  „Traseul Lupișorilor” vor pedala copiii de peste 9 ani,
                    fiecare categorie parcurgând un alt număr de ture din traseul de 1100 m lungime și 15 m diferență de nivel.
                    Lupișorii sunt deja cicliși cu experiență, așadar nu vor putea fi însoțiți pe traseu de către părinți, doar încurajați binențeles. </p>
                
                <p>Toți cei care doresc să participe trebuie să se înscrie în prealabil, completând <a href="<?= Url::to(['site/register']) ?>"><strong>FORMULARUL DE ÎNSCRIERE</strong></a>. 
                    Înscrierea este <strong>GRATUITĂ</strong>.</p>
                
                <p>Prin această inițiativă se urmărește încurajarea copiilor în a petrece cât mai mult timp în aer liber, pe bicicletă, 
                    totodată punându-le la încercare și abilitățile de pedalat. 
                    Îndemânarea pe bicicletă, crearea acelor instincte de coordonare dintre biciclist și bicicletă îl poate 
                    feri pe cel dintâi de multe situații neplăcute și neprevăzute întâlnite în timp ce se deplasează pe bicicletă. 
                    </p>
                
                <?= $this->render('_registerNowBtn'); ?>
                
            </div>



        </div>

    </div>
</div>