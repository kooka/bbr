<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Program de desfășurare';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">

    <div class="wrapper blog-roll ad-pad clearfix">

        <div class="col-2-3">
            <div class="clearfix post">

                <h1 class="title"><?= Html::encode($this->title) ?></h1>

                <table class="table-style-1">
                    <tbody>
                        <tr>
                            <td>8:00 - 9:00</td>
                            <td>Ridicare pachete de concurs înscriși online</td>
                        </tr>
                        <tr>
                            <td>9:00 - 10:00</td>
                            <td>Înscrieri concurenți la fața locului.<br/>Ridicare pachete de concurs cronologic, în ordinea sosirii concurenților.</td>
                        </tr>
                        <tr>
                            <td>10:00 - 10:30</td> 
                            <td>Ședință tehnică cu toți participanții și însoțitorii</td> 
                        </tr>
                        <tr>
                            <td>10:30</td>
                            <td>Start categorie 3-4 ani fete/băieți</td>
                        </tr>
                        <tr>
                            <td>10:50</td>
                            <td>Start categorie 5-6 ani fete/băieți</td>
                        </tr>
                        <tr>
                            <td>11:20</td>
                            <td>Start categorie 7-8 ani fete/băieți</td>
                        </tr>
                        <tr>
                            <td>12:00</td>
                            <td>Start categorie 9-10 ani fete/băieți</td>
                        </tr>
                        <tr>
                            <td>12:45</td>
                            <td>Start categorie 11-12 ani fete/băieți</td>
                        </tr>
                        <tr>
                            <td>13:40</td>
                            <td>Start categorie 13-14 ani fete/băieți</td>
                        </tr>
                        <tr>
                            <td>14:45</td>
                            <td>Premiere</td>
                        </tr>
                    </tbody>
                </table>

                <p>În funcție de numărul participanților și de condițiile meteorologice 
                    la data și locul concursului, programul prezentat poate fi modificat.</p> 
            </div>
        </div>

        <div class="col-1-3 last">

             <div class="widget">
                <h3>Ne găsiți si aici</h3>
                <ul class="widget-list list-1">
                    <li><?= Html::a('facebook.com/juniorbikerace', 'https://www.facebook.com/juniorbikerace', ['target' => '_blank'])?></li>
                    <li><?= Html::a('www.bicheru-cycling.ro', 'http://bicheru-cycling.ro', ['target' => '_blank'])?></li>
                </ul>
            </div>
            
            <div class="centered">
                <?= $this->render('_registerNowBtn'); ?>
            </div>
        </div>

    </div>
</div>
