<?php
/**
 * Description of _sliderHome
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Apr 20, 2015
 * @encoding UTF-8 
 */
?>
<!-- Revolution Slider -->
<div class="tp-banner-container">
    <div class="tp-banner" >
        <ul>
			<?php for ($index = 1; $index <= 6; $index++) : ?>
			<li data-transition="fade" data-masterspeed="500" >
                <img src="img/slides2018/slide0<?= $index ?>.jpg" alt="Junior Bike Race" data-bgposition="left center" data-kenburns="on" data-duration="14000" data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="110" data-bgpositionend="right center"/>
			</li>
			<?php endfor; ?>
        </ul>
    </div>
</div>
