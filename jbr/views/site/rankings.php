<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">

    <div class="wrapper blog-roll ad-pad clearfix">

        <!-- Start Main Column  -->
        <div class="col-1-1">

            <div class="clearfix post">

                <h1 class="title"><?= Html::encode($this->title) ?></h1>

                <?php if (!$isRanking): ?>
                    <div class="message error">Clasamentul nu este încă disponibil. Vom anunța momentul publicării lui. Vă rugăm reveniți.</div>
                <?php endif ?>

            </div>
        </div>

	<?php if ($isRanking): ?>
		
		<?php $row = 0; ?>
		<?php foreach ($categories as $categorySlug => $categoryDescription) : ?>
			<? if ($row % 2 == 0) $isRow = true; else $isRow = false; ?>
			<? if (($row+1) % 2 == 0) $isEndRow = true; else $isEndRow = false; ?>
		
			<? if ($isRow): ?>
			<div class="row">
			<? endif; ?>			
				
				<div class="col-1-2 <?= $isEndRow ? 'last': '' ?>">
                <h3><?= $categoryDescription?></h3>
                <img src="img/podium/<?= strtolower($event) ?>/<?= strtolower($categorySlug) ?>.jpg" alt=""/>
                <?= $this->render('_ranking', ['key' => $categorySlug, 'dataproviders' => $dataproviders]) ?>
				</div>	
			
			<? if ($isEndRow): ?>
			</div><!--end row-->
			<? endif; ?>				
			
			<? $row++; ?>
		<?php endforeach; ?>
			
	<?php endif ?>

    </div>

</div>