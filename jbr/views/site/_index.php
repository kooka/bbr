<?php
use yii\helpers\Url;
/**
 * Description of _index
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Jun 8, 2015
 * @encoding UTF-8 
 */
?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">		
    <div class="wrapper ad-pad clearfix">

        <!-- Column -->
        <div class="col-1-2">
            <h3>De ce să particip?</h3>

            <p class="lead">
            Am putea să-ți dăm nenumărate motive, însă te lăsăm pe tine să îl alegi pe cel pe care îl preferi:</p>
            
            <ul class="list-3">
                <li>O zi de neuitat alături de mulți copii, care abia așteaptă să pedaleze cu tine</li>
                <li>Șansa de a îmbina educația, școala și sportul</li>
                <li>Un concurs cu premii atractive, la care toată lumea va fi câștigătoare</li>
                <li>Tombolă organizată imediat după premiere</li>
                <li>Surprize colorate pregătite de prietenii noștri de la Ping și Pong</li>
            </ul>

            <p class="lead">Te așteptăm așadar în data de <strong><?= Yii::$app->params['jbrCurrentEventDate'] ?></strong>, pentru un <strong>eveniment unic</strong> în județul Alba!</p>
            <p class="lead">Nu uita să te înscrii din timp, pentru a ne ajuta să organizăm totul cât mai bine!</p>
            <?= $this->render('_registerNowBtn'); ?>

        </div>



        <!-- Column -->
        <div class="col-1-2 last">
            <h3>Trasee</h3>
            <div class="mosaic-block circle">
                <a href="<?= Url::to(['site/track']) ?>" class="mosaic-overlay fancybox link" title="Traseu Iepurași - sub 9 ani"></a><div class="mosaic-backdrop">
                    <div class="corner">varsta sub 9 ani</div><img src="img/traseu-jbr-2015-sub9ani.png" alt="Traseu Iepurasi" /></div>
            </div>
            <div class="mosaic-block circle">
                <a href="<?= Url::to(['site/track']) ?>" class="mosaic-overlay fancybox link" title="Traseu Lupișori - peste 9 ani"></a><div class="mosaic-backdrop">
                    <div class="corner">varsta peste 9 ani</div><img src="img/traseu-jbr-2015-peste9ani.png" alt="Traseu Lupișori" /></div>
            </div>
        </div>


    </div>
</div>