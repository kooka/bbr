<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Category;

/**
 * @var \common\models\Category $category
 *
 */

/* @var $this yii\web\View */
$this->title = 'Traseu';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">

    <div class="wrapper blog-roll ad-pad clearfix">

        <!-- Start Main Column  -->
        <div class="col-1-1">

            <div class="clearfix post">


                <h1 class="title"><?= Html::encode($this->title) ?></h1>
                <p class="lead">Concursul se desfășoară pe două trasee cu grade diferite de dificultate în funcție de vârsta participanților. Traseele vor fi stabilite şi amenajate special
                    înainte de data concursului. În funcție de natura terenului în ziua concursului, numărul de ture stabilite inițial poate suferi modificări.</p>
                <h3>Traseu Lupișori</h3>
                <div class="clearfix">
                    <div class="col-1-1">
                        <ul class="list-4">
                            <li>Lungime: 1100 m</li>
                            <li>Diferență de nivel: 15 m</li>
                        </ul>
                        <table class="table-style-1">
                            <thead>
                            <tr>
                                <th>Categorie</th>
                                <th>Născuți în anii</th>
                                <th>Nr. ture</th>
                                <th>Observații</th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach($categories as $category): ?>
                                <? if ($category->course->title == 'Lupișori'): ?>
                                <tr>
                                    <td><?= $category->title ?></td>
                                    <td><?= $category->getYearsRange()?></td>
                                    <td><?= $category->laps?></td>
                                    <td><?= $category->notes ?></td>
                                </tr>
                                <? endif; ?>
                            <? endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    
                    <!--div class="centered">
                        <img class="" src="img/traseu-jbr-2015-peste9ani.png" alt="Traseu JBR - Peste 9 ani" />
                    </div-->
                </div>

                <hr/>

                <h3>Traseu Iepurași</h3>
                <div class="clearfix">
                    <div class="col-1-1">
                        <ul class="list-4">
                            <li>Lungime: 550 m</li>
                            <li>Diferență de nivel: 5 m</li>
                        </ul>

                        <table class="table-style-1">
                            <thead>
                            <tr>
                                <th>Categorie</th>
                                <th>Născuți în anii</th>
                                <th>Nr. ture</th>
                                <th>Observații</th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach($categories as $category): ?>
                                <? if ($category->course->title == 'Iepurași') : ?>
                                <tr>
                                    <td><?= $category->title ?></td>
                                    <td><?= $category->getYearsRange()?></td>
                                    <td><?= $category->laps?></td>
                                    <td><?= $category->notes ?></td>
                                </tr>
                                <? endif; ?>
                            <? endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    
                    <!--div class="centered">
                        <img class="" src="img/traseu-jbr-2015-sub9ani.png" alt="Traseu JBR - Sub 9 ani" />
                    </div-->
                </div>
                
                

            </div>

        </div>
        
        <div class="clearfix">
            <div class="col-1-1">
                <div class="centered">
                    <?= $this->render('_registerNowBtn'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
