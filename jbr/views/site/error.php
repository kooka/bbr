<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<div class="outter-wrapper body-wrapper">

    <div class="wrapper blog-roll ad-pad clearfix">

        <div class="col-1-1 last">
            <div class="clearfix post">
				<div class="site-error">

					<h1><?= Html::encode($this->title) ?></h1>

					<div class="alert alert-danger">
						<?= nl2br(Html::encode($message)) ?>
					</div>

					<p>
						Eroarea de mai sus a apărut în urma procesării cererii tale.
					</p>
					<p>
						Dacă crezi că este o eroare a serverului nostru te rugăm să ne contactezi. Mulțumim.
					</p>

				</div>
			</div>
		</div>
	</div>
</div>


