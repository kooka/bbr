<?php
use common\models\Subscriber;
use yii\helpers\Url;

$showButton = true;

if (Subscriber::isEventDay()) {
    $showButton = false;
}

if (!Subscriber::registrationTimeAllowed()) {
    $showButton = false;
}

if (Subscriber::isMaxRegistrations()) {
    $showButton = false;
}
?>

<?php if ($showButton) : ?>
<a class="btn-2 btn-primary" href="<?= Url::to(['site/register']) ?>"><h4>ÎNSCRIE-TE ACUM, GRATUIT!</h4></a>
<?php endif ?>