<?php
/**
 * Description of _partners
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c)
 * @date  Apr 22, 2015
 * @encoding UTF-8
 */
use yii\helpers\Url;

$partners = [
    ['img' => 'logo-dacia.png', 'url' => '#', 'alt' => 'Dacia Market'],
    ['img' => 'logo-ctc.png', 'url' => '#', 'alt' => 'Cora Trade Center'],
    ['img' => 'logo-cora.png', 'url' => '#', 'alt' => 'Cora Trade Center'],
    ['img' => 'logo-asv.png', 'url' => '#', 'alt' => 'Alba Sport Vision'],
    ['img' => 'logo-tn.png', 'url' => 'https://www.transilvanianuts.ro/', 'alt' => 'Transilvania Nuts'],
    ['img' => 'logo-nb.jpg', 'url' => 'https://www.transilvanianuts.ro/', 'alt' => 'Nutribon'],
    ['img' => 'logo-pingsipong.png', 'url' => '#', 'alt' => 'Ping si Pong'],
    ['img' => 'logo-ballan.png', 'url' => 'http://ballansportswear.com', 'alt' => 'Ballan Sportswear'],
    ['img' => 'logo-alpinlux.png', 'url' => '#', 'alt' => 'Alpin 57 Lux'],
    ['img' => 'logo-alba24.png', 'url' => '#', 'alt' => 'Alba 24'],
    ['img' => 'logo-ziarul-unirea.png', 'url' => '#', 'alt' => 'Ziarul Unirea'],
//    ['img' => 'logo-djst.png', 'url' => '#', 'alt' => 'Directia Judeteana Sport si Tineret'],
    ['img' => 'logo-freerider.png', 'url' => '#', 'alt' => 'freerider.ro'],
//    ['img' => 'logo-zuzu.png', 'url' => '#', 'alt' => 'ZUZU - Albalact'],
];
?>
<!-- Start Carousel -->

<h3>Parteneri / Sponsori</h3>
<ul class="partners-list">
    <?php foreach ($partners as $partner) :?>
        <li class="item">
            <?php if ($partner['url'] == '#'): ?>
                <img src="img/partners/<?= $partner['img'] ?>" alt="<?= $partner['alt'] ?>" title="<?= $partner['alt'] ?>" style="max-width: 150px !important;">
            <?php else : ?>
                <a href="<?= $partner['url'] ?>" target="<?= array_key_exists('local', $partner) &&  $partner['local'] ? '_self':'_blank' ?>" title="<?= $partner['alt'] ?>">
                    <img src="img/partners/<?= $partner['img'] ?>" alt="<?= $partner['alt'] ?>" style="max-width: 150px !important;">
                </a>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
</ul>

