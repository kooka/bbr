<?php

/**
 * Description of offline
 *
 * @author		Claudiu Crisan
 * @date		16.3.2016
 * @encoding	UTF-8 
 */
?>

<div class="outter-wrapper body-wrapper">		
    <div class="wrapper ad-pad clearfix">
		<div class="col-1-1">
			<h1 class='center-block centered'>Pregătim următorul concurs.<br/><span style="color:#FF0000">21 Mai 2022</span><br/>Vă așteptăm.<span class='fa fa-smile-o' style="color:#9C3">&nbsp;</span></h1>
		</div>
	</div>
</div>
<?= $this->render('_sliderHomeOffline'); ?> 
