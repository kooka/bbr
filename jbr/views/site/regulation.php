<?php
use yii\helpers\Html;
use common\models\Category;

/**
 * @var \common\models\Category $category
 *
 */

/* @var $this yii\web\View */
$this->title = 'Regulament concurs';
$this->params['breadcrumbs'][] = $this->title;

?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">
    <div class="wrapper ad-pad clearfix">

        <!-- Start Main Column  -->
        <div class="main-content col-4-5 right last">

                <h1 class="title"><?= Html::encode($this->title) ?></h1>
                
                <h4 id="section1">1. Cadru general</h4>
                <p>1.1. Acest regulament se aplică participanţilor la concursul pentru copii "Junior Bike Race".</p>
                <p>1.2. Organizatorii evenimentului menționat sunt Primăria Comunei Ciugud şi Asociaţia Bicheru Cycling, denumiţi în
                    continuare “organizatori”.</p>
                <p>1.3. "Junior Bike Race" este o întrecere de tip MTB cross-country pentru copii, la care sunt admise biciclete de orice tip, în bună
                    stare de funcţionare, organizatorii recomandând totuşi biciclete tip mountain-bike.</p>
                <p>1.4. Participarea la concursul pentru copii "Junior Bike Race" este <strong>gratuită</strong>.
				Numărul maxim de participanți pentru această ediție este stabilit la 200 (douăsute).</p>
                <p>1.5. Înscrierea participanţilor se face online până în data de <?= Yii::$app->params['jbrStopRegistrationDateTime'] ?> sau la faţa locului, în ziua concursului, în limita locurilor disponibile.
                    Organizatorii încurajează înscrierea online. Concurenţii înscrişi online vor beneficia în pachetul de înscriere de <strong>buff personalizat "Junior Bike Race".</strong> Numărul buff-urilor oferite este limitat la 150 de bucăți.</p>
                <p>1.6. Prin semnarea formularului de înscriere fiecare participant declară că îşi dă acordul ca imaginea sa sau alte date
                publice (spre exemplu rezultatele obţinute) să fie folosite în materialele de promovare ale evenimentului sub formă de
                fotografii, afişe, filme, interviuri etc., în mod gratuit şi necenzurat.</p>
                <p>1.7. Organizatorul îşi rezervă dreptul de a refuza înregistrarea şi participarea la cursă, din motive obiective, vreunui
                    participant.</p>
                <p>1.8. Concursul se adresează în principal copiilor din județul Alba. Organizatorul va permite înregistrarea și participarea copiilor din alte județe, inclusiv a cluburilor și echipelor sportive.</p>
                <p>1.9. Organizatorul recomandă introducerea completă a datelor de contact pentru a putea comunica în caz de necesitate.</p>

                <h4 id="section2">2. Obligațiile participanților</h4>
                <p>2.1. Participarea la "Junior Bike Race" se face pe răspunderea reprezentantului legal al minorului, întărită prin semnarea
                declaraţiei pe propria răspundere a reprezentantului în faţa organizatorului.</p>
                <p>2.2. Fiecare participant este responsabil pentru propria securitate şi siguranţă. Organizatorii, partenerii, sponsorii,
                voluntarii şi personalul care participă la realizarea evenimentului nu sunt responsabili pentru orice fel de rănire, deces
                sau pagube de orice natură care pot surveni în timpul sau ca urmare a desfăşurării evenimentului.</p>
                <p>2.3. <strong>Casca de protecţie este obligatorie</strong> și trebuie purtată pe toată durata desfăşurării concursului. Refuzul purtării acesteia atrage descalificarea concurentului.</p>
                <p>2.4. Participanţii au obligaţia de a asculta şi de a respecta indicaţiile/instrucţiunile organizatorului şi ale personalului de
                concurs.</p>
                <p>2.5. Preluarea numărului de concurs şi a pachetului de înscriere se face la faţa locului, înaintea desfăşurării concursului,
                în intervalul orar prevăzut de către organizator.</p>
                <p>2.6. Organizatorul poate interzice luarea startului sau continuarea cursei pentru orice participant dacă acesta se prezintă
                în stare de slăbiciune evidentă, boală, epuizare fizică sau psihică.</p>
                
                
                <h4 id="section3">3. Pachetul de înscriere</h4>
                <p>3.1. Pachetul de înscriere include:
                <ul class="list-3">
                    <li>alimente şi băuturi în zona de start/sosire</li>
                    <li>buff personalizat JBR (NUMAI pentru cei înscrişi ONLINE)</li>
                    <li>număr de concurs și bride de fixare</li> 
                    <li>produse cu valoare simbolică oferite de sponsori</li>
                    <li>diplome şi medalii pentru premiaţi</li>
                    <li>medalie de finisher</li>
                </ul>
                </p>
                
                <h4 id="section4">4. Trasee</h4>
                <p>4.1. Concursul se desfășoară pe două trasee cu grade diferite de dificultate. Traseele vor fi stabilite şi amenajate special
                    înainte de data concursului.</p>
                <ul>
                    <li>a) Traseul 1. Lupişori. aprox. 1100m, aprox 15m diferență de nivel</li>
                    <li>b) Traseul 2. Iepuraşi. aprox. 550m, aprox. 5m diferență de nivel</li>
                </ul>
                
                <h4 id="section5">5. Categorii şi clasament</h4>
                <p>5.1. Participanţii se pot înregistra în următoarele categorii:</p>
                <table class="table-style-1">
                    <thead>
                        <tr>
                            <th>Categorie</th>
                            <th>Născuți în anii</th>
                            <th>Traseu</th>
                            <th>Nr. ture</th>
                            <th>Observații</th>
                        </tr>
                    </thead>
                    <tbody>
                    <? foreach($categories as $category): ?>
                        <tr>
                            <td><?= $category->title ?></td>
                            <td><?= $category->getYearsRange()?></td>
                            <td><?= $category->course->title ?></td>
                            <td><?= $category->laps?></td>
                            <td><?= $category->notes ?></td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
                
                <p>5.2. La categoria 3-4 ani sunt acceptate biciclete (inclusiv cu roți ajutătoare), biciclete fără pedale și triciclete (fără mâner de împingere).</p>
                <p>5.3. Concurenții de la categoria 3-4 ani care se pot descurca singuri, fără ajutorul părinților, vor lua startul din prima linie.</p>
                <p>5.4. Startul se dă separat pentru fete şi băieţi, la fiecare categorie.</p>
                <p>5.5. Se premiază primii trei clasaţi din fiecare categorie. Premiul constă în medalie şi diplomă, plus plasă de la sponsori.</p>
                <p>5.6. Fiecare participant care termină cursa primeşte medalie de finisher.</p>
                <p>5.7. Clasamentul se face în funcţie de ordinea la sosire.</p>
                <p>5.8. Numărul minim de participanţi la o categorie este de 3. În cazul în care nu se întruneşte acest număr, categoria va fi comasată cu
                cea mai apropiată categorie superioară validă.</p>
                <p>5.9. După premiere va fi organizată o tombolă cu premii. Vor putea participa concurenții prezenți la momentul desfășurării acesteia. În cazul în care numărul anunțat câștigator nu este prezent, se continuă extragerea până la desemnarea unui câștigător prezent.</p>

                <h4 id="section6">6. Proceduri de concurs</h4>
                <p>6.1. Şedinţa tehnică<br/>
                O şedinţă tehnică va fi ţinută pentru toţi participanţii în ziua startului. În cursul şedinţei tehnice participanţii vor fi
                informaţi asupra traseului, porţiunilor periculoase, eventualelor modificări, precum şi a regulilor de comportament pe
                care participanţii sunt obligaţi să le respecte. Şedinţa tehnică va fi ţinută de directorul de concurs pentru fiecare
                categorie în parte, la linia de start. Pentru fiecare categorie un organizator va exemplifica traseul prin parcurgerea
                efectivă a acestuia pe bicicletă.</p>
                <p>6.2. Numărul de concurs</p>
                <ul class="list-3">
                    <li>Preluarea numărului de concurs şi a pachetului de înscriere se face pe baza copiei după certificatul de naştere, în
                        apropierea zonei de Start/Sosire, la cortul organizatorilor.</li>
                    <li>Numărul de concurs trebuie montat pe ghidon şi trebuie să fie vizibil din faţă pe tot timpul concursului.</li>
                    <li>În caz de abandon numărul trebuie predat organizatorilor în zona de Start/Sosire.</li>
                </ul>
                <p>6.3. Startul<br/>
                Startul se va da în data şi ora stabilită de organizator şi anunţată pe site-urile competiţiei.</p>
                <p>6.4. Retragerea din concurs/Abandon</p>
                <ul class="list-3">
                    <li>Retragerea din concurs din alt motiv decât cel de accident se poate face doar la zona start/sosire. Concurentul care nu
                    mai poate sau nu doreşte să continue concursul are obligaţia să se deplaseze până la punctul de start/sosire, predă
                    numărul de concurs şi este considerat retras.</li>
                    <li>În caz de pană, reparaţii, căzătură, accidentare etc. traseul de concurs trebuie eliberat cât mai repede pentru a face
                        posibilă trecerea celorlalţi concurenţi sau a echipelor de salvare.</li>
                </ul>

                <h4 id="section7">7. Echipament obligatoriu</h4>
                <p>7.1. Echipamentul obligatoriu este compus din casca de protecţie şi numărul de concurs (care este pus la dispoziţie de
                către organizator).</p>

                <h4 id="section8">8. Penalizări/Descalificări</h4>
                <p>8.1. Vor duce la descalificare următoarele acţiuni:</p>
                <ul class="list-3">
                    <li>lipsa echipamentului obligatoriu.</li>
                    <li>abaterea de pe traseul stabilit in vederea scurtării traseului.</li>
                    <li>schimbarea numărului de concurs</li>
                    <li>limbaj vulgar, înjurături.</li>
                </ul>
                <p>8.2. Descalificările se pot face și după concurs în urma vizualizării pozelor, filmelor sau a sesizării fondate a altor concurenți.</p>

                <h4 id="section9">9. Reclamaţii/Contestaţii</h4>
                <p>9.1. Reclamaţiile şi contestaţiile se depun în scris comitetului de concurs în decurs de 30 minute de la finish-ul oficial.</p>

                <h4 id="section10">10. Modificarea traseului, alte decizii privind desfăşurarea concursului</h4>
                <p>10.1. Organizatorul poate modifica oricând traseul sau poate anula/opri competiţia dacă aceasta nu se poate desfăşura în
                condiţii de siguranţă.</p>
                <p>10.2. Orice decizie va fi luată de comitetul de concurs sau de directorul de concurs.</p>
                <p>10.3. În caz de forță majoră, organizatorii pot lua decizia unor modificări de traseu sau program. La limită
                se poate decide inclusiv amânarea sau anularea cursei, în situații excepționale.</p>                    
                <p>10.4. Organizatorul îşi rezervă dreptul de a aduce modificări asupra informaţiilor afişate pe site, 
                    a regulamentului sau a programului, etc. Aceste modificări vor fi aduse la cunoştinţă participanţilor cu minim 1 zi 
                    înaintea concursului sau cel târziu la preluarea numerelor de concurs.</p>
				<p>10.5. Asociația “Bicheru Cycling” organizează în data de <?= Yii::$app->params['jbrCurrentEventDate'] ?> evenimentul sportiv de ciclism Junior Bike Race.
					Organizatorul Asociația “Bicheru Cycling” este o asociație non-profit, fără scop comercial/lucrativ și fără angajați. 
					Concursul Junior Bike Race nu este un produs sau serviciu oferit contracost, ci este o manifestare sportivă pentru amatori 
					a cărei organizare și desfășurare este posibilă datorită exclusiv contribuțiilor sponsorilor/partenerilor, precum și datorită 
					pasiunii noastre pentru mișcarea în natură, a muncii în afara joburilor permanente și a dedicației voluntarilor noștri.</p>
                <hr/>
                <p><?= $this->render('_linkDeclaratie')?></p>
            </div>

        
        <!-- Start Main Sidebar  -->
        <aside class="col-1-5">
            <div class="widget">
                <h3 class="widget-title">Cuprins</h3>
                <ul class="widget-list list-1">
                    <li><a href="#section1">Cadru general</a></li>
                    <li><a href="#section2">Obligațiile participanților</a></li>
                    <li><a href="#section3">Pachetul de înscriere</a></li>
                    <li><a href="#section4">Trasee</a></li>
                    <li><a href="#section5">Categorii şi clasament</a></li>
                    <li><a href="#section6">Proceduri de concurs</a></li>
                    <li><a href="#section7">Echipament obligatoriu</a></li>
                    <li><a href="#section8">Penalizări/Descalificări</a></li>
                    <li><a href="#section9">Reclamaţii/Contestaţii</a></li>
                    <li><a href="#section10">Dispoziții finale</a></li>
                </ul>	
            </div>	
        </aside> 
        <!-- Finish Sidebar -->        

    </div>
</div>