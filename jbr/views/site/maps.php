<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Cum ajung la concurs?';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">

    <div class="wrapper blog-roll ad-pad clearfix">

        <div class="col-1-1 last">
            <div class="clearfix post">

                <h1 class="title"><?= Html::encode($this->title) ?></h1>

                <h3>Din Alba Iulia</h3>
                <p>Dacă plecați din Alba Iulia există două variante: <br/>
                <ul>
                    <li>Ruta 1: Alba Iulia - Drâmbar - Șeușa - Ciugud</li>
                    <li>Ruta 2: Alba Iulia - Partoș - Oarda de Jos - Limba - Ciugud</li>
                </ul>
                <img src="img/harta-jbr-ciugud-din-alba-iulia.jpg" alt="Plecare din Alba Iulia"/>
                </p>

                <h3>Dinspre Cluj-Napoca</h3>
                <p>Dacă veniți dinspre Cluj-Napoca, înainte de intrarea în Alba Iulia, faceți stânga pe Șoseaua de Centură. 
                    Continuați pe Centură până la prima intersecție, virați stânga și continuați până la podul peste Mureș.
                    Treceți podul și imediat după pod virați dreapta. Mergeți tot înainte prin Drâmbar, Șeușa, după care ajungeți în Ciugud, lângă terenul de fotbal.
               <br/>
                <img src="img/harta-jbr-ciugud-dinspre-cluj.jpg" alt="Dinspre Cluj-Napoca"/>
                </p>
                
                <h3>Dinspre Sebeș</h3>
                <p>Dacă veniți dinspre Sebeș, înainte de intrarea în Alba Iulia, virați dreapta spre Oarda, înainte de benzinaria Petrom. 
                    Mergeți tot înainte prin Oarda de Jos, ajungeți în Limba, după care ajungeți în Ciugud, continuați înainte prin Ciugud până lângă terenul de fotbal.
               <br/>
                <img src="img/harta-jbr-ciugud-dinspre-sebes.jpg" alt="Dinspre Sebeș"/>
                </p>
                
                           
            </div>
        </div>

    </div>
</div>
