<?php
use yii\helpers\Html;
use common\models\Subscriber;

/* @var $this yii\web\View */
$this->title = 'Înscriere';
$this->params['breadcrumbs'][] = $this->title;

$errorMsg = '';
$isRegistrationExpired = false;
if (Subscriber::isMaxRegistrations()) {
    $errorMsg = 'Numărul maxim de participanți a fost atins. Nu se mai pot face alte înscrieri online.';
    $isRegistrationExpired = true;
}

if (!Subscriber::registrationTimeAllowed()) {
    $errorMsg = 'Perioada de înscrieri online a expirat. Vă așteptăm la concurs.';
    $isRegistrationExpired = true;
}
?>

<!-- Start Outter Wrapper -->
<div class="outter-wrapper body-wrapper">

    <div class="wrapper blog-roll ad-pad clearfix">

        <!-- Start Main Column  -->
        <div class="half" style="margin: 0 auto; float: none;">

            <h1 class="title"><?= Html::encode($this->title) ?></h1>
            
            <?php if ($isRegistrationExpired) : ?>
            <div class="message error">
                <h4><?= $errorMsg ?></h4>
            </div>
            <?php else : ?>
            <?= $this->render('_registerForm', ['model' => $model]) ?>
            <?php endif; ?>
            
            <hr/>
            <p><?= $this->render('_linkDeclaratie')?></p>
        </div>
        
    </div>
</div>
