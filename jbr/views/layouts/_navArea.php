<?php
/**
 * Description of _navArea
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Apr 20, 2015
 * @encoding UTF-8 
 */
use yii\helpers\Url;

?>
<!-- Post Header Area -->
<div class="outter-wrapper nav-container post-header-area header-style-3">

    <!-- Start Mobile Menu Icon -->
    <div id="mobile-header" class="">
        <a id="responsive-menu-button" href="#sidr-main">
            <em class="fa fa-bars"></em> Meniu
        </a>
    </div>

    <div id="navigation" class="clearfix">

        <div class="post-header-center centered-menu">
            
            <nav class="nav megamenu_container wrapper">
                <ul id="nav" class="centered-menu megamenu">
                    <?php if (common\models\Subscriber::isEventDay()) : ?>    
                        <li><a href="<?= Url::to(['site/rankings']) ?>">Clasament</a></li>
                    <?php else : ?>
                        <li><a href="<?= Url::to(['site/register']) ?>">ÎNSCRIERE</a></li>
                        <li><a href="<?= Url::to(['site/riders']) ?>">Participanți</a></li>
                    <?php endif; ?>
                    <li><a href="<?= Url::to(['site/schedule']) ?>">Program</a></li>
                    <li><a href="<?= Url::to(['site/about']) ?>">Despre JBR</a></li>
                    <li><a href="<?= Url::to(['site/track']) ?>">Traseu</a></li>
                    <li><a href="<?= Url::to(['site/maps']) ?>">Cum ajung?</a></li>
                    <li><a href="<?= Url::to(['site/regulation']) ?>">Regulament</a></li>
                </ul>
            </nav>            

        </div>

    </div>
</div>