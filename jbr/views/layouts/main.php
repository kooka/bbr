<?php

use yii\helpers\Html;
use app\assets\JbrAppAsset;
JbrAppAsset::register($this);

/* @var $this yii\web\View */
/* @var $content string */

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="<?= Yii::$app->language ?>"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta content="black" name="apple-mobile-web-app-status-bar-style">

        <!-- Page title + Description -->
        <?php if (!empty($this->title)) : ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php else: ?>
        <title>Junior Bike Race</title>
        <?php endif; ?>
        <meta name="description" content="Junior Bike Race, Ciugud, Alba">

        <!-- Favicons -->
        <link rel="apple-touch-icon" sizes="57x57" href="favicons/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="114x114" href="favicons/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="72x72" href="favicons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="144x144" href="favicons/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="60x60" href="favicons/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="76x76" href="favicons/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="152x152" href="favicons/apple-touch-icon-152x152.png">
        <link rel="icon" type="image/png" href="favicons/favicon-196x196.png" sizes="196x196">
        <link rel="icon" type="image/png" href="favicons/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="favicons/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="favicons/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="favicons/favicon-32x32.png" sizes="32x32">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="favicons/mstile-144x144.png">
        
        <link href='http://fonts.googleapis.com/css?family=Alegreya+SC:400,900|Roboto:400,700,900,400italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <?php $this->head() ?>    
    </head>
    <body>
        <?= $this->render('_analytics'); ?>
        
        <?php $this->beginBody() ?>
        
        <?= $this->render('_preHeaderArea'); ?>
        <?= $this->render('_headerArea'); ?>
        <?= $this->render('_navArea'); ?>


        <?= $content ?>


        <?= $this->render('_breadcrumbs'); ?>
        <?= $this->render('_footerContent'); ?>
        <?= $this->render('_footerCopyright'); ?>

        <?php $this->endBody()  ?>

    </body>
</html>


<?php $this->endPage() ?>