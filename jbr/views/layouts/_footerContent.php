<?php
/**
 * Description of _footerContent
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Apr 20, 2015
 * @encoding UTF-8 
 */
use yii\helpers\Url;

?>
<!-- Start Outter Wrapper -->
<div class="outter-wrapper footer-wrapper">		
    <div class="wrapper clearfix">
        <!-- Start Widget -->
        <div class="col-1-4 widget">
            <h3 class="widget-title">Organizator:</h3>
            <a class="logo" href="http://bicheru-cycling.ro" target="_blank">
                <img src="img/logo-bicheru.png" alt="Bicheru Cycling" />
            </a>	
        </div>

        <!-- Start Widget -->
        <div class="col-1-4 widget">
            <h3 class="widget-title">Partener:</h3>
            <a class="logo" href="http://primariaciugud.ro" target="_blank">
                <img style="margin-top: -30px;" src="img/logo-primaria-ciugud.png" alt="Primăria Ciugud, Alba" />
            </a>	
        </div>


        <!-- Start Widget -->
        <div class="col-1-4 widget">
            <h3 class="widget-title">Info</h3>
            <ul class="open-hours">
                <li>Data <span><?= Yii::$app->params['jbrCurrentEventDateTime'] ?></span></li>
                <li>Locația <span><?= Yii::$app->params['jbrCurrentLocation'] ?></span></li>
                <!--li>Contact <span><?= Yii::$app->params['jbrContactEmail'] ?></span></li-->
                <li>Descarcă<br/><strong><a href="<?= yii\helpers\Url::to(['site/declaratie']) ?>"><i class="fa fa-file-pdf-o">&nbsp;</i>Declarația pe proprie răspundere</a></strong></li>
            </ul>	
        </div>
		
        <!-- Start Widget -->
        <div class="col-1-4 widget last">
            <h3 class="widget-title">Arhiva JBR</h3>
            <ul class="open-hours">
                <li><a href="<?= Url::to(['site/rankings', 'year' => '2017']) ?>">2017 - Clasament</a>
				<li><a href="<?= Url::to(['site/rankings', 'year' => '2016']) ?>">2016 - Clasament</a>
				<li><a href="<?= Url::to(['site/rankings', 'year' => '2015']) ?>">2015 - Clasament</a>
            </ul>
        </div>

    </div>
</div>