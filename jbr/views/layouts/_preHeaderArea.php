<?php
/**
 * Description of _preHeaderArea
 *
 * @author  Claudiu Crisan <claudiu.crisan at gmail.com>
 * @copyright (c) 
 * @date  Apr 20, 2015
 * @encoding UTF-8 
 */
?>
<!-- Pre Header Area -->
<div class="outter-wrapper pre-header-area header-style-3">
    <div class="wrapper clearfix">

        <div class="pre-header-left left">
            <!-- Second Nav -->
            <ul>
                <li><a class="fa fa-home" title="Junior Bike Race" href="<?= Yii::$app->homeUrl ?>">&nbsp;</a></li>
                <li><a class="fa" title="Facebook" href="https://www.facebook.com/juniorbikerace" target="_blank">&#xf09a;</a></li>
            </ul>
        </div>
        
        <?php if (!common\models\Subscriber::isEventDay()) : ?> 
        <div class="pre-header-right right">
            <strong>Au mai rămas:</strong> <div class="countdown styled"></div> 
        </div>				
        <?php endif ?>
    </div>	
</div> 