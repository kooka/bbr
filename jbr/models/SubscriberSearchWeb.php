<?php

namespace jbr\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Subscriber;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * SubscriberSearch represents the model behind the search form about `mbr\models\Subscriber`.
 */
class SubscriberSearchWeb extends Subscriber
{
    public $category;
    public $course;
    
    public function __construct($config = array()) {
        parent::__construct($config);
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'pnc', 'year_of_birth'], 'integer'],
            [['first_name', 'last_name', 'gender', 'email', 'phone', 'city'], 'safe'],
            [['category', 'course'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {		
		$eventSlug = \common\models\Event::getEventSlugFromParams(isset($params['year']) ? $params['year'] : null);
				
        $query = new Query;
        
        $query  ->select([
                        'subscriber.*',
                        'category.title as category',
                        'course.title as course',
                        'event.title as event'
                    ])
                ->from('subscriber')
                ->join('INNER JOIN', 'category', 'category.id = subscriber.category_id')
                ->join('INNER JOIN', 'course', 'course.id = category.course_id')
                ->join('INNER JOIN', 'event', 'event.id = course.event_id')
                ->andWhere('event.slug=:event', [':event' => $eventSlug]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'key' => 'id',
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];
        
        $dataProvider->sort->attributes = [
            'id' => [
                'asc' => ['id' => SORT_ASC],
                'desc' => ['id' => SORT_DESC],                
                'label' => 'ID'
            ],
            'last_name' => [
                'asc' => ['last_name' => SORT_ASC],
                'desc' => ['last_name' => SORT_DESC],                
                'label' => 'Nume'
            ],
            'first_name' => [
                'asc' => ['first_name' => SORT_ASC],
                'desc' => ['first_name' => SORT_DESC],                
                'label' => 'Prenume'
            ],
            'city' => [
                'asc' => ['city' => SORT_ASC],
                'desc' => ['city' => SORT_DESC],                
                'label' => 'Localitate'
            ],
            'gender' => [
                'asc' => ['gender' => SORT_ASC],
                'desc' => ['gender' => SORT_DESC],                
                'label' => 'Sex'
            ],
            'category' => [
                'asc' => ['category' => SORT_ASC],
                'desc' => ['category' => SORT_DESC],
                'label' => 'Categorie'
            ],
            'course' => [
                'asc' => ['course' => SORT_ASC],
                'desc' => ['course' => SORT_DESC],
                'label' => 'Traseu'
            ],
        ];
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'gender' => $this->gender,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'category.title', $this->category])        
            ->andFilterWhere(['like', 'course.title', $this->course]);
        
        return $dataProvider;
    }
    
    public function getStatistics()
    {
		$eventSlug = \common\models\Event::getEventSlugFromParams();
		
        $data = \Yii::$app->db->createCommand("select count(s.id) as total, 
            c.title as category, 
            s.gender as sex, 
            t.title as course 
        from subscriber s
            inner join category c on s.category_id=c.id
            inner join course t on c.course_id=t.id
            inner join event e on t.event_id=e.id
        where e.slug=:event_slug
        group by s.category_id, s.gender
        order by c.id", array(':event_slug' =>$eventSlug))->queryAll();
            
        return $data;    
    }
}
