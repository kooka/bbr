<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Category;
use common\models\Subscriber;

/* @var $this yii\web\View */
/* @var $model app\models\Subscriber */
/* @var $form yii\widgets\ActiveForm */
/* @var $categories array */

$tshirtSizes = \common\models\Subscriber::getTshirtSizeOptions();

?>

<div class="subscriber-form">

    <?php $form = ActiveForm::begin(); ?>
	
    <?= $form->field($model, 'number')->textInput() ?>
    <?= $form->field($model, 'first_name')->textInput(['maxlength' => 255]) ?>   
    <?= $form->field($model, 'last_name')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'pnc')->textInput() ?>
    <?= $form->field($model, 'year_of_birth')->textInput() ?>
    <?= $form->field($model, 'category_id')->dropDownList($categories, ['prompt' => 'Selectează']); ?>
    
    <?= $form->field($model, 'city')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'gender')->dropDownList([ 'F' => 'F', 'M' => 'M', ], ['prompt' => 'Selectează']) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => 20]) ?>
    <?= $form->field($model, 'team')->textInput(['maxlength' => 255]) ?>	    
    <?= $form->field($model, 'tshirt_size')->dropDownList($tshirtSizes, ['prompt'=>'Alege mărimea tricoului...']) ?>

	 <?= $form->field($model, 'amount_to_pay')->textInput() ?>
    <?= $form->field($model, 'payed')->dropDownList([Subscriber::PAYED => 'Da', Subscriber::NOT_PAYED => 'Nu', ], ['prompt' => 'Selectează']) ?>
	 <?= $form->field($model, 'amount_payed')->textInput() ?>
    <?= $form->field($model, 'payment_description')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'payment_date')->textInput(['maxlength' => 255]) ?>
	
    <?= $form->field($model, 'email_sent')->dropDownList(['1' => 'Da', '0' => 'Nu', ], ['prompt' => 'Selectează']) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
