<?php
/**
 * paylist.php
 *
 * @author      Claudiu Crisan
 * @copyright   eins+null GmbH & Co. KG
 * @date        23/05/2017
 * @encoding    UTF-8
 */
use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Subscriber;

/* @var $this yii\web\View */
/* @var $searchModel \backend\models\SubscriberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista plătitori';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="subscriber-index">
    
    <h1><?= Html::encode($this->title) ?></h1>
    <?= Yii::$app->session->getFlash('success'); ?>
    
    <?= Html::beginForm(['subscriber/bulkpay'], 'post'); ?>
    <table cellpadding="3">
        <tr>
            <td>Suma:</td><td><?= Html::textInput('suma') ?></td>
            <td>Data platii:</td><td><?= Html::textInput('data', null, ['placeholder' => 'aaaa-ll-zz'])?></td>
            <td>Descriere plata:</td><td><?= Html::textInput('descriere')?></td>
            <td><?= Html::submitButton('Plateste', ['class' => 'btn btn-info',]); ?></td>
        </tr>
    </table>
    
    
    <?php
    
    $gridColumns = [
        ['class' => 'yii\grid\CheckboxColumn'],
        'id',
        [
            'attribute'=>'number',
            'label'=>'Numar'
        ],
        [
            'attribute' => 'event',
            'value' => 'event',
            'filter' => array('MBR2019' => "MBR2019"),
            'label' => 'Eveniment'
        ],
        [
            'attribute' => 'category',
            'value' => 'category',
            'label' => 'Categorie'
        ],
        [
            'attribute' => 'last_name',
            'value' => 'last_name',
            'label' => 'Nume'
        ],
        [
            'attribute' => 'first_name',
            'value' => 'first_name',
            'label' => 'Prenume'
        ],
        [
            'attribute' => 'payed',
            'format' => 'html',
            'contentOptions' => ['class' => 'centered'],
            'value' => function($data){
                return $data['payed'] === Subscriber::PAYED ? '<strong style="color:green;">DA</strong>' : '<strong style="color:red;">NU</strong>';
            },
            'filter' => array(Subscriber::PAYED => "Da", Subscriber::NOT_PAYED => "Nu"),
            'label' => 'Plătit?'
        ],
        [
            'attribute'=>'amount_payed',
            'label' => 'Suma platita'
        ],
        [
            'attribute'=>'payment_date',
            'label' => 'Data platii'
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:50px;'],
            'header' => 'Actions',
            'template' => '{update}',
        ]
        
    ];
    ?>
    
    <?php \yii\widgets\Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => "Total: {totalCount} înscriși",
        'emptyText' => "Nu am găsit rezultate",
        'tableOptions' => [
            'class' => 'table table-striped table-bordered'
        ],
        'columns' => $gridColumns
    ]);
    ?>
    <?php \yii\widgets\Pjax::end(); ?>
    
    <?= Html::endForm();?>
</div>
