<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Subscriber;
use yii\helpers\Url;
//use kartik\export\ExportMenu;
//use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubscriberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Înscriși';
$this->params['breadcrumbs'][] = $this->title;
$tshirtSizes = \common\models\Subscriber::getTshirtSizeOptions();
?>
<div class="subscriber-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?= Html::a('Înscrie o persoană', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    
    $gridColumnsExport = [
        'id',
        [
            'attribute' => 'event',
            'value' => 'event',
            'label' => 'Eveniment'
        ],
        [
            'attribute' => 'course',
            'value' => 'course',
            'label' => 'Traseu'
        ],
        [
            'attribute' => 'category',
            'value' => 'category',
            'label' => 'Categorie'
        ],
        [
            'attribute' => 'last_name',
            'value' => 'last_name',
            'label' => 'Nume'
        ],
        [
            'attribute' => 'first_name',
            'value' => 'first_name',
            'label' => 'Prenume'
        ],
        [
            'attribute' => 'city',
            'value' => 'city',
            'label' => 'Localitate'
        ],
        [
            'attribute' => 'pnc',
            'value' => 'pnc',
            'label' => 'CNP'
        ],
        [
            'attribute' => 'year_of_birth',
            'label' => 'An'
        ],
        [
            'attribute' => 'gender',
            'value' => 'gender',
            'filter' => array("M" => "Masculin", "F" => "Feminin"),
            'label' => 'Sex'
        ],
        'email:email',
        'phone',
        [
            'attribute' => 'created_at',
            'label' => 'Creat in'
        ]
    ];

    $gridColumns = [
        // ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'attribute' => 'event',
            'value' => 'event',
            'filter' => array('MBR2019' => 'MBR2019', 'JBR2019' => "JBR2019", 'MBR2018' => 'MBR2018', 'JBR2018' => "JBR2018", 'MBR2017' => "MBR2017", 'JBR2017' => "JBR2017", 'MBR2016' => "MBR2016", 'JBR2016' => "JBR2016", 'MBR2015' => "MBR2015", 'JBR2015' => "JBR2015"),
            'label' => 'Eveniment'
        ],
//        [
//            'attribute' => 'course',
//            'value' => 'course',
//            'label' => 'Traseu'
//        ],
        [
            'attribute' => 'category',
            'value' => 'category',
            'label' => 'Categorie'
        ],
        'number',
        [
            'attribute' => 'last_name',
            'value' => 'last_name',
            'label' => 'Nume'
        ],
        [
            'attribute' => 'first_name',
            'value' => 'first_name',
            'label' => 'Prenume'
        ],
        [
            'attribute' => 'city',
            'value' => 'city',
            'label' => 'Localitate'
        ],
//        [
//            'attribute' => 'pnc',
//            'value' => 'pnc',
//            'label' => 'CNP'
//        ],
//        [
//            'attribute' => 'year_of_birth',
//            'label' => 'An'
//        ],
//        [
//            'attribute' => 'gender',
//            'value' => 'gender',
//            'filter' => array("M" => "Masculin", "F" => "Feminin"),
//            'label' => 'Sex'
//        ],
        'email',
//        'phone',
        [
            'attribute' => 'payed',
            'format' => 'html',
            'contentOptions' => ['class' => 'centered'],
            'value' => function($data){
                return $data['payed'] === Subscriber::PAYED ? '<strong style="color:green;">DA</strong>' : '<strong style="color:red;">NU</strong>'; 
            },
            'filter' => array(Subscriber::PAYED => "Da", Subscriber::NOT_PAYED => "Nu"),
            'label' => 'Plătit?'
        ],
        [
            'attribute' => 'tshirt_size',
            'format' => 'html',
            'contentOptions' => ['class' => 'centered'],
            'filter' => $tshirtSizes,
            'label' => 'Tricou'
        ],
//        [
//            'attribute' => 'created_at',
//            'label' => 'Creat in'
//        ],

        [
            'contentOptions' => ['class' => 'centered', 'style' => 'width:50px;'],
            'label' => 'Declaratie',
            'format' => 'raw',
            'value' => function($model) {
                    return Html::a('', Url::to(['print',"id" => $model["id"]]), ["target" => "_blank", "class" => "glyphicon glyphicon-print"]);
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:50px;'],
            'header' => 'Actions',
            'template' => '{update} {delete}',
        ]
    ];
//    $isFa = true;
//    $exportFileName = 'subscriber-export_'.date('m-d-Y_hia');
//    $defaultExportConfig = [
//        ExportMenu::FORMAT_CSV => [
//            'label' => 'CSV',
//            'icon' => $isFa ? 'file-code-o' : 'floppy-open',
//            'iconOptions' => ['class' => 'text-primary'],
//            'showHeader' => true,
//            'showPageSummary' => true,
//            'showFooter' => true,
//            'showCaption' => true,
//            'filename' => $exportFileName,
//            'alertMsg' => 'The CSV export file will be generated for download.',
//            'options' => ['title' => 'Comma Separated Values'],
//            'mime' => 'application/csv',
//            'config' => [
//                'colDelimiter' => ";",
//                'rowDelimiter' => "\r\n",
//            ]
//        ],
//        ExportMenu::FORMAT_TEXT => [
//            'label' => 'Text',
//            'icon' => $isFa ? 'file-text-o' : 'floppy-save',
//            'iconOptions' => ['class' => 'text-muted'],
//            'showHeader' => true,
//            'showPageSummary' => true,
//            'showFooter' => true,
//            'showCaption' => true,
//            'filename' => $exportFileName,
//            'alertMsg' => 'The TEXT export file will be generated for download.',
//            'options' => ['title' => 'Tab Delimited Text'],
//            'mime' => 'text/plain',
//            'config' => [
//                'colDelimiter' => "\t",
//                'rowDelimiter' => "\r\n",
//            ]
//        ],
//        ExportMenu::FORMAT_EXCEL => [
//            'label' => 'Excel',
//            'icon' => $isFa ? 'file-excel-o' : 'floppy-remove',
//            'iconOptions' => ['class' => 'text-success'],
//            'showHeader' => true,
//            'showPageSummary' => true,
//            'showFooter' => true,
//            'showCaption' => true,
//            'filename' => $exportFileName,
//            'alertMsg' => 'The EXCEL export file will be generated for download.',
//            'options' => ['title' => 'Microsoft Excel 95+'],
//            'mime' => 'application/vnd.ms-excel',
//            'config' => [
//                'worksheet' => 'ExportWorksheet',
//                'cssFile' => ''
//            ]
//        ],    
//        ExportMenu::FORMAT_HTML => false,
//        ExportMenu::FORMAT_PDF => false,
//        ExportMenu::FORMAT_EXCEL_X => false,
////        ExportMenu::FORMAT_HTML => [
////            'label' => 'HTML',
////            'icon' => $isFa ? 'file-text' : 'floppy-saved',
////            'iconOptions' => ['class' => 'text-info'],
////            'showHeader' => true,
////            'showPageSummary' => true,
////            'showFooter' => true,
////            'showCaption' => true,
////            'filename' => $exportFileName,
////            'alertMsg' => 'The HTML export file will be generated for download.',
////            'options' => ['title' => 'Hyper Text Markup Language'],
////            'mime' => 'text/html',
////            'config' => [
////                'cssFile' => 'http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css'
////            ]
////        ],
//    ];
    // Renders a export dropdown menu    
//    echo ExportMenu::widget([
//        'dataProvider' => $dataProvider,
//        'columns' => $gridColumnsExport,
//        'target' => ExportMenu::TARGET_BLANK,   
//        'exportConfig' => $defaultExportConfig,
//    ]);

//    echo GridView::widget([
//        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
//        'columns' => $gridColumns,
//        'responsive' => true,
//        'hover' => true
//    ]);
    ?>
    
    <?php \yii\widgets\Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => "Total: {totalCount} înscriși",
        'emptyText' => "Nu am găsit rezultate",
        'tableOptions' => [
            'class' => 'table table-striped table-bordered'
        ],
        'columns' => $gridColumns
    ]);
    ?>
    <?php \yii\widgets\Pjax::end(); ?>

</div>
