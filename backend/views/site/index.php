<?php
/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'Bicheru Bike Race';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Bicheru Bike Race</h1>

        <p class="lead">Cycling race app.</p>

        <p>
            <a class="btn btn-lg btn-success" href="<?= Url::to(['event/index']) ?>">Evenimente</a>
            <a class="btn btn-lg btn-success" href="<?= Url::to(['course/index']) ?>">Trasee</a>
            <a class="btn btn-lg btn-success" href="<?= Url::to(['category/index']) ?>">Categorii</a>
            <a class="btn btn-lg btn-success" href="<?= Url::to(['subscriber/index']) ?>">Inscrisi</a>
            <a class="btn btn-lg btn-success" href="<?= Url::to(['subscriber/download', 'event' => 'MBR2019']) ?>">Download Inscrisi</a>
            <a class="btn btn-lg btn-success" href="<?= Url::to(['ranking/index']) ?>">Clasamente</a>
        </p>
    </div>

    <!--div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div-->
</div>
