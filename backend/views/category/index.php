<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categorii';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crează Categorie', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'event',
                'value' => 'event' ,
                'label' => 'Eveniment'
            ],
            [
                'attribute' => 'course',
                'value' => 'course',                
                'label' => 'Traseu'
            ],
            [
                'attribute' => 'title',
                'value' => 'title',                
                'label' => 'Categorie'
            ],
            [
                'attribute' => 'slug',
                'value' => 'slug',                
                'label' => 'Slug'
            ],
            [
                'attribute' => 'start_year',
                'value' => 'start_year',                
                'label' => 'De la'
            ],
            [
                'attribute' => 'end_year',
                'value' => 'end_year',                
                'label' => 'Pana la'
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width:50px;'],
                'header'=>'Actions',
                'template' => '{update} {delete}',
            ]
            
        ],
    ]); ?>

</div>
