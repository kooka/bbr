<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Course;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
        $items = ArrayHelper::map(Course::find()->all(), 'id', 'titleCategoryEvent');
        echo $form->field($model, 'course_id')->dropDownList($items);
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'start_year')->textInput() ?>
    <?= $form->field($model, 'end_year')->textInput() ?>
	<?= $form->field($model, 'gender')->dropDownList([ 'F' => 'F', 'M' => 'M', ], ['prompt' => 'Selectează sau lasă gol', 'empty' => '--']) ?>
    <?= $form->field($model, 'laps')->textInput() ?>
    <?= $form->field($model, 'open')->textInput(['maxlength' => 1]) ?>
    <?= $form->field($model, 'notes')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
