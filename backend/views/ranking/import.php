<?php

use yii\bootstrap\ActiveForm;

use yii\helpers\Html;

$eventsList = ['MBR2019' => 'Mamut Bike Race 2019', 'JBR2019' => 'Junior Bike Race 2019'];
?>

<div class="event-form">
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'importCsvForm']]) ?>
    
    <?= $form->field($model, 'event')->dropDownList($eventsList) ?>
    <?= $form->field($model, 'file')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Importa rezultatele', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end() ?>  
</div>
