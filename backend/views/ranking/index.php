<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use common\models\Event;
use common\models\Ranking;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RankingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clasamente';
$this->params['breadcrumbs'][] = $this->title;

$events = ArrayHelper::map(Event::find()->all(), 'slug', 'title');
$distinctArray = Ranking::find()->distinct(true)->all();  
$courses = ArrayHelper::map($distinctArray, 'course_slug', 'course_title');
$categories = ArrayHelper::map($distinctArray, 'category_slug', 'category_title');

?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
<?php if (Yii::$app->session->getFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo Yii::$app->session->getFlash('success') ?>
    </div>
<?php endif; ?>

<?php if (Yii::$app->session->getFlash('error')): ?>
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo Yii::$app->session->getFlash('error') ?>
    </div>
<?php endif; ?>
    
    <div class="toolbar"style="margin:10px 0;">
        <?= Html::button('Importa rezultate', [
            'value' => Url::to(['ranking/import']),
            'id' => 'importRankingsBtn',
            'class' => 'btn btn-success btn-primary']) ?>
		<?php foreach (Event::find()->all() as $event) : ?>
		
		<?= Html::button("Sterge rezultate {$event->slug}", [
            'data-url' => Url::to(['ranking/reset']),
            'data-event' => $event->slug,
			'disabled' => date('Y', strtotime($event->start_date)) < date('Y') ? true : false,
            'id' => 'deleteRankings'.$event->slug,
            'data-confirmation' => "Esti sigur ca vrei sa stergi toate rezultatele de la {$event->slug}?",
            'class' => "btn btn-danger deleteRankingsBtn"]) ?>
		<?php endforeach; ?>       
    </div>
    
    <?php 
 Modal::begin([
     'header' => 'Import rezultate',
     'id' => 'modalImport',
 ]);
 
 echo '<div id="modalContent"></div>';
 Modal::end();
    ?>
    
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'event_slug',
                'value' => 'event_title',
                'filter' => $events,
                'label' => 'Eveniment'
            ], 
            [
                'attribute' => 'course_slug',
                'value' => 'course_title',
                'filter' => $courses,
                'label' => 'Traseu'
            ], 
            [
                'attribute' => 'category_slug',
                'value' => 'category_title',
                'filter' => $categories,
                'label' => 'Categorie'
            ], 
            'first_name', 
            'last_name', 
            'team', 
            'position_category',
            [
                'attribute' => 'final_time',
                'value' => 'time',
                'label' => 'Timp'
            ],
        ],
    ]); ?>

</div>
