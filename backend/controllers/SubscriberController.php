<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use common\models\Subscriber;
use backend\models\SubscriberSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Event;
/**
 * SubscriberController implements the CRUD actions for Subscriber model.
 */
class SubscriberController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [                
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Subscriber models.
     * @return mixed
     */
    public function actionIndex()
    {
	    $searchModel = new SubscriberSearch();
		$sql = "select * from event WHERE start_date > now() order by start_date asc limit 1";
		$event = Event::findBySql($sql, [])->one();
		if ($event) {
			$searchModel->event = $event->slug;
		}
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Lists all Subscriber models.
     * @return mixed
     */
    public function actionPaylist()
    {
        $searchModel = new SubscriberSearch();
        $sql = "select * from event order by id desc";
        $event = Event::findBySql($sql, [])->one();
        if ($event) {
            $searchModel->event = $event->slug;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->setPageSize(100);
        
        return $this->render('paylist', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionBulkpay()
    {
        $suma = Yii::$app->request->post('suma', 0);
        $suma = (int) $suma;
        
        $data = Yii::$app->request->post('data', date('Y-m-d'));
        if (!isset($data) || empty($data)) {
            $data = date('Y-m-d');
        }
        $data = date('Y-m-d', strtotime($data));
        
        $descriere = Yii::$app->request->post('descriere');
        if (!($suma > 0) && empty($descriere)) {
            $descriere = "GRATUIT";
        }
        
        $selection = (array)Yii::$app->request->post('selection');
        $updated = array();
        foreach ($selection as $id) {
            $subscriber = Subscriber::findOne((int) $id);
            if (!$subscriber) continue;
            $subscriber->payed = 1;
            $subscriber->amount_payed = $suma;
            $subscriber->payment_date = $data;
            $subscriber->payment_description = $descriere;
            if (!$subscriber->validate()) continue;
            if ($subscriber->save(false)) {
                $updated []= $subscriber->id;
            }
        }
        $count = count($updated);
        $ids = implode(',', $updated);
        Yii::$app->session->setFlash('success', "Ai marcat plata de {$suma} pentru {$count} participanti. IDs: {$ids}");
        
        $this->redirect(['subscriber/paylist']);
    }

    /**
     * Displays a single Subscriber model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Subscriber model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Subscriber();
		$model->payed = Subscriber::NOT_PAYED;
		$model->email_sent = '0';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
            //return $this->redirect(['view', 'id' => $model->id]);
        } else {
			$categories = \common\models\Category::getCategoriesListItems();
            return $this->render('create', [
                'model' => $model, 'categories' => $categories
            ]);
        }
    }

    /**
     * Updates an existing Subscriber model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
            //return $this->redirect(['view', 'id' => $model->id]);
        } else {
			$event = $model->category->course->event->slug;
			$categories = \common\models\Category::getCategoriesListItems($event);

            return $this->render('update', [
                'model' => $model, 'categories' => $categories
            ]);
        }
    }

    /**
     * Deletes an existing Subscriber model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
//    public function actionDownload()
//    {
//        $searchModel = new SubscriberSearch();
//        $data = $searchModel->getForCsv();
//
//    }
    
    /**
     * Finds the Subscriber model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Subscriber the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Subscriber::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionDownload()
    {
        $event = Yii::$app->request->get('event', 'MBR2019');

        $sql = "select s.id as id_mbr, '' as 'nr_concurs',
            s.last_name as nume, s.first_name as prenume, t.title as traseu,
                    c.title as categorie,
                    s.pnc as cnp, s.gender as sex, s.city as localitate, s.phone as telefon,
                    IF((s.payed=1 and s.payment_date<=:dam_tricou), s.tshirt_size, '-') as tricou,
                    s.payed as platit, s.payment_date as data_platii, s.created_at as data_inscrierii
                    FROM
                    subscriber s INNER JOIN category c on c.id=s.category_id
                    inner join course t on t.id=c.course_id
                    inner join event e on e.id=t.event_id
                    where e.slug=:event_slug
                    order by t.title, s.id";
        
        $damTricou = date('Y-m-d', Yii::$app->params['damTricou']);
        
        $data = \Yii::$app->db->createCommand($sql, array(":event_slug" => $event, ":dam_tricou" => $damTricou))->queryAll();
        if (!empty($data)) {
            $file = tmpfile();
            $addHeader = true;
            foreach ($data as $row) {
                if ($addHeader) {
                    fputcsv($file, array_keys($row), ';');
                    $addHeader = false;
                }
                fputcsv($file, array_values($row), ';');
            }
            
            $filename = "inscrisi_{$event}_".date('Y-m-d'). ".csv";
            header('Pragma: public');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header("Content-type: application/csv; charset=UTF-8");
            header("Content-Disposition: attachment; filename={$filename}");
    
            rewind($file);
            while (!feof($file)) {
                echo fread($file, 1000);
            }
    
            fclose($file);
            Yii::$app->end();
        } else {
            echo "Nu s-au gasit rezultate. Nu am ce sa export.";
            Yii::$app->end();
        }
    }

    public function actionPrint($id)
    {
        $model = $this->findModel($id);
        $model->printDeclaratiePdf();
    }
}
