<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\Ranking;
use common\models\Event;
use common\models\RankingSearch;
use backend\models\ImportRankingsForm;
use yii\web\UploadedFile;

class RankingController extends Controller
{
    
    public function behaviors()
    {
        return [            
            'access' => [                
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        $searchModel = new RankingSearch();
		$searchModel->event_slug = Event::findOne(['year(start_date)' => date('Y')])->slug;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);        
    }
    
    public function actionImport()
    {
        $model = new ImportRankingsForm();
       
        if (Yii::$app->request->isPost) {

            $csvFile = UploadedFile::getInstance($model, 'file');
            $extension = strtolower(pathinfo($csvFile->name, PATHINFO_EXTENSION));
            $model->file = $csvFile;
            $model->event = Yii::$app->request->post('ImportRankingsForm')['event'];
            
            if ($model->validate()) { 
                $count = $this->importCsv($csvFile->tempName, $model->event);
                
                if ($count) {
                    Yii::$app->session->setFlash('success', $count . ' rezultate au fost importate');
                } else {
                    Yii::$app->session->setFlash('error', 'Nici un rezultat nu a fost importat');
                }

                return $this->redirect(['index']);
            }
        }

        return $this->renderAjax('import', ['model' => $model]);
    }
    
    private function importCsv($file, $event_slug)
    {
        $items = array();
        $attributes = array();
        $row = 1;
        
        if (($handle = fopen($file, "r")) === false) {
            return false;
        }
        
        while (($data = fgetcsv($handle, 1000, ';', '"')) !== false) {
            $num = count($data);
            $item = array();
            
            //echo "<p> $num fields in line $row: <br /></p>\n";
            if ($row === 1) {
                for ($c = 0; $c < $num; $c++) {
                    $attributes []= $data[$c];
                }
            } else {  
                for ($c = 0; $c < $num; $c++) {
                    //echo $data[$c] . "<br />\n";
                    $item []= $data[$c];
                }
            }
            
            $row++;
            if ($item !== array()) {
                $items []= $item;
            }
        }
            
//        echo 'attributes\n';
//        var_dump($attributes);die;
//        echo 'items\n';
//        var_dump($items);die;
        
        if ($items === array()) {
            return false;
        }
        
        return Yii::$app->db->createCommand()->batchInsert(Ranking::tableName(), $attributes, $items)->execute();

  
    }
    
    public function actionReset()
    {
        if (!Yii::$app->request->isPost)
        {
            throw new \yii\web\BadRequestHttpException;
        }
        
        $event = Event::findBySlug(Yii::$app->request->post('event'));
        
        $deleted = Ranking::deleteAll('event_slug=:event', [':event' => $event->slug]);
        if ($deleted > 0) {
            Yii::$app->session->setFlash('success', $deleted . ' records were deleted from Ranking table for event ' . $event->slug);
        } else {
            Yii::$app->session->setFlash('error', 'No records deleted from Ranking table for event '. $event->slug); 
        }
        
        return $this->redirect(['index']);
    }

}
