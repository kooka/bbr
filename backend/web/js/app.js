$(function(){
    $('#importRankingsBtn').click(function(){
        $('#modalImport').modal('show')
                .find('#modalContent')
                .load($(this).attr('value'));
        
    });
    
    $('.deleteRankingsBtn').click(function(){
        if (confirm($(this).data('confirmation'))) {
            $.post($(this).data('url'), {event: $(this).data('event')});
        }
    });
});