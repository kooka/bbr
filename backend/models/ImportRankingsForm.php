<?php

namespace backend\models;

use yii\base\Model;

/**
 * Description of ImportRankingsForm
 *
 * @author crisan
 */
class ImportRankingsForm extends Model 
{

    public $event;
    public $file;

    public function rules() 
    {
        return [
            [['event', 'file'], 'required'],
            [['file'], 'file'],
        ];
    }

    public function attributeLabels() 
    {
        return [
            'event' => 'Eveniment',
            'file' => 'Fisier CSV',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->file->saveAs('/uploads/import_' . date('m-d-Y_hia') . '.' . $this->file->extension);
            return true;
        } else {
            return false;
        }            
    }
}
