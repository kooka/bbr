<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Subscriber;
use yii\db\Query;

/**
 * SubscriberSearch represents the model behind the search form about `common\models\Subscriber`.
 */
class SubscriberSearch extends Subscriber
{
    public $category;
    public $course;
    public $event;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'pnc', 'year_of_birth'], 'integer'],
            [['first_name', 'last_name', 'gender', 'email', 'phone', 'city', 'event', 'payed', 'tshirt_size'], 'safe'],
            [['category', 'course', 'event'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
//        $query = Subscriber::find();
//        $query->joinWith(['category']);
//        
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);
//        
//        $dataProvider->sort->attributes['category'] = [
//            'asc' => ['category.title' => SORT_ASC],
//            'desc' => ['category.title' => SORT_DESC],
//        ];
    
//        $dataProvider->sort->attributes['course'] = [
//            'asc' => ['course.title' => SORT_ASC],
//            'desc' => ['course.title' => SORT_DESC],
//        ];
//    
//        $dataProvider->sort->attributes['event'] = [
//            'asc' => ['event.title' => SORT_ASC],
//            'desc' => ['event.title' => SORT_DESC],
//        ];
    
        $query = new Query;
        
        $query  ->select([
                        'subscriber.*',
                        'category.title as category',
                        'course.title as course',
                        'event.slug as event'
                    ])
                ->from('subscriber')
                ->join('INNER JOIN', 'category', 'category.id = subscriber.category_id')
                ->join('INNER JOIN', 'course', 'course.id = category.course_id')
                ->join('INNER JOIN', 'event', 'event.id = course.event_id');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'key' => 'id',
            //'pagination' => false
        ]);
        
        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];
        
        $dataProvider->sort->attributes = [
            'id' => [
                'asc' => ['id' => SORT_ASC],
                'desc' => ['id' => SORT_DESC],                
                'label' => 'ID'
            ],
            'number' => [
                'asc' => ['number' => SORT_ASC],
                'desc' => ['number' => SORT_DESC],                
                'label' => 'NR'
            ],
            'last_name' => [
                'asc' => ['last_name' => SORT_ASC],
                'desc' => ['last_name' => SORT_DESC],                
                'label' => 'Nume'
            ],
            'first_name' => [
                'asc' => ['first_name' => SORT_ASC],
                'desc' => ['first_name' => SORT_DESC],                
                'label' => 'Prenume'
            ],
            'city' => [
                'asc' => ['city' => SORT_ASC],
                'desc' => ['city' => SORT_DESC],                
                'label' => 'Localitate'
            ],
            'gender' => [
                'asc' => ['gender' => SORT_ASC],
                'desc' => ['gender' => SORT_DESC],                
                'label' => 'Sex'
            ],
            'year_of_birth' => [
                'asc' => ['year_of_birth' => SORT_ASC],
                'desc' => ['year_of_birth' => SORT_DESC],                
                'label' => 'An'
            ],
            'created_at' => [
                'asc' => ['created_at' => SORT_ASC],
                'desc' => ['created_at' => SORT_DESC],                
                'label' => 'Creat in'
            ],
            'category' => [
                'asc' => ['category' => SORT_ASC],
                'desc' => ['category' => SORT_DESC],
                'label' => 'Categorie'
            ],
            'course' => [
                'asc' => ['course' => SORT_ASC],
                'desc' => ['course' => SORT_DESC],
                'label' => 'Traseu'
            ],
            'event.slug' => [
                'asc' => ['event' => SORT_ASC],
                'desc' => ['event' => SORT_DESC],
                'label' => 'Eveniment'
            ]
        ];
        Yii::error($params);
				
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'number' => $this->number,
            'pnc' => $this->pnc,
            'year_of_birth' => $this->year_of_birth,
            'payed' => $this->payed,
            'tshirt_size' => $this->tshirt_size,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'course', $this->course])
            ->andFilterWhere(['like', 'event.slug', $this->event]);

        return $dataProvider;
    }
    
    public function getForCsv()
    {
        $data = \Yii::$app->db->createCommand("select 
            s.id,
            s.first_name, 
            s.last_name, 
            s.gender, 
            s.year_of_birth, 
            s.email, 
            s.phone, 
            s.city, 
            e.title as event,
            t.title as course,
            c.title as category,
        from subscriber s
            inner join category c on s.category_id=c.id
            inner join course t on c.course_id=t.id
            inner join event e on t.event_id=e.id
        where e.id=:event_id
        order by c.id, s.gender", array(':event_id' => 1))->queryAll();
            
        return $data;    
    }
}
