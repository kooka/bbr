<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Category;
use yii\db\Query;

/**
 * CategorySearch represents the model behind the search form about `common\models\Category`.
 */
class CategorySearch extends Category
{
    public $course;
    public $event;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'start_year', 'end_year'], 'integer'],
            [['title', 'open'], 'safe'],
            [['course', 'event'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = new Query;
        
        $query  ->select([
                        'category.*',
                        'course.title as course',
                        'event.title as event'
                    ])
                ->from('category')
                ->join('INNER JOIN', 'course', 'course.id = category.course_id')
                ->join('INNER JOIN', 'event', 'event.id = course.event_id');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'key' => 'id'
        ]);
                        
        $dataProvider->sort->defaultOrder = [            
            'id' => SORT_DESC
        ];
        
        $dataProvider->sort->attributes = [
            'id' => [
                'asc' => ['id' => SORT_ASC],
                'desc' => ['id' => SORT_DESC],                
                'label' => 'ID'
            ],
            'title' => [
                'asc' => ['title' => SORT_ASC],
                'desc' => ['title' => SORT_DESC],                
                'label' => 'Denumire'
            ],
            'slug' => [
                'asc' => ['slug' => SORT_ASC],
                'desc' => ['slug' => SORT_DESC],                
                'label' => 'Identificator'
            ],
            'course' => [
                'asc' => ['course' => SORT_ASC],
                'desc' => ['course' => SORT_DESC],
                'label' => 'Traseu'
            ],
            'event' => [
                'asc' => ['event' => SORT_ASC],
                'desc' => ['event' => SORT_DESC],
                'label' => 'Eveniment'
            ]
        ];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'course.title', $this->course])
            ->andFilterWhere(['like', 'event.title', $this->event]);

        return $dataProvider;
    }
}
